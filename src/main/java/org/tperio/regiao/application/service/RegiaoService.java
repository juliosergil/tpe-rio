package org.tperio.regiao.application.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.application.port.in.AtualizaRegiaoUseCase;
import org.tperio.regiao.application.port.in.CriaNovaRegiaoUseCase;
import org.tperio.regiao.application.port.in.RecuperaRegiaoUseCase;
import org.tperio.regiao.application.port.out.AtualizaRegiaoPort;
import org.tperio.regiao.application.port.out.CriaNovaRegiaoPort;
import org.tperio.regiao.application.port.out.RecuperaRegiaoPort;
import org.tperio.regiao.domain.Regiao;

public class RegiaoService implements CriaNovaRegiaoUseCase, AtualizaRegiaoUseCase, RecuperaRegiaoUseCase {
	
	private CriaNovaRegiaoPort criaNovaRegiaoPort;
	private AtualizaRegiaoPort atualizaRegiaoPort;
	private RecuperaRegiaoPort recuperaRegiaoPort;
	
	public RegiaoService(CriaNovaRegiaoPort criaNovaRegiaoPort,
			AtualizaRegiaoPort atualizaRegiaoPort,
			RecuperaRegiaoPort recuperaRegiaoPort) {
		this.atualizaRegiaoPort = atualizaRegiaoPort;
		this.criaNovaRegiaoPort = criaNovaRegiaoPort;
		this.recuperaRegiaoPort = recuperaRegiaoPort;
	}

	@Override
	public List<Regiao> recuperaRegioesAtivas() {
		return recuperaRegiaoPort.recuperaRegioesAtivas();
	}

	@Override
	public Regiao atualizaRegiao(Regiao regiao) throws TperioBusinessException {
		Optional<Regiao> regiaoOpt = recuperaRegiaoPort.recuperaRegiaoPorUuid(regiao.getUuid());
		if(regiaoOpt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.REGIAONAOENCONTRADA));
		}
		Regiao temp = regiaoOpt.get();
		regiaoOpt = recuperaRegiaoPort.recuperaRegiaoPorNome(regiao.getNome());
		if(regiaoOpt.isPresent() && !regiaoOpt.get().getIdregiao().equals(temp.getIdregiao())) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.REGIAOJAEXISTE));
		}
		return atualizaRegiaoPort.atualizaRegiao(regiao);
	}

	@Override
	public Regiao criaNovoRegiao(Regiao regiao) throws TperioBusinessException {
		regiao.valida();
		Optional<Regiao> regiaoOpt = recuperaRegiaoPort.recuperaRegiaoPorNome(regiao.getNome());
		if(regiaoOpt.isPresent()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.REGIAOJAEXISTE));
		}
		return criaNovaRegiaoPort.criaRegiao(regiao);
	}

}
