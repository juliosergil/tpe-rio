package org.tperio.regiao.application.port.out;

import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.domain.Regiao;

public interface AtualizaRegiaoPort {

	Regiao atualizaRegiao(Regiao regiao) throws TperioBusinessException;

}
