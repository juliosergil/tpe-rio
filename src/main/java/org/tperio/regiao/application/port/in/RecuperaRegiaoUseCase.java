package org.tperio.regiao.application.port.in;

import java.util.List;

import org.tperio.regiao.domain.Regiao;

public interface RecuperaRegiaoUseCase {

	List<Regiao> recuperaRegioesAtivas();

}
