package org.tperio.regiao.application.port.in;

import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.domain.Regiao;

public interface CriaNovaRegiaoUseCase {

	Regiao criaNovoRegiao(Regiao regiao) throws TperioBusinessException;

}
