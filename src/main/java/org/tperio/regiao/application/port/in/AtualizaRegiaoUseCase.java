package org.tperio.regiao.application.port.in;

import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.domain.Regiao;

public interface AtualizaRegiaoUseCase {

	Regiao atualizaRegiao(Regiao regiao) throws TperioBusinessException;

}
