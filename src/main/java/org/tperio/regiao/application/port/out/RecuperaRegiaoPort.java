package org.tperio.regiao.application.port.out;

import java.util.List;
import java.util.Optional;

import org.tperio.regiao.domain.Regiao;

public interface RecuperaRegiaoPort {

	List<Regiao> recuperaRegioesAtivas();

	Optional<Regiao> recuperaRegiaoPorNome(String nome);

	Optional<Regiao> recuperaRegiaoPorUuid(String uuid);

}
