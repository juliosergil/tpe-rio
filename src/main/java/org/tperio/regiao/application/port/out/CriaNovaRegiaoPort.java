package org.tperio.regiao.application.port.out;

import org.tperio.regiao.domain.Regiao;

public interface CriaNovaRegiaoPort {

	Regiao criaRegiao(Regiao regiao);

}
