package org.tperio.regiao.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import org.tperio.StringUteis;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;

public class Regiao {

	private Long idregiao;
	
	private String uuid = UUID.randomUUID().toString();
	private String nome;
	
	private Date dataCriacao;
	
	private Date datadesativado;

	public Long getIdregiao() {
		return idregiao;
	}

	public void setIdregiao(Long idregiao) {
		this.idregiao = idregiao;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

	public void valida() throws TperioBusinessException {
		if(StringUteis.isNullOrEmpty(this.getNome())) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PREENCHANOMEREGIAO));
		}
		
	}
	
}
