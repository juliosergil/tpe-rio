package org.tperio.regiao.adapter.out.persistence;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

@Entity
@Table(name = "regiao")
public class RegiaoJpaEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idregiao",unique = true)
	private Long idregiao;
	
	private String uuid = UUID.randomUUID().toString();
	private String nome;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datacriacao")
	private Date dataCriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datadesativado")
	private Date datadesativado;

	public Long getIdregiao() {
		return idregiao;
	}

	public void setIdregiao(Long idregiao) {
		this.idregiao = idregiao;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}
	
}
