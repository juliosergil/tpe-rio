package org.tperio.regiao.adapter.out.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RegiaoRepository extends JpaRepository<RegiaoJpaEntity, Long>{

	Optional<RegiaoJpaEntity> findByNome(String nome);

	Optional<RegiaoJpaEntity> findByUuid(String uuid);
}
