package org.tperio.regiao.adapter.out.persistence;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.application.port.out.AtualizaRegiaoPort;
import org.tperio.regiao.application.port.out.CriaNovaRegiaoPort;
import org.tperio.regiao.application.port.out.RecuperaRegiaoPort;
import org.tperio.regiao.domain.Regiao;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class RegiaoAdapter implements AtualizaRegiaoPort, CriaNovaRegiaoPort, RecuperaRegiaoPort {

	private final RegiaoRepository regiaoRepository;
	
	@Override
	public List<Regiao> recuperaRegioesAtivas() {
		return regiaoRepository.findAll()
				.stream()
				.filter(ent -> ent.getDatadesativado() == null)
				.map(this::toRegiao)
				.collect(Collectors.toList());
	}

	public Regiao toRegiao(RegiaoJpaEntity entity) {
		Regiao regiao = new Regiao();
		BeanUtils.copyProperties(entity, regiao);
		return regiao;
	}
	
	@Override
	public Regiao criaRegiao(Regiao regiao) {
		RegiaoJpaEntity entity = new RegiaoJpaEntity();
		BeanUtils.copyProperties(regiao, entity);
		entity.setUuid(UUID.randomUUID().toString());
		entity.setDataCriacao(new Date());
		entity = regiaoRepository.save(entity);
		return toRegiao(entity);
	}

	@Override
	public Regiao atualizaRegiao(Regiao regiao) throws TperioBusinessException {
		RegiaoJpaEntity entity = regiaoRepository.findByUuid(regiao.getUuid())
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.REGIAONAOENCONTRADA)));
		entity.setNome(regiao.getNome());
		entity = regiaoRepository.save(entity);
		return toRegiao(entity);
	}

	@Override
	public Optional<Regiao> recuperaRegiaoPorNome(String nome) {
		Optional<RegiaoJpaEntity> entity = regiaoRepository.findByNome(nome);
		if(entity.isPresent()) {
			return Optional.of(toRegiao(entity.get()));
		}
		return Optional.empty();
	}

	@Override
	public Optional<Regiao> recuperaRegiaoPorUuid(String uuid) {
		Optional<RegiaoJpaEntity> entity = regiaoRepository.findByUuid(uuid);
		if(entity.isPresent()) {
			return Optional.of(toRegiao(entity.get()));
		}
		return Optional.empty();
	}

	public RegiaoJpaEntity fromRegiao(Regiao regiao) {
		RegiaoJpaEntity p = new RegiaoJpaEntity();
		BeanUtils.copyProperties(regiao, p);
		return p;
	}

}
