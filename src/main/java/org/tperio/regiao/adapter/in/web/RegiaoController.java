package org.tperio.regiao.adapter.in.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.application.port.in.AtualizaRegiaoUseCase;
import org.tperio.regiao.application.port.in.CriaNovaRegiaoUseCase;
import org.tperio.regiao.application.port.in.RecuperaRegiaoUseCase;
import org.tperio.regiao.domain.Regiao;

import lombok.AllArgsConstructor;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/regiao")
@AllArgsConstructor
public class RegiaoController {

	private final RecuperaRegiaoUseCase recuperaRegioes;
	private final CriaNovaRegiaoUseCase criaRegiao;
	private final AtualizaRegiaoUseCase atualizaRegiaoUseCase;
	
	@GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<RegiaoDto>> recuperaRegiao() {
		List<Regiao> regioes = recuperaRegioes.recuperaRegioesAtivas();
		List<RegiaoDto> lista = regioes.stream().map(RegiaoDto::fromRegiao).collect(Collectors.toList());
		return ResponseEntity.ok(lista);
	}
	
	@PostMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RegiaoDto> gravaRegiao(@Valid @RequestBody RegiaoDto regiaoDto) throws TperioBusinessException {
		Regiao circ = criaRegiao.criaNovoRegiao(regiaoDto.toRegiao());
		return ResponseEntity.ok(RegiaoDto.fromRegiao(circ));
	}

	@PutMapping(path = "/{uuid}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public RegiaoDto atualizaRegiao(@PathVariable String uuid,
    		@Valid @RequestBody RegiaoDto regiaoDto, 
    		@AuthenticationPrincipal UserDetails user) 
    				throws TperioBusinessException {
		Regiao regiao = atualizaRegiaoUseCase.atualizaRegiao(regiaoDto.toRegiao());
    	return RegiaoDto.fromRegiao(regiao);
    }
	
}
