package org.tperio.regiao.adapter.in.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.BeanUtils;
import org.tperio.regiao.domain.Regiao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class RegiaoDto {

	private String uuid;
	
	@NotNull(message = "Preencha o nome")
	@Size(min = 4, max = 255, message = "Tamanho do nome deve ser entre 4 e 255 letras")
	private String nome;

	public Regiao toRegiao() {
		Regiao regiao = new Regiao();
		BeanUtils.copyProperties(this, regiao);
		return regiao;
	}

	public static RegiaoDto fromRegiao(Regiao regiao) {
		return RegiaoDto.builder()
				.nome(regiao.getNome())
				.uuid(regiao.getUuid())
				.build();
	}
	
}
