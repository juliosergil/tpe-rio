package org.tperio.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Random;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.tperio.config.MessageKeys;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;

@ConfigurationProperties(prefix = "file")
@Service
public class FileUploadService {
	
	private String prefix = "stream2file";
	private String suffix = ".tmp";
	private Logger logger = LoggerFactory.getLogger(FileUploadService.class);
	
	@Value("${file.import-dir-banner}")
	private String importDirbanners;
	
	public void uploadEventoBanner(Evento evento) throws TperioBusinessException {
		InputStream in = null;
		OutputStream out = null;
		try {
			File assinaturaF = Files.createTempFile(prefix, suffix).toFile();
			Files.write(assinaturaF.toPath(), evento.getBanner());
			String ext = FilenameUtils.getExtension(evento.getCaminhobanner());
			String nomeArquivo = String.format("%s%s", System.currentTimeMillis(), new Random().nextInt(100000) + "." + ext);
			
			File path = new File(importDirbanners + nomeArquivo);
			if (!path.getParentFile().exists()) {
				path.getParentFile().mkdirs();
			}
	        if (!path.exists()) {  
	        	if(!path.createNewFile()) {
	        		logger.debug("NÃO FOI POSSÍVEL CRIAR O ARQUIVO: {} ", path.getAbsolutePath());
	        		throw new TperioBusinessException(Arrays.asList(MessageKeys.ERROAOCRIARARQUIVO));
	        	}
	        } else {
	        	logger.debug("NÃO FOI POSSÍVEL ACHAR O DIRETÓRIO: {} ", path.getAbsolutePath());
	        	throw new TperioBusinessException(Arrays.asList(MessageKeys.ERROAOCRIARARQUIVO));
	        }
			in = new FileInputStream(path);
	    	out = new FileOutputStream(assinaturaF, true);
	    	IOUtils.copy(in, out);
	    	evento.setCaminhobanner(nomeArquivo);
		} catch (IOException e) {
			logger.debug("ERRO AO CRIAR ARQUIVO: {} ", e.getLocalizedMessage());
    		throw new TperioBusinessException(Arrays.asList(MessageKeys.ERROAOCRIARARQUIVO));
		} finally {
	    	try {
				in.close();
				out.close();
			} catch (IOException e) {
				logger.debug("ERRO AO FECHAR RECURSOS: {} ", e.getLocalizedMessage());
	    		throw new TperioBusinessException(Arrays.asList(MessageKeys.ERROAOCRIARARQUIVO));
			}
		}
    	
	}
	
	public boolean apagaEventoBanner(String nomeArquivo) throws TperioBusinessException {
		File path = new File(importDirbanners + nomeArquivo);
		if (!path.getParentFile().exists()) {
			path.getParentFile().mkdirs();
		}
        if (!path.exists()) {  
        		logger.debug("NÃO FOI POSSÍVEL ACHAR O ARQUIVO: {} ", path.getAbsolutePath());
        		throw new TperioBusinessException(Arrays.asList(MessageKeys.ARQUIVONAOENCONTRADO));
        } else {
        	try {
				Files.delete(path.toPath());
				return true;
			} catch (IOException e) {
				logger.debug("NÃO FOI POSSÍVEL ACHAR O ARQUIVO: {} ", path.getAbsolutePath());
        		throw new TperioBusinessException(Arrays.asList(MessageKeys.ARQUIVONAOENCONTRADO, e.getMessage()));
			}
        }
	}

}
