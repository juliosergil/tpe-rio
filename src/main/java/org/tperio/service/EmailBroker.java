package org.tperio.service;

import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.config.EmailConfig;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.domain.Publicador;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Component
public class EmailBroker {

	private static Logger log = LoggerFactory.getLogger(EmailBroker.class);
	private final OkHttpClient httpClient = new OkHttpClient();

    @Autowired
    @Qualifier("emailConfigBean")
    private Configuration emailTemplateConfig;
    
    @Autowired
    private EmailConfig emailLocalConfig;

    @Autowired private Environment env;

	public String enviaEmailNovaSenha(String name, String senha, String email) {
		String msg = null;
		StringWriter stringWriter = new StringWriter();
        Map<String, Object> model = new HashMap<>();
        model.put("senha", senha);
        model.put("nome",name);
        model.put("nometpe", emailLocalConfig.getNome());
		try {
			emailTemplateConfig.getTemplate("novasenha.ftlh").process(model, stringWriter);
			msg = stringWriter.getBuffer().toString();
		} catch (IOException | TemplateException e) {
			log.debug(e.getLocalizedMessage());
		}
		
		montaRequest(email, "[" + emailLocalConfig.getNome()  + " - Nova senha]", msg);
		
		return msg;
	}
	
	public String enviaEmailSenhaCadastro(String name, String senha, String email) {
		String msg = null;
		StringWriter stringWriter = new StringWriter();
        Map<String, Object> model = new HashMap<>();
        model.put("senha", senha);
        model.put("nome",name);
        model.put("nometpe", emailLocalConfig.getNome());
		try {
			emailTemplateConfig.getTemplate("senhacadastro.ftlh").process(model, stringWriter);
			msg = stringWriter.getBuffer().toString();
		} catch (IOException | TemplateException e) {
			log.debug(e.getLocalizedMessage());
		}
		
		montaRequest(email, "[" + emailLocalConfig.getNome()  + " - Cadastro]", msg);
		
		return msg;
	}
	
	private Request montaRequest(String email, String assunto, String msg){
		String credential = Credentials.basic("api", emailLocalConfig.getApi());
		RequestBody formBody = new FormBody.Builder()
		        .add("from", emailLocalConfig.getNome() + "<" + emailLocalConfig.getRemetente() + ">")
		        .add("to", email)
		        .add("subject", assunto)
		        .add("html", msg)
		        .build();
	    Request request = new Request.Builder()
	            .url("https://api.mailgun.net/v3/" + emailLocalConfig.getDominio() + "/messages")
	            .addHeader("User-Agent", "OkHttp Bot")
	            .addHeader("Authorization", credential)
	            .post(formBody)
	            .build();
	    if(!env.getActiveProfiles()[0].contains("test")) {
		    try (Response response = httpClient.newCall(request).execute()) {
		        if (!response.isSuccessful()) throw new IOException("Não foi possível enviar o email: " + response);
		    } catch (IOException e) {
				log.debug(e.getLocalizedMessage());
			}
	    }
	    return null;
	}

	public String enviaEmailAgendamento(Agendamento agendamento) {
		String msg = null;
		StringWriter stringWriter = new StringWriter();
        Map<String, Object> model = new HashMap<>();
        model.put("agendamento", agendamento);
        model.put("nometpe", emailLocalConfig.getNome());
		try {
			emailTemplateConfig.getTemplate("confirmacaoagendamento.ftlh").process(model, stringWriter);
			msg = stringWriter.getBuffer().toString();
		} catch (IOException | TemplateException e) {
			e.printStackTrace();
			log.debug(e.getLocalizedMessage());
		}

		montaRequest(agendamento.getPrincipal().getEmail(), "[" + emailLocalConfig.getNome()  + " - Confirmação agendamento]", msg);

		return msg;
	}

	public String enviaEmailProgramacaoDia(String email, List<Agendamento> agendamentos, Date data, Ponto ponto) {
		
		String dataFinal = new SimpleDateFormat("dd/MM/yyyy").format(data);

		String msg = null;
		StringWriter stringWriter = new StringWriter();
        Map<String, Object> model = new HashMap<>();
        model.put("agendamentos", agendamentos);
        model.put("data", dataFinal);
        model.put("ponto", ponto);
        model.put("nometpe", emailLocalConfig.getNome());
		try {
			emailTemplateConfig.getTemplate("programacaodia.ftlh").process(model, stringWriter);
			msg = stringWriter.getBuffer().toString();
		} catch (IOException | TemplateException e) {
			e.printStackTrace();
			log.debug(e.getLocalizedMessage());
		}

		montaRequest(email, "[" + emailLocalConfig.getNome()  + " - Programação completa do dia " + dataFinal + " e ponto " + ponto.getNome() + "]", msg);

		return msg;
	}

	public String enviaEmailCancelamento(final Publicador destinatario, final Agendamento agendamento) {
		String msg = null;
		StringWriter stringWriter = new StringWriter();
        Map<String, Object> model = new HashMap<>();
        model.put("agendamento", agendamento);
        model.put("destinatario", destinatario);
        model.put("nometpe", emailLocalConfig.getNome());
		try {
			emailTemplateConfig.getTemplate("cancelamentoagendamento.ftlh").process(model, stringWriter);
			msg = stringWriter.getBuffer().toString();
		} catch (IOException | TemplateException e) {
			e.printStackTrace();
			log.debug(e.getLocalizedMessage());
		}

		montaRequest(destinatario.getEmail(), "[" + emailLocalConfig.getNome()  + " - Cancelamento de Programação]", msg);

		return msg;
	}
}
