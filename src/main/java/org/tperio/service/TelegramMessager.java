package org.tperio.service;

import java.util.Optional;

import org.springframework.stereotype.Component;
import org.tperio.config.TelegramListener;
import org.tperio.publicador.adapter.out.persistence.PublicadorAdapter;
import org.tperio.publicador.domain.Publicador;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class TelegramMessager {
	
	private final PublicadorAdapter publicadorAdapter;
	private final TelegramListener listener;

	public void enviaMensagemPublicadorUuid(String uuid, String message) {
		Optional<Publicador> pub = publicadorAdapter.recuperaPublicadorUuid(uuid);
		if(pub.isPresent()) {
			String chatId = publicadorAdapter.recuperaContatoTelegram(pub.get());
			listener.sendMessage(chatId, message);
		}
	}
	
	public void enviaMensagem(String chatId, String message) {
		listener.sendMessage(chatId, message);
	}
}
