package org.tperio.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = "app.email")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmailConfig {

	private String nome;
	private String api;
	private String dominio;
	private String remetente;
	
}
