package org.tperio.config;

import java.io.IOException;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.tperio.publicador.adapter.in.web.PublicadorDto;
import org.tperio.publicador.adapter.in.web.UsuarioServiceImpl;
import org.tperio.service.JwtUtil;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JwtAuthFilter extends OncePerRequestFilter {

    private JwtUtil jwtUtil;
    private UsuarioServiceImpl usuarioService;
    private ObjectMapper mapper;

    public JwtAuthFilter(JwtUtil jwtService, UsuarioServiceImpl usuarioService, ObjectMapper mapper) {
        this.jwtUtil = jwtService;
        this.usuarioService = usuarioService;
        this.mapper = mapper;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            FilterChain filterChain) throws ServletException, IOException {

        String authorization = httpServletRequest.getHeader("Authorization");

        if( authorization != null && authorization.startsWith("Bearer")){
            String token = authorization.split(" ")[1];
            boolean isValid = jwtUtil.tokenValido(token);

            if(isValid){
                String loginUsuario = jwtUtil.obterLoginUsuario(token);
                PublicadorDto usu = mapper.readValue(loginUsuario, PublicadorDto.class);
                UserDetails usuario = usuarioService.loadUserByUsername(usu.getEmail());
                UsernamePasswordAuthenticationToken user = new
                        UsernamePasswordAuthenticationToken(usuario,null,
                        usuario.getAuthorities());
                user.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(user);
            }
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);

    }
}
