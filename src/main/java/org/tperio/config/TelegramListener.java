package org.tperio.config;

import java.util.Arrays;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.tperio.publicador.adapter.out.persistence.PublicadorAdapter;
import org.tperio.publicador.domain.Publicador;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.model.request.KeyboardButton;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.request.SendMessage;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TelegramListener {
	
	private TelegramBot bot;
	@Autowired private PublicadorAdapter adapter;
	@Autowired private Environment env;
	
	@Profile(value = "prd")//Verificar o profile.
	@EventListener(classes = ApplicationReadyEvent.class)
	public void getChatId() {
		if (Arrays.asList(env.getActiveProfiles()).contains("dev")) {
	       log.debug("em ambiente de dev");  
		}
		if (Arrays.asList(env.getActiveProfiles()).contains("prd")) {
		       log.debug("em ambiente de prd");  
			Keyboard keyboard = new ReplyKeyboardMarkup(
		        new KeyboardButton[]{
	                new KeyboardButton("Compartilhar seus dados").requestContact(true)
		        }
			).oneTimeKeyboard(true);
			getBot().setUpdatesListener(updates -> {
				for(Update up : updates) {
					log.debug("recebido update");
					Long chatId = up.message().chat().id();
					if(!adapter.existeContatoTelegram(chatId + "")) {
						if(up.message().contact() == null) {
							SendMessage request = new SendMessage(chatId, "Por favor compartilhe seus dados clicando no botão abaixo.")
							        .parseMode(ParseMode.Markdown).replyMarkup(keyboard)
							        .disableNotification(true);
							getBot().execute(request);
						} else {
							Optional<Publicador> pub = adapter.recuperaPublicadorPorTelefone(up.message().contact().phoneNumber().substring(2));
							if(pub.isPresent()) {
								adapter.criaPublicadorContatoTelegram(pub.get(), chatId + "");
								System.out.println("publicador gravado com sucesso");
							}
							String numero = up.message().contact().phoneNumber();
							System.out.println(numero + " - " + chatId);
						}
					} else {
						//Envi
						//messager.enviaMensagem(chatId, "");
					}
				}
			    return UpdatesListener.CONFIRMED_UPDATES_ALL;
			});
		}
	}

	public void sendMessage(String chatId, String message) {
		SendMessage request = new SendMessage(chatId, message)
		        .parseMode(ParseMode.HTML)
		        .disableNotification(true);
		getBot().execute(request);
	}

	
	private TelegramBot getBot() {
		if(this.bot == null) {
			bot = new TelegramBot("5407090004:AAHD0oGznBw6cyidufJ0KTH2u6-tcEwIHYc");
		}
		return this.bot;
	}
}
