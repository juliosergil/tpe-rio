package org.tperio.config;

public class MessageKeys {
	
	public static final String PREENCHALAT 			= "preencha.latitude";
	public static final String PREENCHALATDEP 		= "preencha.latitude.deposito";
	public static final String PREENCHALONGDEP 		= "preencha.longitude.deposito";
	public static final String PREENCHALONG 		= "preencha.longitude";
	public static final String PREENCHANOME 		= "preencha.longitude.deposito";
	public static final String PREENCHAENDERECO 	= "preencha.longitude.deposito";
	public static final String PREENCHAENDERECODEP 	= "preencha.longitude.deposito";
	public static final String PREENCHARESPONSAVEL 	= "preencha.responsavel";
	public static final String PREENCHAPRINCIPAL 	= "preencha.principal";
	public static final String PREENCHAAUXILIAR		= "preencha.auxiliar";
	public static final String PREENCHAUUID			= "preencha.uuid";
	public static final String PREENCHAHORAINICIO	= "preencha.hora.inicio";
	public static final String HORAINICIOFORMATOINVALIDO	= "hora.inicio.formato.invalido";
	public static final String HORAFIMFORMATOINVALIDO		= "hora.fim.formato.invalido";
	public static final String PREENCHAHORAFIM		= "preencha.hora.fim";
	public static final String PREENCHAPONTO 		= "preencha.ponto";
	public static final String PREENCHADATA 		= "preencha.data";
	public static final String PRINCIPALNAOENCONTRADO 	= "principal.nao.encontrado";
	public static final String PUBLICADORNAOENCONTRADO 	= "publicador.nao.encontrado";
	public static final String PONTONAOENCONTRADO 		= "ponto.nao.encontrado";
	public static final String PERFILNAOENCONTRADO 		= "perfil.nao.encontrado";
	public static final String CONGREGACAONAOENCONTRADA	= "congregacao.nao.encontrada";
	public static final String AUXILIARNAOENCONTRADO 	= "auxiliar.nao.encontrado";
	public static final String RESPONSAVELNAOENCONTRADO = "responsavel.nao.encontrado";
	public static final String AGENDAMENTOJAEXISTE 		= "agendamento.ja.existe";
	public static final String PREENCHADATAINICIAL		= "preencha.data.inicial";
	public static final String PREENCHADATAFINAL		= "preencha.data.final";
	public static final String ERROAOCRIARARQUIVO 		= "erro.ao.criar.arquivo";
	public static final String ARQUIVONAOENCONTRADO		= "arquivo.nao.encontrado";
	public static final String EVENTOJAEXISTE 			= "evento.ja.existe";
	public static final String EVENTONAOENCONTRADO		= "evento.nao.encontrado";
	public static final String PREENCHAEMAIL 			= "preencha.email";
	public static final String PREENCHATELEFONE 		= "preencha.telefone";
	public static final String PREENCHAGENERO 			= "preencha.genero";
	public static final String PREENCHASENHA 			= "preencha.senha";
	public static final String PREENCHACONGREGACAO 		= "preencha.congregacao";
	public static final String EMAILINVALIDO 			= "email.invalido";
	public static final String SENHATAMANHOINVALIDO 	= "senha.tamanho.invalido";
	public static final String PREENCHAPERFIL 			= "preencha.perfil";
	public static final String PREENCHAREGIAO 			= "preencha.regiao";
	public static final String AGENDAMENTONAOENCONTRADO = "agendamento.nao.encontrado";
	public static final String RESPONSAVELSEMPERMISSAO  = "responsavel.sem.permissao";
	public static final String AUXILIARSEMPERMISSAO 	= "auxiliar.sem.permissao";
	public static final String AGENDAMENTONAOAUTORIZADO = "publicador.nao.autorizado.agendamento";
	public static final String AGENDAMENTOAUXNAOAUTORIZADO = "publicador.auxiliar.nao.autorizado.agendamento";
	public static final String PREENCHANOMEREGIAO		= "preencha.nome.regiao";
	public static final String REGIAONAOENCONTRADA		= "regiao.nao.encontrada";
	public static final String REGIAOJAEXISTE 			= "regiao.ja.existe";
	public static final String CIRCUITONAOENCONTRADO 	= "circuito.nao.encontrado";
	public static final String CONGREGACAOJAEXISTE 		= "congregacao.ja.existe";
	public static final String PREENCHACIRCUITO 		= "preencha.circuito";
	
}
