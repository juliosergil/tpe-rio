package org.tperio.config;

public class GeneralError {
	
	private String text;
	private String code;
	
	@SuppressWarnings("unused")
	private GeneralError () {}
	
	public GeneralError (String text, String code) {
		this.text = text;
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public String getCode() {
		return code;
	}
	

}
