package org.tperio.config;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.tperio.exception.TperioBusinessException;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class GeneralExceptionHandler {

	private final String mensagemErro = "Ocorreu um erro, tente novamente mais tarde: ";
	private final String statusErro = "500";

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentNotValidException.class)
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<?> lista = ex.getAllErrors().stream().map(exp -> new GeneralError(Messages.getInstance().getMessage(exp.getDefaultMessage()), "500")).collect(Collectors.toList());
		return ResponseEntity.badRequest().body(lista);
	}
	
	@ExceptionHandler(TperioBusinessException.class)
    public ResponseEntity<GeneralError> constraintViolationException(HttpServletResponse response, Exception ex) {
    	if(ex instanceof TperioBusinessException) {
    		TperioBusinessException cve = (TperioBusinessException) ex;
    		var str = new StringBuilder();
    		for (String item: cve.getMensagens()) {
        		str.append(item);
            }
    		return new ResponseEntity<>(
 	               new GeneralError("Ocorreu um erro: " + str.toString(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
    	}
       return new ResponseEntity<>(
               new GeneralError("Ocorreu um erro, tente novamente mais tarde: " + ex.getCause().getLocalizedMessage(), "500"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<GeneralError> constraintViolationException(HttpServletResponse response, RuntimeException ex) {
		return new ResponseEntity<>(
				new GeneralError(mensagemErro + ex.getLocalizedMessage(), statusErro), HttpStatus.INTERNAL_SERVER_ERROR);
	}
    
}
