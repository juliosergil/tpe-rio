package org.tperio.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.tperio.agendamento.application.port.in.AtualizaAgendamentoUseCase;
import org.tperio.agendamento.application.port.in.CriaNovoAgendamentoUseCase;
import org.tperio.agendamento.application.port.in.RecuperaAgendamentoUseCase;
import org.tperio.agendamento.application.port.out.AtualizaAgendamentoPort;
import org.tperio.agendamento.application.port.out.CriaNovoAgendamentoPort;
import org.tperio.agendamento.application.port.out.RecuperaAgendamentoPort;
import org.tperio.agendamento.application.service.AtualizaAgendamentoService;
import org.tperio.agendamento.application.service.CriaAgendamentoService;
import org.tperio.agendamento.application.service.RecuperaAgendamentosService;
import org.tperio.circuito.application.port.in.RecuperaCircuitosUseCase;
import org.tperio.circuito.application.port.out.AtualizaCircuitoPort;
import org.tperio.circuito.application.port.out.CriaNovoCircuitoPort;
import org.tperio.circuito.application.port.out.RecuperaCircuitosPort;
import org.tperio.circuito.application.service.CircuitoService;
import org.tperio.congregacao.application.port.in.RecuperaCongregacoesUseCase;
import org.tperio.congregacao.application.port.out.AtualizaCongregacaoPort;
import org.tperio.congregacao.application.port.out.CriaNovaCongregacaoPort;
import org.tperio.congregacao.application.port.out.RecuperaCongregacoesPort;
import org.tperio.congregacao.application.service.CongregacaoService;
import org.tperio.evento.adapter.out.persistence.EventoAdapter;
import org.tperio.evento.application.port.in.CriaEventoUseCase;
import org.tperio.evento.application.port.in.DesativaEventoUseCase;
import org.tperio.evento.application.port.out.AtualizaEventoPort;
import org.tperio.evento.application.port.out.CriaEventoPort;
import org.tperio.evento.application.port.out.DesativaEventoPort;
import org.tperio.evento.application.port.out.RecuperaEventoPort;
import org.tperio.evento.application.service.DesativaEventoService;
import org.tperio.evento.application.service.EventoService;
import org.tperio.message.port.out.MessageBrokerPort;
import org.tperio.ponto.adapter.out.persistence.PontoAdapter;
import org.tperio.ponto.application.port.in.CriaNovoPontoUseCase;
import org.tperio.ponto.application.port.out.AtualizaPontoPort;
import org.tperio.ponto.application.port.out.CriaNovoPontoPort;
import org.tperio.ponto.application.port.out.RecuperaPontoPort;
import org.tperio.ponto.application.service.PontoService;
import org.tperio.publicador.adapter.out.persistence.PublicadorAdapter;
import org.tperio.publicador.application.port.in.AtualizaPublicadorUseCase;
import org.tperio.publicador.application.port.in.RecuperaPerfilUseCase;
import org.tperio.publicador.application.port.in.RecuperaPublicadorUseCase;
import org.tperio.publicador.application.port.out.AtualizaPublicadorPort;
import org.tperio.publicador.application.port.out.CriaNovoPublicadorPort;
import org.tperio.publicador.application.port.out.RecuperaPerfilPort;
import org.tperio.publicador.application.port.out.RecuperaPublicadorPort;
import org.tperio.publicador.application.service.AtualizaPublicadorService;
import org.tperio.publicador.application.service.PublicadorService;
import org.tperio.regiao.application.port.in.RecuperaRegiaoUseCase;
import org.tperio.regiao.application.port.out.AtualizaRegiaoPort;
import org.tperio.regiao.application.port.out.CriaNovaRegiaoPort;
import org.tperio.regiao.application.port.out.RecuperaRegiaoPort;
import org.tperio.regiao.application.service.RegiaoService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration
public class BeanConfiguration {

    @Bean
    RecuperaCircuitosUseCase recuperaCircuito(
    		CriaNovoCircuitoPort criaNovoCircuitoPort,
    		RecuperaCircuitosPort recuperaCircuitosPort,
    		AtualizaCircuitoPort atualizaCircuitoPort) {
        return new CircuitoService(
        		criaNovoCircuitoPort,
        		recuperaCircuitosPort,
        		atualizaCircuitoPort);
    }

	@Bean
	DesativaEventoUseCase desativaEvento(DesativaEventoPort desativaEventoPort) {
		return new DesativaEventoService(desativaEventoPort);
	}

	@Bean
    RecuperaCongregacoesUseCase recuperaCongregacao(
    		CriaNovaCongregacaoPort criaNovaCongregacaoPort,
    		RecuperaCongregacoesPort recuperaCongregacoesPort,
    		AtualizaCongregacaoPort atualizaCongregacaoPort,
   		    RecuperaCircuitosPort recuperaCircuitosPort,
   		    RecuperaRegiaoPort recuperaRegiaoPort) {
        return new CongregacaoService(
        		criaNovaCongregacaoPort, 
        		recuperaCongregacoesPort, 
        		atualizaCongregacaoPort, 
        		recuperaCircuitosPort,
        		recuperaRegiaoPort);
    }
    
    @Bean("recuperaPublicador")
    RecuperaPublicadorUseCase recuperaPublicador(
    		RecuperaPublicadorPort recuperaPublicadorPort, 
    		CriaNovoPublicadorPort criaNovoPublicadorPort,
    		RecuperaCongregacoesPort recuperaCongregacoesPort,
    		RecuperaPerfilPort recuperaPerfilPort,
    		RecuperaEventoPort recuperaEventoPort) {
    	return new PublicadorService(recuperaPublicadorPort, criaNovoPublicadorPort, recuperaCongregacoesPort,
    			recuperaPerfilPort, recuperaEventoPort);
    }
    
    @Bean("recuperaPerfil")
    RecuperaPerfilUseCase recuperaPerfil(
    		RecuperaPublicadorPort recuperaPublicadorPort, 
    		CriaNovoPublicadorPort criaNovoPublicadorPort,
    		RecuperaCongregacoesPort recuperaCongregacoesPort,
    		RecuperaPerfilPort recuperaPerfilPort,
    		RecuperaEventoPort recuperaEventoPort) {
    	return new PublicadorService(recuperaPublicadorPort, criaNovoPublicadorPort, recuperaCongregacoesPort,
    			recuperaPerfilPort, recuperaEventoPort);
    }
    
    @Bean
    CriaNovoPontoUseCase criaNovoPonto(
    		CriaNovoPontoPort criaNovoPontoPort,
    		RecuperaPublicadorPort recuperaPublicadorPort,
    		RecuperaPontoPort recuperaPontoPort,
    		AtualizaPontoPort atualizaPontoPort,
    		RecuperaEventoPort recuperaEventoPort) {
    	return new PontoService(criaNovoPontoPort, 
    			recuperaPublicadorPort, 
    			recuperaPontoPort, 
    			atualizaPontoPort,
    			recuperaEventoPort);
    }

    @Bean
    CriaNovoAgendamentoUseCase criaNovoAgendamento(
    		RecuperaAgendamentoPort recuperaAgendamento,
    		CriaNovoAgendamentoPort criaNovoAgendamento,
			MessageBrokerPort messageBrokerPort,
			CriaNovoPontoPort criaNovoPontoPort,
    		RecuperaPublicadorPort recuperaPublicadorPort,
    		RecuperaPontoPort recuperaPontoPort,
    		AtualizaPontoPort atualizaPontoPort,
    		CriaNovoPublicadorPort criaNovoPublicadorPort, 
			RecuperaCongregacoesPort recuperaCongregacoesPort,
			RecuperaPerfilPort recuperaPerfilPort,
    		RecuperaEventoPort recuperaEventoPort) {
    	return new CriaAgendamentoService(
    			recuperaAgendamento,
    			criaNovoAgendamento,
    			messageBrokerPort,
    			new PontoService(criaNovoPontoPort, recuperaPublicadorPort, recuperaPontoPort, atualizaPontoPort, recuperaEventoPort),
    			new PublicadorService(recuperaPublicadorPort, criaNovoPublicadorPort, recuperaCongregacoesPort, recuperaPerfilPort, recuperaEventoPort));
    }
    
    @Bean
    RecuperaRegiaoUseCase recuperaRegiao(
    		CriaNovaRegiaoPort criaNovaRegiaoPort,
    		AtualizaRegiaoPort atualizaRegiaoPort,
    		RecuperaRegiaoPort recuperaRegiaoPort) {
    	return new RegiaoService(criaNovaRegiaoPort, atualizaRegiaoPort, recuperaRegiaoPort);
    }

    @Bean
    RecuperaAgendamentoUseCase recuperaAgendamentos(
    		RecuperaAgendamentoPort recuperaAgendamento,
			PontoAdapter pontoAdapter,
			PublicadorAdapter publicadorAdapter,
			EventoAdapter eventoAdapter) {
    	return new RecuperaAgendamentosService(recuperaAgendamento, pontoAdapter, publicadorAdapter, eventoAdapter);
    }

    @Bean
    AtualizaAgendamentoUseCase atualizaAgendamentos(
    		AtualizaAgendamentoPort atualizaAgendamento,
    		RecuperaAgendamentoPort recuperaAgendamento,
    		MessageBrokerPort messageBrokerPort) {
    	return new AtualizaAgendamentoService(atualizaAgendamento, recuperaAgendamento, messageBrokerPort);
    }

    @Bean
    CriaEventoUseCase criaNovoEvento(
    		RecuperaEventoPort recuperaEvento,
    		CriaEventoPort criaEventoPort,
    		RecuperaPontoPort recuperaPontoPort,
    		AtualizaEventoPort atualizaEventoPort,
    		RecuperaPublicadorPort recuperaPublicadorPort) {
    	return new EventoService(recuperaEvento, criaEventoPort, recuperaPontoPort, atualizaEventoPort, recuperaPublicadorPort);
    }
    
    @Bean
    AtualizaPublicadorUseCase atualizaPublicador(
    		AtualizaPublicadorPort atualizaPublicadorPort,
    		RecuperaPublicadorPort recuperaPublicadorPort){
    	return new AtualizaPublicadorService(atualizaPublicadorPort, recuperaPublicadorPort);
    }
    
    @Bean
    @Primary
    ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper(); 
        mapper.findAndRegisterModules();
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }
    
}
