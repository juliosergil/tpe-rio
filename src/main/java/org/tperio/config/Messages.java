package org.tperio.config;

import java.util.Locale;
import java.util.ResourceBundle;

public class Messages {
	private static Messages message;
	private ResourceBundle bundle;
	private Locale l = new Locale("pt_BR");
	
	public static Messages getInstance() {
		if(message == null) {
			return new Messages();
		}
		return message;
	}
	
	private Messages() {
		
		this.bundle = ResourceBundle.getBundle("messages", l);
	}
	
	public String getMessage(String key) {
		return bundle.getString(key);
	}

}
