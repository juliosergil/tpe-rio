package org.tperio.circuito.application.port.in;

import org.tperio.circuito.domain.Circuito;
import org.tperio.exception.TperioBusinessException;

public interface AtualizaCircuitoUseCase {
	
	Circuito atualizaCircuito(Circuito circuito) throws TperioBusinessException;

}
