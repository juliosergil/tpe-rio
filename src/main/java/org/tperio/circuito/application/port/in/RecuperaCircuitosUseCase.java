package org.tperio.circuito.application.port.in;

import java.util.List;

import org.tperio.circuito.domain.Circuito;
import org.tperio.exception.TperioBusinessException;

public interface RecuperaCircuitosUseCase {
	
	List<Circuito> recuperaCircuitosAtivos();
	Circuito recuperaCircuitoPorUuid(String uuid) throws TperioBusinessException;

}
