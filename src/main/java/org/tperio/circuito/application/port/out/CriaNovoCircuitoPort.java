package org.tperio.circuito.application.port.out;

import org.tperio.circuito.domain.Circuito;

public interface CriaNovoCircuitoPort {

	Circuito criaNovoCircuito(Circuito circuito);
	
}
