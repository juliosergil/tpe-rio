package org.tperio.circuito.application.port.out;

import org.tperio.circuito.domain.Circuito;
import org.tperio.exception.TperioBusinessException;

public interface AtualizaCircuitoPort {
	
	Circuito atualizaCircuito(Circuito circuito) throws TperioBusinessException;

}
