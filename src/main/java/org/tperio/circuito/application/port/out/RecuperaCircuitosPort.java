package org.tperio.circuito.application.port.out;

import java.util.List;
import java.util.Optional;

import org.tperio.circuito.domain.Circuito;

public interface RecuperaCircuitosPort {

	List<Circuito> recuperaCircuitosAtivos();
	
	Optional<Circuito> recuperaCircuitoPorNome(String nome);
	
	Optional<Circuito> recuperaCircuitoPorUuid(String uuid);

}

