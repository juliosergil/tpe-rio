package org.tperio.circuito.application.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.tperio.circuito.application.port.in.AtualizaCircuitoUseCase;
import org.tperio.circuito.application.port.in.CriaNovoCircuitoUseCase;
import org.tperio.circuito.application.port.in.RecuperaCircuitosUseCase;
import org.tperio.circuito.application.port.out.AtualizaCircuitoPort;
import org.tperio.circuito.application.port.out.CriaNovoCircuitoPort;
import org.tperio.circuito.application.port.out.RecuperaCircuitosPort;
import org.tperio.circuito.domain.Circuito;
import org.tperio.exception.TperioBusinessException;

public class CircuitoService implements RecuperaCircuitosUseCase, CriaNovoCircuitoUseCase, 
AtualizaCircuitoUseCase {
	
	private CriaNovoCircuitoPort criaNovoCircuitoPort;
	private RecuperaCircuitosPort recuperaCircuitosPort;
	private AtualizaCircuitoPort atualizaCircuitoPort;
	
	public CircuitoService(CriaNovoCircuitoPort criaNovoCircuitoPort, 
							  RecuperaCircuitosPort recuperaCircuitosPort, 
							  AtualizaCircuitoPort atualizaCircuitoPort) {
		this.criaNovoCircuitoPort = criaNovoCircuitoPort;
		this.recuperaCircuitosPort = recuperaCircuitosPort;
		this.atualizaCircuitoPort = atualizaCircuitoPort;
	}

	@Override
	public List<Circuito> recuperaCircuitosAtivos() {
		return recuperaCircuitosPort.recuperaCircuitosAtivos();
	}

	@Override
	public Circuito criaNovoCircuito(Circuito circuito) throws TperioBusinessException {
		nomeCircuitoJaExiste(circuito);
		circuito.setUuid(UUID.randomUUID().toString());
		return criaNovoCircuitoPort.criaNovoCircuito(circuito);
	}

	private void nomeCircuitoJaExiste(Circuito circuito) throws TperioBusinessException {
		Optional<Circuito> circopt = recuperaCircuitosPort.recuperaCircuitoPorNome(circuito.getNome());
		if(!circopt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList("Circuito já existe."));
		}
	}
	
	@Override
	public Circuito recuperaCircuitoPorUuid(String uuid) throws TperioBusinessException {
		Optional<Circuito> circuito = recuperaCircuitosPort.recuperaCircuitoPorUuid(uuid);
		if(circuito.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList("Circuito não encontrado."));
		}
		return circuito.get();
	}

	@Override
	public Circuito atualizaCircuito(Circuito circuito) throws TperioBusinessException {
		recuperaCircuitoPorUuid(circuito.getUuid());

		atualizaCircuitoPort.atualizaCircuito(circuito);
		return circuito;
	}
	

}
