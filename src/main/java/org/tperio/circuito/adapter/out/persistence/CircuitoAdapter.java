package org.tperio.circuito.adapter.out.persistence;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.tperio.circuito.application.port.out.CriaNovoCircuitoPort;
import org.tperio.circuito.application.port.out.RecuperaCircuitosPort;
import org.tperio.circuito.application.port.out.AtualizaCircuitoPort;
import org.tperio.circuito.domain.Circuito;
import org.tperio.exception.TperioBusinessException;

@Component
public class CircuitoAdapter implements RecuperaCircuitosPort, 
										   CriaNovoCircuitoPort, 
										   AtualizaCircuitoPort {

	@Autowired
	private CircuitoRepository circuitoRepository;

	@Override
	public List<Circuito> recuperaCircuitosAtivos() {
		List<CircuitoJpaEntity> circ = circuitoRepository.findAll();
		return circ.stream().map(this::toCircuito).collect(Collectors.toList());
	}

	@Override
	public Circuito criaNovoCircuito(Circuito circuito) {
		CircuitoJpaEntity ent = circuitoRepository.save(fromCircuito(circuito));
		return toCircuito(ent);
	}

	@Override
	public Optional<Circuito> recuperaCircuitoPorNome(String nome) {
		Optional<CircuitoJpaEntity> opt = circuitoRepository.findByNome(nome);
		if(opt.isPresent()) {
			return Optional.of(toCircuito(opt.get()));
		}
		return Optional.empty();
	}

	public Circuito toCircuito(CircuitoJpaEntity circuitoJpaEntity) {
		Circuito p = new Circuito();
		BeanUtils.copyProperties(circuitoJpaEntity, p);
		return p;
	}

	public CircuitoJpaEntity fromCircuito(Circuito circuito) {
		CircuitoJpaEntity p = new CircuitoJpaEntity();
		BeanUtils.copyProperties(circuito, p);
		return p;
	}
	
	@Override
	public Circuito atualizaCircuito(Circuito circuito) throws TperioBusinessException {
		Optional<CircuitoJpaEntity> circuitoJpaOpt = circuitoRepository.findByUuid(circuito.getUuid());

		if(circuitoJpaOpt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList("Circuito não encontrado."));
		}
		
		CircuitoJpaEntity entity = circuitoJpaOpt.get();

		Long id = entity.getIdcircuito();
		Date dataCriacao = entity.getDataCriacao();
		String nome = entity.getNome();
		BeanUtils.copyProperties(fromCircuito(circuito), entity);
		entity.setIdcircuito(id);
		entity.setNome(nome);
		entity.setDataCriacao(dataCriacao);
		circuitoRepository.save(entity);
		return toCircuito(entity);
	}

	@Override
	public Optional<Circuito> recuperaCircuitoPorUuid(String uuid) {
		Optional<CircuitoJpaEntity> opt = circuitoRepository.findByUuid(uuid);
		if(opt.isPresent()) {
			return Optional.of(toCircuito(opt.get()));
		}
		return Optional.empty();
	}
	
}
