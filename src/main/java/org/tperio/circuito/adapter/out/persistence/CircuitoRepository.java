package org.tperio.circuito.adapter.out.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CircuitoRepository extends JpaRepository<CircuitoJpaEntity, Long> {

	Optional<CircuitoJpaEntity> findByNome(String nome);
	Optional<CircuitoJpaEntity> findByUuid(String uuid);
	
}
