package org.tperio.circuito.adapter.in.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tperio.circuito.application.port.in.AtualizaCircuitoUseCase;
import org.tperio.circuito.application.port.in.CriaNovoCircuitoUseCase;
import org.tperio.circuito.application.port.in.RecuperaCircuitosUseCase;
import org.tperio.circuito.domain.Circuito;
import org.tperio.exception.TperioBusinessException;

import lombok.AllArgsConstructor;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/circuito")
@AllArgsConstructor
public class CircuitoController {
	
	private final RecuperaCircuitosUseCase recuperaCircuitos;
	private final CriaNovoCircuitoUseCase criaCircuito;
	private final AtualizaCircuitoUseCase atualizaCircuitoUseCase;
	
	@GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CircuitoDto>> recuperaCircuito() {
		List<Circuito> circuitos = recuperaCircuitos.recuperaCircuitosAtivos();
		List<CircuitoDto> lista = circuitos.stream().map(CircuitoDto::fromCircuito).collect(Collectors.toList());
		return ResponseEntity.ok(lista);
	}
	
	@PostMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CircuitoDto> gravaCircuito(@Valid @RequestBody CircuitoDto circuitoDto) throws TperioBusinessException {
		Circuito circ = criaCircuito.criaNovoCircuito(circuitoDto.toCircuito());
		return ResponseEntity.ok(CircuitoDto.fromCircuito(circ));
	}

	@PutMapping(path = "/{uuid}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public CircuitoDto atualizaCircuito(@PathVariable String uuid,
    		@Valid @RequestBody CircuitoDto circuitoDto, 
    		@AuthenticationPrincipal UserDetails user) 
    				throws TperioBusinessException{
		Circuito circuito = atualizaCircuitoUseCase.atualizaCircuito(circuitoDto.toCircuito());
    	return CircuitoDto.fromCircuito(circuito);
    }
	
	
}
