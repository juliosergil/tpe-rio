package org.tperio.circuito.adapter.in.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.tperio.circuito.domain.Circuito;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class CircuitoDto {

	private String uuid;
	
	@NotNull(message = "Preencha o nome")
	@Size(min = 4, max = 255, message = "Tamanho do nome deve ser entre 4 e 255 letras")
	private String nome;

	public static CircuitoDto fromCircuito(Circuito circuito) {
		return CircuitoDto.builder()
				.uuid(circuito.getUuid())
				.nome(circuito.getNome())
				.build();
	}
	
	public Circuito toCircuito() {
		Circuito circuito = new Circuito();
		circuito.setUuid(this.uuid);
		circuito.setNome(this.nome);
		return circuito;
	}
}
