package org.tperio.circuito.domain;

import java.util.Date;
import java.util.UUID;

public class Circuito {
	
	private String uuid = UUID.randomUUID().toString();
	private String nome;
	private Date dataCriacao = new Date();
	private Date datadesativado;
	

	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Date getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public Date getDatadesativado() {
		return datadesativado;
	}
	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	

}
