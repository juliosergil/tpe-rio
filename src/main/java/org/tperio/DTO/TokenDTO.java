package org.tperio.DTO;

import java.util.List;

import org.tperio.evento.adapter.in.web.EventoAtivoDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TokenDTO {
    private String login;
    private String token;
    private List<EventoAtivoDto> eventos;
}
