package org.tperio.agendamento.application.port.in;

import org.tperio.agendamento.domain.Agendamento;
import org.tperio.exception.TperioBusinessException;

public interface CriaNovoAgendamentoUseCase {

	Agendamento criaNovoAgendamento(Agendamento agendamento) throws TperioBusinessException;
	
}
