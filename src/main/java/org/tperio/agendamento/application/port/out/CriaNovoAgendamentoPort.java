package org.tperio.agendamento.application.port.out;

import org.tperio.agendamento.domain.Agendamento;
import org.tperio.exception.TperioBusinessException;

public interface CriaNovoAgendamentoPort {

	/**
	 * Cria um novo agendamento
	 * Estoura exceção se não encontrar.
	 * @since 2022-06-10
	 * @author mgaa
	 * @return Agendamento
	 * @param agendamento Agendamento
	 * @throws TperioBusinessException
	 * */
	Agendamento criaNovoAgendamento(Agendamento agendamento) throws TperioBusinessException;
}
