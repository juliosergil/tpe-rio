package org.tperio.agendamento.application.port.in;

import org.tperio.exception.TperioBusinessException;

public interface ApagaAgendamentoUseCase {

	void apagaAgendamento(String uuid) throws TperioBusinessException;
	
}
