package org.tperio.agendamento.application.port.out;

import org.tperio.agendamento.domain.Agendamento;
import org.tperio.exception.TperioBusinessException;

public interface AtualizaAgendamentoPort {

	/**
	 * Atualiza um agendamento
	 * Estoura exceção se não encontrar.
	 * @since 2022-06-10
	 * @author mgaa
	 * @return Agendamento
	 * @param agendamento Agendamento
	 * @throws TperioBusinessException
	 * */
	Agendamento atualizaAgendamento(Agendamento agendamento) throws TperioBusinessException;
	
	/**
	 * Desativa um agendamento atualizando com a datadesativado
	 * Estoura exceção se não encontrar.
	 * @since 2022-06-24
	 * @author mgaa
	 * @param uuid String
	 * @throws TperioBusinessException
	 * */
	void desativaAgendamento(String uuid) throws TperioBusinessException;
	
}
