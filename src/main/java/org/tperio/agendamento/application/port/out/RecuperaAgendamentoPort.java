package org.tperio.agendamento.application.port.out;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.tperio.agendamento.domain.Agendamento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.domain.Publicador;

public interface RecuperaAgendamentoPort {

	/**
	 * Recupera um agendamento utilizando um UUID.
	 * Estoura exceção se não encontrar.
	 * @since 2022-06-10
	 * @author mgaa
	 * @return Agendamento
	 * @param uuid String
	 * @throws TperioBusinessException
	 * */
	Agendamento recuperaAgendamentoPorUuid(String uuid) throws TperioBusinessException;
	/**
	 * Recupera uma lista de agendamentos utilizando uma data e um ponto.
	 * Estoura exceção se não encontrar.
	 * @since 2022-06-10
	 * @author mgaa
	 * @return List<Agendamento>
	 * @param data Date
	 * @param ponto Ponto
	 * @throws TperioBusinessException
	 * */
	List<Agendamento> recuperaAgendamentosPorDataPonto(Date data, Ponto ponto) throws TperioBusinessException;
	/**
	 * Recupera uma lista de agendamentos utilizando uma data e um publicador.
	 * Estoura exceção se não encontrar.
	 * @since 2022-06-10
	 * @author mgaa
	 * @return List<Agendamento>
	 * @param data Date
	 * @param publicador Publicador
	 * @throws TperioBusinessException
	 * */
	List<Agendamento> recuperaAgendamentosPorDataPublicador(Date data, Publicador publicador, Optional<Date> datafim) throws TperioBusinessException;
	
	List<Agendamento> recuperaAgendamentosPorDataInicioeDataFim(Date datainicio, Date datafim) throws TperioBusinessException;
}
