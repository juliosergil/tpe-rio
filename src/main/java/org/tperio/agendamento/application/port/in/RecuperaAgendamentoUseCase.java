package org.tperio.agendamento.application.port.in;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.tperio.agendamento.adapter.in.web.AgendamentoEstatisticaDto;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.exception.TperioBusinessException;

public interface RecuperaAgendamentoUseCase {

	List<Agendamento> recuperaAgendamentos(Date data, String pontouuid) throws TperioBusinessException;
	List<Agendamento> recuperaAgendamentos(Date datainicio, Date datafim, boolean aguardandoPar) throws TperioBusinessException;
	List<Agendamento> recuperaAgendamentosPublicador(Date data, String publicadorUuid, Optional<Date> datafim) throws TperioBusinessException;
	AgendamentoEstatisticaDto recuperaAgendamentoEstatisticaPorFiltro(@Valid Optional<String> mesano,
			@Valid Optional<String> circuitoUuid, @Valid Optional<String> regiaoUuid) throws TperioBusinessException;
	
}
