package org.tperio.agendamento.application.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.tperio.agendamento.application.port.in.ApagaAgendamentoUseCase;
import org.tperio.agendamento.application.port.in.AtualizaAgendamentoUseCase;
import org.tperio.agendamento.application.port.out.AtualizaAgendamentoPort;
import org.tperio.agendamento.application.port.out.RecuperaAgendamentoPort;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.message.port.out.MessageBrokerPort;

public class AtualizaAgendamentoService implements AtualizaAgendamentoUseCase, ApagaAgendamentoUseCase {

	private AtualizaAgendamentoPort atualizaAgendamentoPort;
	private RecuperaAgendamentoPort recuperaAgendamentoPort;
	private MessageBrokerPort messageBrokerPort;

	public AtualizaAgendamentoService(
			final AtualizaAgendamentoPort atualizaAgendamentoPort,
			final RecuperaAgendamentoPort recuperaAgendamentoPort,
			final MessageBrokerPort messageBrokerPort) {
		this.atualizaAgendamentoPort = atualizaAgendamentoPort;
		this.recuperaAgendamentoPort = recuperaAgendamentoPort;
		this.messageBrokerPort = messageBrokerPort;
	}

	@Override
	public Agendamento atualizaAgendamento(final Agendamento agendamento) throws TperioBusinessException {
		agendamento.validaAgendamento();
		Agendamento old = recuperaAgendamentoPort.recuperaAgendamentoPorUuid(agendamento.getUuid());
		List<Agendamento> listaAgendamentos = recuperaAgendamentoPort
				.recuperaAgendamentosPorDataPonto(agendamento.getData(), agendamento.getPonto());
		if (listaAgendamentos != null && !listaAgendamentos.isEmpty()) {
			agendamento.validaAgendamento(listaAgendamentos);
		}

		List<Agendamento> listaAgendamentosConflitantes = recuperaAgendamentoPort
				.recuperaAgendamentosPorDataPublicador(agendamento.getData(),
						agendamento.getPrincipal(),
						Optional.empty());
		if (listaAgendamentosConflitantes != null && !listaAgendamentosConflitantes.isEmpty()) {
			agendamento.validaAgendamento(listaAgendamentosConflitantes);
		}
		
		if (old.getPrincipal() != null && !agendamento.possuiPublicador(old.getPrincipal().getUuid())) {
			messageBrokerPort.mensagemCancelamentoAgendamento(old.getPrincipal(), old);
		}
		
		if (old.getAuxiliar() != null && !agendamento.possuiPublicador(old.getAuxiliar().getUuid())) {
			messageBrokerPort.mensagemCancelamentoAgendamento(old.getAuxiliar(), old);
		}
		agendamento.validaPublicadores();
		return atualizaAgendamentoPort.atualizaAgendamento(agendamento);
	}

	@Override
	public void apagaAgendamento(final String uuid) throws TperioBusinessException {
		Agendamento agenda = recuperaAgendamentoPort.recuperaAgendamentoPorUuid(uuid);
		if (agenda == null) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.AGENDAMENTONAOENCONTRADO));
		}
		atualizaAgendamentoPort.desativaAgendamento(uuid);
	}

}
