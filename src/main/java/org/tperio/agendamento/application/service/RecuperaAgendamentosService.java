package org.tperio.agendamento.application.service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.tperio.agendamento.adapter.in.web.AgendamentoEstatisticaDto;
import org.tperio.agendamento.application.port.in.RecuperaAgendamentoUseCase;
import org.tperio.agendamento.application.port.out.RecuperaAgendamentoPort;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.config.MessageKeys;
import org.tperio.evento.adapter.out.persistence.EventoAdapter;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.adapter.out.persistence.PontoAdapter;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.adapter.out.persistence.PublicadorAdapter;
import org.tperio.publicador.domain.Publicador;

public class RecuperaAgendamentosService implements RecuperaAgendamentoUseCase {
	
	private RecuperaAgendamentoPort recuperaAgendamento;
	private PontoAdapter pontoAdapter;
	private PublicadorAdapter publicadorAdapter;
	private EventoAdapter eventoAdapter;
	
	public RecuperaAgendamentosService(
			RecuperaAgendamentoPort recuperaAgendamento,
			PontoAdapter pontoAdapter,
			PublicadorAdapter publicadorAdapter,
			EventoAdapter eventoAdapter) {
		this.recuperaAgendamento = recuperaAgendamento;
		this.pontoAdapter = pontoAdapter;
		this.publicadorAdapter = publicadorAdapter;
		this.eventoAdapter = eventoAdapter;
	}

	@Override
	public List<Agendamento> recuperaAgendamentos(Date data, String pontouuid) throws TperioBusinessException {
		Optional<Ponto> ponto = pontoAdapter.recuperaPonto(pontouuid);
		if(ponto.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PONTONAOENCONTRADO));
		}
		List<Agendamento> agendamentos = recuperaAgendamento.recuperaAgendamentosPorDataPonto(data, ponto.get());
		List<Evento> eventos = eventoAdapter.recuperaEventosAtivos();
		agendamentos.forEach(ag -> ag.getPonto().adicionaEventoAtivo(eventos));
		return agendamentos;
	}

	@Override
	public List<Agendamento> recuperaAgendamentosPublicador(Date data, String publicadorUuid, Optional<Date> datafim)
			throws TperioBusinessException {
		Optional<Publicador> pubopt = publicadorAdapter.recuperaPublicadorUuid(publicadorUuid);
		if(pubopt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PUBLICADORNAOENCONTRADO));
		}
		List<Agendamento> agendamentos = recuperaAgendamento.recuperaAgendamentosPorDataPublicador(data, pubopt.get(), datafim);
		List<Evento> eventos = eventoAdapter.recuperaEventosAtivos();
		agendamentos.forEach(ag -> ag.getPonto().adicionaEventoAtivo(eventos));
		return agendamentos;
	}

	@Override
	public List<Agendamento> recuperaAgendamentos(Date datainicio, Date datafim, boolean aguardandoPar)
			throws TperioBusinessException {
		List<Evento> eventos = eventoAdapter.recuperaEventosAtivos();
		List<Agendamento> agendamentos = recuperaAgendamento.recuperaAgendamentosPorDataInicioeDataFim(datainicio, datafim)
				.stream()
				.collect(Collectors.toList());
		if(aguardandoPar) {
			agendamentos = agendamentos.stream()
					.filter(ag -> ag.getAuxiliar() == null || ag.getPrincipal() == null).collect(Collectors.toList());
		}
		agendamentos.forEach(ag -> ag.getPonto().adicionaEventoAtivo(eventos));
		return agendamentos;
	}

	@Override
	public AgendamentoEstatisticaDto recuperaAgendamentoEstatisticaPorFiltro(@Valid Optional<String> mesano,
			@Valid Optional<String> circuitoUuid, @Valid Optional<String> regiaoUuid) throws TperioBusinessException {
		DateTimeFormatter pattern = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String mesAnoFim = "";
		if(mesano.isEmpty()) {
			LocalDate dataAtual = LocalDate.now();
			int mes = dataAtual.getMonthValue();
			int ano = dataAtual.getYear();
			mesAnoFim = mes + "/" + ano;
		} else {
			mesAnoFim = mesano.get();
		}
		
		LocalDate datetime = LocalDate.parse("01/" + mesAnoFim, pattern);
		LocalDate lastDayOfMonth = datetime
	    	       .with(TemporalAdjusters.lastDayOfMonth());
		List<Agendamento> lista = recuperaAgendamento.recuperaAgendamentosPorDataInicioeDataFim(convertToDateViaInstant(datetime), convertToDateViaInstant(lastDayOfMonth));
		Long totalPubs = 0L;
		Long totalAgend = 0L;
		Long pontos = 0L;
		if(circuitoUuid.isPresent()) {
			List<Publicador> pubs = publicadorAdapter.recuperaPublicadoresPorCircuitoUuid(circuitoUuid.get());
			totalPubs = Long.valueOf(pubs.size());
			totalAgend = Long.valueOf(lista.stream().filter(ag -> ag.getPrincipal().getCongregacao().getCircuito().getUuid().equals(circuitoUuid.get())).toList().size());
		} else {
			List<Publicador> pubs = publicadorAdapter.recuperaPublicadoresAtivos();
			totalPubs = Long.valueOf(pubs.size());
		}

		if(regiaoUuid.isPresent()) {
			totalAgend = Long.valueOf(lista.stream().filter(ag -> ag.getPonto().getRegiao().getUuid().equals(regiaoUuid.get())).toList().size());
		}

		if(regiaoUuid.isEmpty() && circuitoUuid.isEmpty()) {
			totalAgend = (long) lista.size();
		}

		Map<String, Long> contagemPorPonto = lista.stream()
				.collect(Collectors.groupingBy(agendamento -> agendamento.getPonto().getNome(), Collectors.counting()));

		return AgendamentoEstatisticaDto.builder()
				.totalAgendamento(totalAgend)
				.totalPublicadores(totalPubs)
				.contagemPorPonto(contagemPorPonto)
				.build();
	}
	
	private Date convertToDateViaInstant(LocalDate dateToConvert) {
	    return java.util.Date.from(dateToConvert.atStartOfDay()
	      .atZone(ZoneId.systemDefault())
	      .toInstant());
	}

}
