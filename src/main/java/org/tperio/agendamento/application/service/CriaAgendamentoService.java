package org.tperio.agendamento.application.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.tperio.agendamento.application.port.in.CriaNovoAgendamentoUseCase;
import org.tperio.agendamento.application.port.out.CriaNovoAgendamentoPort;
import org.tperio.agendamento.application.port.out.RecuperaAgendamentoPort;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.message.port.out.MessageBrokerPort;
import org.tperio.ponto.application.service.PontoService;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.application.service.PublicadorService;
import org.tperio.publicador.domain.Publicador;

public class CriaAgendamentoService implements CriaNovoAgendamentoUseCase {

	private RecuperaAgendamentoPort recuperaAgendamento;
	private CriaNovoAgendamentoPort criaNovoAgendamento;
	private MessageBrokerPort messageBrokerPort;
	private PontoService pontoService;
	private PublicadorService publicadorService;

	public CriaAgendamentoService(
			final RecuperaAgendamentoPort recuperaAgendamento,
			final CriaNovoAgendamentoPort criaNovoAgendamento,
			final MessageBrokerPort messageBrokerPort,
			final PontoService pontoService,
			final PublicadorService publicadorService) {
		this.criaNovoAgendamento = criaNovoAgendamento;
		this.recuperaAgendamento = recuperaAgendamento;
		this.messageBrokerPort = messageBrokerPort;
		this.pontoService = pontoService;
		this.publicadorService = publicadorService;
	}

	@Override
	public Agendamento criaNovoAgendamento(final Agendamento agendamento) throws TperioBusinessException {
		agendamento.validaAgendamento();
		Ponto ponto = pontoService.recuperaPontoPorUuid(agendamento.getPonto().getUuid());
		agendamento.setPonto(ponto);
		
		Publicador principal = publicadorService.recuperaPublicadorUuid(agendamento.getPrincipal().getUuid());
		agendamento.setPrincipal(principal);
		if (!Objects.isNull(agendamento.getAuxiliar())) {
			Publicador auxiliar = publicadorService.recuperaPublicadorUuid(agendamento.getAuxiliar().getUuid());
			agendamento.setAuxiliar(auxiliar);
		}
		agendamento.validaPublicadores();
		
		Publicador pub = publicadorService.recuperaPublicadorUuid(agendamento.getPublicador().getUuid());
		if (pub == null) {
			new TperioBusinessException(Arrays.asList(MessageKeys.PUBLICADORNAOENCONTRADO));
		}
		List<Agendamento> listaAgendamentos = recuperaAgendamento
				.recuperaAgendamentosPorDataPonto(agendamento.getData(), agendamento.getPonto());
		if (listaAgendamentos != null && !listaAgendamentos.isEmpty()) {
			agendamento.validaAgendamento(listaAgendamentos);
		}

		List<Agendamento> listaAgendamentosConflitantes = recuperaAgendamento
				.recuperaAgendamentosPorDataPublicador(agendamento.getData(),
						agendamento.getPrincipal(),
						Optional.empty());
		if (listaAgendamentosConflitantes != null && !listaAgendamentosConflitantes.isEmpty()) {
			agendamento.validaAgendamento(listaAgendamentosConflitantes);
		}

		agendamento.setDatacriacao(new Date());
		agendamento.setUuid(UUID.randomUUID().toString());
		Agendamento agendamentoFim = criaNovoAgendamento.criaNovoAgendamento(agendamento);
		messageBrokerPort.mensagemAgendamentoFeitoComSucesso(agendamentoFim.getPrincipal(), agendamentoFim);
		return agendamentoFim;
	}

}
