package org.tperio.agendamento.domain;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.tperio.StringUteis;
import org.tperio.config.MessageKeys;
import org.tperio.config.Messages;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.domain.Publicador;

public final class Agendamento {

	private String uuid;
	private String horainicio;
	private String horafim;
	private Date data;
	private Publicador principal;
	private Publicador auxiliar;
	private Publicador publicador;
	private Ponto ponto;
	private Date datacriacao;
	private Date datadesativado;

	public void validaAgendamento() throws TperioBusinessException {
		List<String> sb = new ArrayList<>();
		if (Objects.isNull(horainicio)) {
			adicionaMensagem(sb, MessageKeys.PREENCHAHORAINICIO);
		}
		if (Objects.isNull(horafim)) {
			adicionaMensagem(sb, MessageKeys.PREENCHAHORAFIM);
		}
		if (Objects.isNull(data)) {
			adicionaMensagem(sb, MessageKeys.PREENCHADATA);
		}
		if (Objects.isNull(principal)) {
			adicionaMensagem(sb, MessageKeys.PREENCHAPRINCIPAL);
		}
		if (Objects.isNull(ponto)) {
			adicionaMensagem(sb, MessageKeys.PREENCHAPONTO);
		}
		if (!sb.isEmpty()) {
			throw new TperioBusinessException(sb);
		}
	}

	private void adicionaMensagem(List<String> sb, String key) {
		sb.add(Messages.getInstance().getMessage(key));
	}

	public String getUuid() {
		return uuid;
	}
	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}
	public String getHorainicio() {
		return horainicio;
	}
	public void setHorainicio(final String horainicio) {
		this.horainicio = horainicio;
	}
	public String getHorafim() {
		return horafim;
	}
	public void setHorafim(final String horafim) {
		this.horafim = horafim;
	}
	public Date getData() {
		return data;
	}
	public void setData(final Date data) {
		this.data = data;
	}
	public Publicador getPrincipal() {
		return principal;
	}
	public void setPrincipal(final Publicador principal) {
		this.principal = principal;
	}
	public Publicador getAuxiliar() {
		return auxiliar;
	}
	public void setAuxiliar(final Publicador auxiliar) {
		this.auxiliar = auxiliar;
	}
	public Publicador getPublicador() {
		return publicador;
	}
	public void setPublicador(final Publicador publicador) {
		this.publicador = publicador;
	}
	public Ponto getPonto() {
		return ponto;
	}
	public void setPonto(final Ponto ponto) {
		this.ponto = ponto;
	}
	public Date getDatacriacao() {
		return datacriacao;
	}
	public void setDatacriacao(final Date datacriacao) {
		this.datacriacao = datacriacao;
	}
	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(final Date datadesativado) {
		this.datadesativado = datadesativado;
	}

	private LocalTime getLocalTime(final String hour) {
		return LocalTime.of(Integer.parseInt(hour), Integer.parseInt(hour));
	}

	private boolean horaContainsHora(final LocalTime inicioUm,
			final LocalTime fimUm,
			final LocalTime inicioDois,
			final LocalTime fimDois) {
		return inicioDois.isAfter(inicioUm) && fimDois.isBefore(fimUm);
	}

	private boolean ehEdicao() {
		return this.uuid != null;
	}

	/**
	 * Este método verifica se o agendamento tem horário conflitante em uma lista de agendamentos.
	 * */
	public void validaAgendamento(final List<Agendamento> listaAgendamentos) throws TperioBusinessException {
		LocalTime inicioAg = getLocalTime(this.getHorainicio());
		LocalTime fimAg = getLocalTime(this.getHorafim());
		for (Agendamento ag : listaAgendamentos) {
			if (ehEdicao() && ag.getUuid().equals(this.getUuid())) {
				continue;
			}
			LocalTime inicio = getLocalTime(ag.getHorainicio());
			LocalTime fim = getLocalTime(ag.getHorafim());
			if ((inicioAg.isAfter(inicio) && inicioAg.isBefore(fim))
				|| (fimAg.isAfter(inicio) && fimAg.isBefore(fim))
				|| fimAg.equals(fim)
				|| horaContainsHora(inicioAg, fimAg, inicio, fim)
				|| horaContainsHora(inicio, fim, inicioAg, fimAg)) {
				throw new TperioBusinessException(Arrays.asList(MessageKeys.AGENDAMENTOJAEXISTE));
			}
		}
	}
	
	public boolean possuiPublicador(String uuid) {
		if (this.principal != null && !StringUteis.isNullOrEmpty(this.principal.getUuid())
				&& this.principal.getUuid().equals(uuid)) {
			return true;
		}
		if (this.auxiliar != null && !StringUteis.isNullOrEmpty(this.auxiliar.getUuid())
				&& this.auxiliar.getUuid().equals(uuid)) {
			return true;
		}
		return false;
	}
	
	private boolean publicadorPodeAgendar(Publicador pub, String eventoUuid) {
		return !Objects.isNull(pub)
				&& pub.possuiEventoAtivo()
				&& pub.estaNoEvento(eventoUuid);
	}

	public void validaPublicadores() throws TperioBusinessException {
		if (this.ponto.pontoPossuiEventoAtivo()) {
			boolean podeAgendarPrincipal = false;
			boolean podeAgendarAuxiliar = false;

			for (Evento evento : this.ponto.getEventosAtivos()) {
				String eventoAtivoUuid = evento.getUuid();

				if (publicadorPodeAgendar(this.principal, eventoAtivoUuid)) {
					podeAgendarPrincipal = true;
				}

				if (!Objects.isNull(this.auxiliar) && publicadorPodeAgendar(this.auxiliar, eventoAtivoUuid)) {
					podeAgendarAuxiliar = true;
				}
			}

			if (!podeAgendarPrincipal) {
				throw new TperioBusinessException(Arrays.asList(MessageKeys.AGENDAMENTONAOAUTORIZADO));
			}

			if (!Objects.isNull(this.auxiliar) && !podeAgendarAuxiliar) {
				throw new TperioBusinessException(Arrays.asList(MessageKeys.AGENDAMENTOAUXNAOAUTORIZADO));
			}
		}
	}

}
