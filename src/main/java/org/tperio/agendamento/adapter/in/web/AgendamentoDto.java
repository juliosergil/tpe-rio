package org.tperio.agendamento.adapter.in.web;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.config.MessageKeys;
import org.tperio.ponto.adapter.in.web.PontoDto;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.adapter.in.web.PublicadorDto;
import org.tperio.publicador.domain.Publicador;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class AgendamentoDto {

	private String uuid;
	
	@Pattern(regexp = "^(?:([01]?\\d|2[0-3]))$", message = MessageKeys.HORAINICIOFORMATOINVALIDO)
	@NotNull(message = MessageKeys.PREENCHAHORAINICIO)
	private String horainicio;
	
	@Pattern(regexp = "^(?:([01]?\\d|2[0-3]))$", message = MessageKeys.HORAFIMFORMATOINVALIDO)
	@NotNull(message = MessageKeys.PREENCHAHORAFIM)
	private String horafim;
	
	private String publicadoruuid;
	
	@NotNull(message = MessageKeys.PREENCHAPRINCIPAL)
	private String principaluuid;
	
	private String auxiliaruuid;
	
	@NotNull(message = MessageKeys.PREENCHAPONTO)
	private String pontouuid;
	
	@JsonFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = MessageKeys.PREENCHADATA)
	private LocalDate data;
	
	private PublicadorDto principal;
	private PublicadorDto auxiliar;
	private PublicadorDto publicador;
	private PontoDto ponto;
	private Date datacriacao;
	private Date datadesativado;
	
	public Agendamento toAgendamento() {
		Agendamento agendamento = new Agendamento();
		BeanUtils.copyProperties(this, agendamento);
		Publicador pub = new Publicador();
		pub.setUuid(publicadoruuid);
		agendamento.setPublicador(pub);
		agendamento.setData(convertToDateUsingInstant(data));
		Publicador principalP = new Publicador();
		principalP.setUuid(principaluuid);
		agendamento.setPrincipal(principalP);
		if(!ObjectUtils.isEmpty(auxiliaruuid)) {
			Publicador auxiliarP = new Publicador();
			auxiliarP.setUuid(auxiliaruuid);
			agendamento.setAuxiliar(auxiliarP);
		}
		Ponto pontoP = new Ponto(pontouuid);
		agendamento.setPonto(pontoP);
		return agendamento;
	}
	
	public static Date convertToDateUsingInstant(LocalDate date) {
        return java.util.Date.from(date.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }
	
	public static LocalDate convertToLocalDate(Date date) {
		return Instant.ofEpochMilli(date.getTime())
			      .atZone(ZoneId.systemDefault())
			      .toLocalDate();
	}
	
	public static AgendamentoDto fromAgendamento(Agendamento agendamento) {
		AgendamentoDto dto = AgendamentoDto
				.builder()
				.uuid(agendamento.getUuid())
				.principal(PublicadorDto.fromPublicador(agendamento.getPrincipal()))
				.ponto(PontoDto.fromPonto(agendamento.getPonto()))
				.data(convertToLocalDate(agendamento.getData()))
				.horainicio(agendamento.getHorainicio())
				.horafim(agendamento.getHorafim())
				.build();
		
		if(!ObjectUtils.isEmpty(agendamento.getAuxiliar())) {
			dto.setAuxiliar(PublicadorDto.fromPublicador(agendamento.getAuxiliar()));
		}
		return dto;
	}
}
