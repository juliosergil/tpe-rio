package org.tperio.agendamento.adapter.in.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.tperio.ponto.domain.Ponto;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class AgendamentoEstatisticaDto {
	private Long totalPublicadores;
	private Long totalAgendamento;
	private String mesAno;
	private String circuitouuid;
	private Map<String, Long> contagemPorPonto;
}
