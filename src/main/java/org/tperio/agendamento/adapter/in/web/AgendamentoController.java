package org.tperio.agendamento.adapter.in.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.tperio.agendamento.application.port.in.ApagaAgendamentoUseCase;
import org.tperio.agendamento.application.port.in.AtualizaAgendamentoUseCase;
import org.tperio.agendamento.application.port.in.CriaNovoAgendamentoUseCase;
import org.tperio.agendamento.application.port.in.RecuperaAgendamentoUseCase;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.exception.TperioBusinessException;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/agendamento")
public class AgendamentoController {
	
	@Autowired private CriaNovoAgendamentoUseCase criaNovoAgendamentoUseCase;
	@Autowired private AtualizaAgendamentoUseCase atualizaAgendamentoUseCase;
	@Autowired private ApagaAgendamentoUseCase apagaAgendamentoUseCase;
	@Autowired private RecuperaAgendamentoUseCase recuperaAgendamentoUseCase;

	@PostMapping(path = "", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)

    public AgendamentoDto criaNovoAgendamento(
    		@Valid @RequestBody AgendamentoDto agendamentoDto, 
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException{
		Agendamento agendamento = criaNovoAgendamentoUseCase.criaNovoAgendamento(agendamentoDto.toAgendamento());
    	return AgendamentoDto.fromAgendamento(agendamento);
    }
	
	@PutMapping(path = "/{uuid}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)

    public AgendamentoDto atualizaAgendamento(
    		@PathVariable String uuid,
    		@Valid @RequestBody AgendamentoDto agendamentoDto,
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException{
		agendamentoDto.setUuid(uuid);
		Agendamento agendamento = atualizaAgendamentoUseCase.atualizaAgendamento(agendamentoDto.toAgendamento());
    	return AgendamentoDto.fromAgendamento(agendamento);
    }
	
	@SuppressWarnings("rawtypes")
	@DeleteMapping(path = "/{uuid}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)

    public ResponseEntity apagaAgendamento(
    		@PathVariable String uuid,
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException{
		apagaAgendamentoUseCase.apagaAgendamento(uuid);
    	return ResponseEntity.ok().build();
    }
	
	@GetMapping(path = "/ponto/{pontouuid}",
			produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AgendamentoDto> recuperaAgendamentos(
    		@PathVariable String pontouuid, 
            @RequestParam(name = "data") @DateTimeFormat(pattern = "dd/MM/yyyy") Date data,
            @AuthenticationPrincipal UserDetails user)
            		throws TperioBusinessException {
		List<Agendamento> listaAgendamento = recuperaAgendamentoUseCase.recuperaAgendamentos(data, pontouuid);
		return listaAgendamento.stream().map(AgendamentoDto::fromAgendamento).collect(Collectors.toList());
    }
	
	@GetMapping(path = "",
			produces = MediaType.APPLICATION_JSON_VALUE)

    public List<AgendamentoDto> recuperaAgendamentos(
            @RequestParam(name = "datainicio") @DateTimeFormat(pattern = "dd/MM/yyyy") Date datainicio,
            @RequestParam(name = "datafim") @DateTimeFormat(pattern = "dd/MM/yyyy") Date datafim,
            @RequestParam(name = "aguardandopar") boolean aguardandopar,
            @AuthenticationPrincipal UserDetails user)
            		throws TperioBusinessException {
		List<Agendamento> listaAgendamento = recuperaAgendamentoUseCase.recuperaAgendamentos(datainicio, datafim, aguardandopar);
		return listaAgendamento.stream().map(AgendamentoDto::fromAgendamento).collect(Collectors.toList());
    }
	
	@GetMapping(path = "/publicador/{publicadoruuid}",
			produces = MediaType.APPLICATION_JSON_VALUE)

    public List<AgendamentoDto> recuperaAgendamentosPublicador(
    		@PathVariable String publicadoruuid,
            @RequestParam(name = "data") @DateTimeFormat(pattern = "dd/MM/yyyy") Date data,
            @RequestParam(name = "datafim") @DateTimeFormat(pattern = "dd/MM/yyyy") Optional<Date> datafim,
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException {
		List<Agendamento> listaAgendamento = recuperaAgendamentoUseCase.recuperaAgendamentosPublicador(data, publicadoruuid, datafim);
		return listaAgendamento.stream().map(AgendamentoDto::fromAgendamento).collect(Collectors.toList());
    }
	
	@GetMapping(path = "/estatisticas", produces = MediaType.APPLICATION_JSON_VALUE)
	public AgendamentoEstatisticaDto recuperaPublicador(
			@Valid @RequestParam("mes") Optional<String> mesano,
			@Valid @RequestParam("circuito") Optional<String> circuitoUuid,
			@Valid @RequestParam("regiao") Optional<String> regiaoUuid) throws TperioBusinessException {
		return recuperaAgendamentoUseCase.recuperaAgendamentoEstatisticaPorFiltro(mesano, circuitoUuid, regiaoUuid);
	}
}
