package org.tperio.agendamento.adapter.out.persistence;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.tperio.agendamento.application.port.out.AtualizaAgendamentoPort;
import org.tperio.agendamento.application.port.out.CriaNovoAgendamentoPort;
import org.tperio.agendamento.application.port.out.RecuperaAgendamentoPort;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.adapter.out.persistence.PontoAdapter;
import org.tperio.ponto.adapter.out.persistence.PontoJpaEntity;
import org.tperio.ponto.adapter.out.persistence.PontoRepository;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.adapter.out.persistence.PublicadorAdapter;
import org.tperio.publicador.adapter.out.persistence.PublicadorJpaEntity;
import org.tperio.publicador.adapter.out.persistence.PublicadorRepository;
import org.tperio.publicador.domain.Publicador;

@Component
public class AgendamentoAdapter implements CriaNovoAgendamentoPort, RecuperaAgendamentoPort, AtualizaAgendamentoPort {
	
	@Autowired private AgendamentoRepository agendamentoRepository;
	@Autowired private PontoRepository pontoRepository;
	@Autowired private PublicadorRepository publicadorRepository;
	@Autowired private PublicadorAdapter publicadorAdapter;
	@Autowired private PontoAdapter pontoAdapter;

	@Override
	public List<Agendamento> recuperaAgendamentosPorDataPonto(Date data, Ponto ponto) throws TperioBusinessException {
		Optional<PontoJpaEntity> pontoJpa = pontoRepository.findByUuid(ponto.getUuid());
		if(pontoJpa.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PONTONAOENCONTRADO));
		}
		
		List<AgendamentoJpaEntity> lista = agendamentoRepository.findByDataAndPonto(data, pontoJpa.get());
		
		return lista.stream().filter(age -> age.getDatadesativado() == null).map(this::toAgendamento).collect(Collectors.toList());
	}

	@Override
	public Agendamento criaNovoAgendamento(Agendamento agendamento) throws TperioBusinessException {
		AgendamentoJpaEntity entity = validaEMontaEntidade(agendamento);
		agendamentoRepository.save(entity);
		agendamento = toAgendamento(entity);
		return agendamento;
	}

	private AgendamentoJpaEntity validaEMontaEntidade(Agendamento agendamento) throws TperioBusinessException {
		PublicadorJpaEntity principal = publicadorRepository.findByUuid(agendamento.getPrincipal().getUuid())
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PRINCIPALNAOENCONTRADO)));
		
		PublicadorJpaEntity publicador = publicadorRepository.findByUuid(agendamento.getPublicador().getUuid())
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PUBLICADORNAOENCONTRADO)));
		
		PontoJpaEntity ponto = pontoRepository.findByUuid(agendamento.getPonto().getUuid())
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PONTONAOENCONTRADO)));
		AgendamentoJpaEntity entity = fromAgendamento(agendamento);
		entity.setPublicador(publicador);
		entity.setPrincipal(principal);
		entity.setPonto(ponto);
		if(!ObjectUtils.isEmpty(agendamento.getAuxiliar())) {
			PublicadorJpaEntity auxiliar = publicadorRepository.findByUuid(agendamento.getAuxiliar().getUuid())
					.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.AUXILIARNAOENCONTRADO)));
			entity.setAuxiliar(auxiliar);
		}
		return entity;
	}

	public AgendamentoJpaEntity fromAgendamento(Agendamento agendamento) {
		AgendamentoJpaEntity entity = new AgendamentoJpaEntity();
		BeanUtils.copyProperties(agendamento, entity);
		return entity;
	}

	public Agendamento toAgendamento(AgendamentoJpaEntity entity) {
		Agendamento agendamento = new Agendamento();
		BeanUtils.copyProperties(entity, agendamento);
		agendamento.setPrincipal(publicadorAdapter.toPublicador(entity.getPrincipal()));
		if(!ObjectUtils.isEmpty(entity.getAuxiliar())) {
			agendamento.setAuxiliar(publicadorAdapter.toPublicador(entity.getAuxiliar()));
		}
		agendamento.setPonto(pontoAdapter.toPonto(entity.getPonto()));
		return agendamento;
	}

	@Override
	public List<Agendamento> recuperaAgendamentosPorDataPublicador(Date data,
			final Publicador publicador,
			final Optional<Date> datafim)
			throws TperioBusinessException {
		Optional<PublicadorJpaEntity> publicadoropt = publicadorRepository.findByUuid(publicador.getUuid());
		if(publicadoropt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PUBLICADORNAOENCONTRADO));
		}

		if (datafim.isPresent()) {
			return agendamentoRepository.findByDataAndPublicadorBetweenOrderByData(data, publicadoropt.get(), datafim.get())
					.stream()
					.filter(ag -> ag.getDatadesativado() == null)
					.map(this::toAgendamento)
					.collect(Collectors.toList());
		}

		return agendamentoRepository.findByDataAndPublicadorOrderByData(Optional.of(data), publicadoropt.get())
				.stream()
				.filter(ag -> ag.getDatadesativado() == null)
				.map(this::toAgendamento)
				.collect(Collectors.toList());
	}

	@Override
	public Agendamento recuperaAgendamentoPorUuid(String uuid) throws TperioBusinessException {
		AgendamentoJpaEntity agendamento = agendamentoRepository.findByUuid(uuid)
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.AGENDAMENTONAOENCONTRADO)));
		return this.toAgendamento(agendamento);
	}

	@Override
	public Agendamento atualizaAgendamento(Agendamento agendamento) throws TperioBusinessException {
		AgendamentoJpaEntity agendamentoEntityOrig = agendamentoRepository.findByUuid(agendamento.getUuid())
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.AGENDAMENTONAOENCONTRADO)));
		Long id = agendamentoEntityOrig.getIdagendamento();
		Date dataCriacao = agendamentoEntityOrig.getDatacriacao();
		AgendamentoJpaEntity agendamentoEntity = validaEMontaEntidade(agendamento);
		BeanUtils.copyProperties(agendamentoEntity, agendamentoEntityOrig);
		agendamentoEntityOrig.setIdagendamento(id);
		agendamentoEntityOrig.setDatacriacao(dataCriacao);
		agendamentoRepository.save(agendamentoEntityOrig);
		return toAgendamento(agendamentoEntityOrig);
	}

	@Override
	public void desativaAgendamento(String uuid) throws TperioBusinessException {
		AgendamentoJpaEntity agendamentoEntityOrig = agendamentoRepository.findByUuid(uuid)
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.AGENDAMENTONAOENCONTRADO)));
		agendamentoEntityOrig.setDatadesativado(new Date());
		agendamentoRepository.save(agendamentoEntityOrig);
	}

	@Override
	public List<Agendamento> recuperaAgendamentosPorDataInicioeDataFim(Date datainicio, Date datafim)
			throws TperioBusinessException {
		return agendamentoRepository.findAllByDataBetweenOrderByData(datainicio, datafim)
				.stream()
				.filter(ag -> ag.getDatadesativado() == null)
				.map(this::toAgendamento)
				.collect(Collectors.toList());
	}

}
