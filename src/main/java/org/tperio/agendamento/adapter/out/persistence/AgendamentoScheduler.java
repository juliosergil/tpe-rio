package org.tperio.agendamento.adapter.out.persistence;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.ponto.adapter.out.persistence.PontoAdapter;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.domain.Publicador;
import org.tperio.service.EmailBroker;
import org.tperio.service.TelegramMessager;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class AgendamentoScheduler {
	
	private final AgendamentoAdapter agendamentoAdapter;
	private final PontoAdapter pontoAdapter;
	private final EmailBroker emailBroker;
	private final TelegramMessager messager;

	@Scheduled(cron = "0 59 23 * * *")
	@Async
	public void programacaoDiaria() {
		List<Ponto> pontos = pontoAdapter.recuperaPontosAtivos();
		pontos.stream().forEach(this::trataPonto);
	}
	
	private void trataPonto(Ponto ponto) {
		LocalDate today = LocalDate.now();
		LocalDate tomorrow = today.plusDays(1);
		Date dateF = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
		try {
			List<Agendamento> agend = agendamentoAdapter.recuperaAgendamentosPorDataPonto(dateF, ponto);
			List<String> emails = recuperaEmails(agend);
			emails.stream().forEach(email -> emailBroker.enviaEmailProgramacaoDia(email, agend, dateF, ponto));
			List<Publicador> pubComTelegram = recuperaPublicadoresContatos(agend);
			pubComTelegram.stream().forEach(pub -> messager.enviaMensagem(
					pub.getUuid(), 
					"Olá " + pub.getNome() + ". Lhe enviamos um email com a programação do dia "
					+ new SimpleDateFormat("dd/MM/yyyy").format(dateF)  + ", onde o irmão agendou trabalhar."));
			
		} catch (Exception e) {
			log.info("enviando emails com a programação diária {} ", LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
		}
	}
	
	private List<String> recuperaEmails(List<Agendamento> agendamentos) {
		List<String> emails = agendamentos.stream().map(agenda -> agenda.getPrincipal().getEmail()).distinct().collect(Collectors.toList());
		emails.addAll(agendamentos.stream().map(agenda -> agenda.getAuxiliar().getEmail()).distinct().collect(Collectors.toList()));
		return emails.stream().filter(str -> !ObjectUtils.isEmpty(str)).distinct().collect(Collectors.toList());
	}
	
	private List<Publicador> recuperaPublicadoresContatos(List<Agendamento> agendamentos) {
		List<Publicador> publicadores = agendamentos.stream().map(Agendamento::getPrincipal).distinct().collect(Collectors.toList());
		publicadores.addAll(agendamentos.stream().map(Agendamento::getAuxiliar).distinct().collect(Collectors.toList()));
		return publicadores.stream().filter(str -> !ObjectUtils.isEmpty(str.getContato())).distinct().collect(Collectors.toList());
	}
}
