package org.tperio.agendamento.adapter.out.persistence;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import org.tperio.ponto.adapter.out.persistence.PontoJpaEntity;
import org.tperio.publicador.adapter.out.persistence.PublicadorJpaEntity;

@Entity
@Table(name = "agendamento")
class AgendamentoJpaEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idagendamento",unique = true)
	private Long idagendamento;
	
	private String uuid = UUID.randomUUID().toString();
	
	private String horainicio;
	
	private String horafim;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data")
	private Date data;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "idponto", referencedColumnName = "idponto")
	private PontoJpaEntity ponto;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "idprincipal", referencedColumnName = "idpublicador")
	private PublicadorJpaEntity principal;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true)
	@JoinColumn(name = "idauxiliar", referencedColumnName = "idpublicador")
	private PublicadorJpaEntity auxiliar;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "idpublicador", referencedColumnName = "idpublicador")
	private PublicadorJpaEntity publicador;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datacriacao")
	private Date datacriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datadesativado")
	private Date datadesativado;
	
	public String getHorainicio() {
		return horainicio;
	}

	public void setHorainicio(String horainicio) {
		this.horainicio = horainicio;
	}

	public String getHorafim() {
		return horafim;
	}

	public void setHorafim(String horafim) {
		this.horafim = horafim;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public PontoJpaEntity getPonto() {
		return ponto;
	}

	public void setPonto(PontoJpaEntity ponto) {
		this.ponto = ponto;
	}

	public PublicadorJpaEntity getPrincipal() {
		return principal;
	}

	public void setPrincipal(PublicadorJpaEntity principal) {
		this.principal = principal;
	}

	public PublicadorJpaEntity getAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar(PublicadorJpaEntity auxiliar) {
		this.auxiliar = auxiliar;
	}

	public PublicadorJpaEntity getPublicador() {
		return publicador;
	}

	public void setPublicador(PublicadorJpaEntity publicador) {
		this.publicador = publicador;
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

	public Long getIdagendamento() {
		return idagendamento;
	}

	public void setIdagendamento(Long idagendamento) {
		this.idagendamento = idagendamento;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
