package org.tperio.agendamento.adapter.out.persistence;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.tperio.ponto.adapter.out.persistence.PontoJpaEntity;
import org.tperio.publicador.adapter.out.persistence.PublicadorJpaEntity;

interface AgendamentoRepository extends JpaRepository<AgendamentoJpaEntity, Long> {

	List<AgendamentoJpaEntity> findByDataAndPonto(Date data, PontoJpaEntity ponto);

	List<AgendamentoJpaEntity> findAllByDataBetweenOrderByData(Date datainicio, Date datafim);

	@Query("FROM AgendamentoJpaEntity i WHERE (i.principal = :pubid OR i.auxiliar = :pubid) AND (i.data = :data OR :data is null) ORDER BY i.data")
	List<AgendamentoJpaEntity> findByDataAndPublicadorOrderByData(@Param("data") Optional<Date> data, @Param("pubid") PublicadorJpaEntity pubjpa);
	
	@Query("FROM AgendamentoJpaEntity i WHERE (i.principal = :pubid OR i.auxiliar = :pubid) AND i.data between :data AND :datafim ORDER BY i.data")
	List<AgendamentoJpaEntity> findByDataAndPublicadorBetweenOrderByData(@Param("data") Date data, @Param("pubid") PublicadorJpaEntity pubjpa, @Param("datafim") Date datafim);

	Optional<AgendamentoJpaEntity> findByUuid(String uuid);
	
	@Query("FROM AgendamentoJpaEntity i WHERE i.data between :data AND :datafim ORDER BY i.data")
	List<AgendamentoJpaEntity> findByDataBetweenOrderByData(@Param("data") Date data, @Param("datafim") Date datafim);

}
