package org.tperio.message.port.out;

import org.tperio.agendamento.domain.Agendamento;
import org.tperio.publicador.domain.Publicador;

public interface MessageBrokerPort {

	void mensagemCancelamentoAgendamento(Publicador principal, Agendamento agendamento);
	void mensagemAgendamentoFeitoComSucesso(Publicador principal, Agendamento agendamento);

}
