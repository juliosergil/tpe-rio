package org.tperio.message.adapter.out;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.message.port.out.MessageBrokerPort;
import org.tperio.publicador.domain.Publicador;
import org.tperio.service.EmailBroker;
import org.tperio.service.TelegramMessager;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class MessageBroker implements MessageBrokerPort {

	private final EmailBroker emailBroker;
	private final TelegramMessager telegramMessager;

	@Override
	public void mensagemCancelamentoAgendamento(final Publicador destinatario, final Agendamento agendamento) {
		emailBroker.enviaEmailCancelamento(destinatario, agendamento);
		if (!ObjectUtils.isEmpty(destinatario.getContato())
				&& !ObjectUtils.isEmpty(destinatario.getContato().getTelegram())) {
			telegramMessager.enviaMensagem(destinatario.getContato().getTelegram(),
					"Prezado, enviamos um email avisando que um agendamento seu foi cancelado.");
		}
	}

	@Override
	public void mensagemAgendamentoFeitoComSucesso(final Publicador destinatario, final Agendamento agendamento) {
		emailBroker.enviaEmailAgendamento(agendamento);
		if (!ObjectUtils.isEmpty(destinatario.getContato())
				&& !ObjectUtils.isEmpty(destinatario.getContato().getTelegram())) {
			telegramMessager.enviaMensagem(destinatario.getContato().getTelegram(),
					"Prezado(a), enviamos um email com a confirmação do agendamento que foi feito.");
		}
	}

}
