package org.tperio.congregacao.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import org.tperio.StringUteis;
import org.tperio.circuito.domain.Circuito;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.domain.Regiao;

public class Congregacao {
	
	private String uuid = UUID.randomUUID().toString();
	private String nome;
	private String numero;
	private Regiao regiao;
	private Date dataCriacao = new Date();
	private Date datadesativado;
	private Circuito circuito;

	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Date getDataCriacao() {
		return dataCriacao;
	}
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}
	public Date getDatadesativado() {
		return datadesativado;
	}
	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Regiao getRegiao() {
		return regiao;
	}
	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	public Circuito getCircuito() {
		return circuito;
	}
	public void setCircuito(Circuito circuito) {
		this.circuito = circuito;
	}
	
	public void valida() throws TperioBusinessException {
		if(this.getCircuito() == null 
			|| StringUteis.isNullOrEmpty(this.getCircuito().getUuid())) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PREENCHACIRCUITO));
		}
		if(this.getRegiao() == null 
			|| StringUteis.isNullOrEmpty(this.getRegiao().getUuid())) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PREENCHAREGIAO));
		}
		if(StringUteis.isNullOrEmpty(this.getNome())) {
				throw new TperioBusinessException(Arrays.asList(MessageKeys.PREENCHANOME));
		}
		
	}

}
