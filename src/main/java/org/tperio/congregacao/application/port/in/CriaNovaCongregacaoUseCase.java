package org.tperio.congregacao.application.port.in;

import org.tperio.congregacao.domain.Congregacao;
import org.tperio.exception.TperioBusinessException;

public interface CriaNovaCongregacaoUseCase {
	
	Congregacao criaNovaCongregacao(Congregacao congregacao) throws TperioBusinessException;

}
