package org.tperio.congregacao.application.port.in;

import java.util.List;

import org.tperio.congregacao.domain.Congregacao;
import org.tperio.exception.TperioBusinessException;

public interface RecuperaCongregacoesUseCase {
	
	List<Congregacao> recuperaCongregacoesAtivas();
	Congregacao recuperaCongregacaoPorUuid(String uuid) throws TperioBusinessException;

}
