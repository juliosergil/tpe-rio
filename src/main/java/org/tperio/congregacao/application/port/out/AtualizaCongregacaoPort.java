package org.tperio.congregacao.application.port.out;

import org.tperio.congregacao.domain.Congregacao;
import org.tperio.exception.TperioBusinessException;

public interface AtualizaCongregacaoPort {
	
	Congregacao atualizaCongregacao(Congregacao congregacao) throws TperioBusinessException;

	void desativaCongregacao(String uuid) throws TperioBusinessException;

}
