package org.tperio.congregacao.application.port.out;

import org.tperio.congregacao.domain.Congregacao;
import org.tperio.exception.TperioBusinessException;

public interface CriaNovaCongregacaoPort {

	Congregacao criaNovaCongregacao(Congregacao congregacao) throws TperioBusinessException;
	
}
