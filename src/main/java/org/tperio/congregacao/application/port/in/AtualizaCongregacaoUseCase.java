package org.tperio.congregacao.application.port.in;

import org.tperio.congregacao.domain.Congregacao;
import org.tperio.exception.TperioBusinessException;

public interface AtualizaCongregacaoUseCase {
	
	Congregacao atualizaCongregacao(Congregacao congregacao) throws TperioBusinessException;
	void apagaPublicador(String uuid) throws TperioBusinessException;

}
