package org.tperio.congregacao.application.port.out;

import java.util.List;
import java.util.Optional;

import org.tperio.congregacao.domain.Congregacao;

public interface RecuperaCongregacoesPort {

	List<Congregacao> recuperaCongregacoesAtivas();
	
	Optional<Congregacao> recuperaCongregacaoPorNomeNumero(String nome, String numero);
	
	Optional<Congregacao> recuperaCongregacaoPorUuid(String uuid);

}

