package org.tperio.congregacao.application.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.tperio.circuito.application.port.out.RecuperaCircuitosPort;
import org.tperio.circuito.domain.Circuito;
import org.tperio.config.MessageKeys;
import org.tperio.congregacao.application.port.in.AtualizaCongregacaoUseCase;
import org.tperio.congregacao.application.port.in.CriaNovaCongregacaoUseCase;
import org.tperio.congregacao.application.port.in.RecuperaCongregacoesUseCase;
import org.tperio.congregacao.application.port.out.AtualizaCongregacaoPort;
import org.tperio.congregacao.application.port.out.CriaNovaCongregacaoPort;
import org.tperio.congregacao.application.port.out.RecuperaCongregacoesPort;
import org.tperio.congregacao.domain.Congregacao;
import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.application.port.out.RecuperaRegiaoPort;
import org.tperio.regiao.domain.Regiao;

public class CongregacaoService implements RecuperaCongregacoesUseCase, CriaNovaCongregacaoUseCase, AtualizaCongregacaoUseCase {
	
	private CriaNovaCongregacaoPort criaNovaCongregacaoPort;
	private RecuperaCongregacoesPort recuperaCongregacoesPort;
	private RecuperaRegiaoPort recuperaRegiaoPort;
	private AtualizaCongregacaoPort atualizaCongregacaoPort;
	private RecuperaCircuitosPort recuperaCircuitosPort;

	
	public CongregacaoService(CriaNovaCongregacaoPort criaNovaCongregacaoPort, 
							  RecuperaCongregacoesPort recuperaCongregacoesPort, 
							  AtualizaCongregacaoPort atualizaCongregacaoPort,
							  RecuperaCircuitosPort recuperaCircuitosPort,
							  RecuperaRegiaoPort recuperaRegiaoPort) {
		this.criaNovaCongregacaoPort = criaNovaCongregacaoPort;
		this.recuperaCongregacoesPort = recuperaCongregacoesPort;
		this.atualizaCongregacaoPort = atualizaCongregacaoPort;
		this.recuperaCircuitosPort = recuperaCircuitosPort;
		this.recuperaRegiaoPort = recuperaRegiaoPort;
	}

	@Override
	public List<Congregacao> recuperaCongregacoesAtivas() {
		return recuperaCongregacoesPort.recuperaCongregacoesAtivas();
	}

	@Override
	public Congregacao criaNovaCongregacao(Congregacao congregacao) throws TperioBusinessException {
		congregacao.valida();
		nomeEnumeroCongregacaoJaExiste(congregacao);
		circuitoExiste(congregacao.getCircuito().getUuid());
		regiaoExiste(congregacao.getRegiao().getUuid());
		return criaNovaCongregacaoPort.criaNovaCongregacao(congregacao);
	}
	
	private void regiaoExiste(String uuid) throws TperioBusinessException {
		Optional<Regiao> opt = recuperaRegiaoPort.recuperaRegiaoPorUuid(uuid);
		if(opt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.REGIAONAOENCONTRADA));
		}
	}

	private void circuitoExiste(String uuid) throws TperioBusinessException {
		Optional<Circuito> opt = recuperaCircuitosPort.recuperaCircuitoPorUuid(uuid);
		if(opt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.CIRCUITONAOENCONTRADO));
		}
	}


	private void nomeEnumeroCongregacaoJaExiste(Congregacao congregacao) throws TperioBusinessException {
		Optional<Congregacao> opt = recuperaCongregacoesPort.recuperaCongregacaoPorNomeNumero(congregacao.getNome(), congregacao.getNumero());
		if(!opt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.CONGREGACAOJAEXISTE));
		}
	}
	
	@Override
	public Congregacao recuperaCongregacaoPorUuid(String uuid) throws TperioBusinessException {
		Optional<Congregacao> congregacao = recuperaCongregacoesPort.recuperaCongregacaoPorUuid(uuid);
		if(congregacao.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.CONGREGACAONAOENCONTRADA));
		}
		return congregacao.get();
	}

	@Override
	public Congregacao atualizaCongregacao(Congregacao congregacao) throws TperioBusinessException {
		recuperaCongregacaoPorUuid(congregacao.getUuid());

		congregacao = atualizaCongregacaoPort.atualizaCongregacao(congregacao);
		return congregacao;
	}

	@Override
	public void apagaPublicador(String uuid) throws TperioBusinessException {
		recuperaCongregacoesPort.recuperaCongregacaoPorUuid(uuid).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.AGENDAMENTONAOENCONTRADO)));
		atualizaCongregacaoPort.desativaCongregacao(uuid);
	}
	

}
