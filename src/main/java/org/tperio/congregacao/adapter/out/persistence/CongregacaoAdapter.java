package org.tperio.congregacao.adapter.out.persistence;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.tperio.circuito.adapter.out.persistence.CircuitoAdapter;
import org.tperio.circuito.adapter.out.persistence.CircuitoJpaEntity;
import org.tperio.circuito.adapter.out.persistence.CircuitoRepository;
import org.tperio.config.MessageKeys;
import org.tperio.congregacao.application.port.out.AtualizaCongregacaoPort;
import org.tperio.congregacao.application.port.out.CriaNovaCongregacaoPort;
import org.tperio.congregacao.application.port.out.RecuperaCongregacoesPort;
import org.tperio.congregacao.domain.Congregacao;
import org.tperio.exception.TperioBusinessException;
import org.tperio.regiao.adapter.out.persistence.RegiaoAdapter;
import org.tperio.regiao.adapter.out.persistence.RegiaoJpaEntity;
import org.tperio.regiao.adapter.out.persistence.RegiaoRepository;

import lombok.AllArgsConstructor;


@Component
@AllArgsConstructor
public class CongregacaoAdapter implements RecuperaCongregacoesPort, 
										   CriaNovaCongregacaoPort, 
										   AtualizaCongregacaoPort {

	private final CongregacaoRepository congregacaoRepository;
	private final CircuitoRepository circuitoRepository;
	private final CircuitoAdapter circuitoAdapter;
	private final RegiaoRepository regiaoRepository;
	private final RegiaoAdapter regiaoAdapter;

	@Override
	public List<Congregacao> recuperaCongregacoesAtivas() {
		List<CongregacaoJpaEntity> cong = congregacaoRepository.findAll();
		return cong.stream().filter(congT -> congT.getDatadesativado() == null).map(this::toCongregacao).collect(Collectors.toList());
	}

	@Override
	public Congregacao criaNovaCongregacao(Congregacao congregacao) throws TperioBusinessException {
		Optional<CircuitoJpaEntity> congentity = circuitoRepository.findByUuid(congregacao.getCircuito().getUuid());
		if(congentity.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.CIRCUITONAOENCONTRADO));
		}
		
		Optional<RegiaoJpaEntity> regentity = regiaoRepository.findByUuid(congregacao.getRegiao().getUuid());
		if(regentity.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.REGIAONAOENCONTRADA));
		}

		CongregacaoJpaEntity ent = fromCongregacao(congregacao);
		ent.setCircuito(congentity.get());
		ent.setRegiao(regentity.get());
		ent.setUuid(UUID.randomUUID().toString());
		ent = congregacaoRepository.save(ent);
		
		return toCongregacao(ent);
	}

	@Override
	public Optional<Congregacao> recuperaCongregacaoPorNomeNumero(String nome, String numero) {
		Optional<CongregacaoJpaEntity> opt = congregacaoRepository.findByNomeAndNumero(nome, numero);
		if(opt.isPresent()) {
			return Optional.of(toCongregacao(opt.get()));
		}
		return Optional.empty();
	}

	public Congregacao toCongregacao(CongregacaoJpaEntity congregacaoJpaEntity) {
		Congregacao p = new Congregacao();
		BeanUtils.copyProperties(congregacaoJpaEntity, p);
		p.setCircuito(circuitoAdapter.toCircuito(congregacaoJpaEntity.getCircuito()));
		p.setRegiao(regiaoAdapter.toRegiao(congregacaoJpaEntity.getRegiao()));
		return p;
	}

	public CongregacaoJpaEntity fromCongregacao(Congregacao congregacao) {
		CongregacaoJpaEntity p = new CongregacaoJpaEntity();
		BeanUtils.copyProperties(congregacao, p);
		if(!ObjectUtils.isEmpty(congregacao.getCircuito())) {
			p.setCircuito(circuitoAdapter.fromCircuito(congregacao.getCircuito()));
		}
		if(!ObjectUtils.isEmpty(congregacao.getRegiao())) {
			p.setRegiao(regiaoAdapter.fromRegiao(congregacao.getRegiao()));
		}
		return p;
	}
	
	@Override
	public Congregacao atualizaCongregacao(Congregacao congregacao) throws TperioBusinessException {
		Optional<CongregacaoJpaEntity> congregacaoJpaOpt = congregacaoRepository.findByUuid(congregacao.getUuid());

		if(congregacaoJpaOpt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.CONGREGACAONAOENCONTRADA));
		}
		
		Optional<CircuitoJpaEntity> circentity = circuitoRepository.findByUuid(congregacao.getCircuito().getUuid());
		if(circentity.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.CIRCUITONAOENCONTRADO));
		}
		
		Optional<RegiaoJpaEntity> regentity = regiaoRepository.findByUuid(congregacao.getRegiao().getUuid());
		if(regentity.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.REGIAONAOENCONTRADA));
		}
		
		CongregacaoJpaEntity entity = congregacaoJpaOpt.get();

		Long id = entity.getIdcongregacao();
		Date dataCriacao = entity.getDataCriacao();
		BeanUtils.copyProperties(fromCongregacao(congregacao), entity);
		entity.setIdcongregacao(id);
		entity.setCircuito(circentity.get());
		entity.setRegiao(regentity.get());
		entity.setDataCriacao(dataCriacao);
		congregacaoRepository.save(entity);
		return toCongregacao(entity);
	}

	@Override
	public Optional<Congregacao> recuperaCongregacaoPorUuid(String uuid) {
		Optional<CongregacaoJpaEntity> opt = congregacaoRepository.findByUuid(uuid);
		if(opt.isPresent()) {
			return Optional.of(toCongregacao(opt.get()));
		}
		return Optional.empty();
	}

	@Override
	public void desativaCongregacao(String uuid) throws TperioBusinessException {
		CongregacaoJpaEntity agendamentoEntityOrig = congregacaoRepository.findByUuid(uuid)
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.CONGREGACAONAOENCONTRADA)));
		agendamentoEntityOrig.setDatadesativado(new Date());
		congregacaoRepository.save(agendamentoEntityOrig);
	}
	
}
