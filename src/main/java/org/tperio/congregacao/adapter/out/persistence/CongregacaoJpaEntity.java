package org.tperio.congregacao.adapter.out.persistence;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import org.tperio.circuito.adapter.out.persistence.CircuitoJpaEntity;
import org.tperio.regiao.adapter.out.persistence.RegiaoJpaEntity;

@Entity
@Table(name = "congregacao")
public class CongregacaoJpaEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idcongregacao",unique = true)
	private Long idcongregacao;
	
	private String uuid = UUID.randomUUID().toString();
	private String nome;
	private String numero;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name="idregiao", nullable=false)
	private RegiaoJpaEntity regiao;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name="idcircuito", nullable=false)
	private CircuitoJpaEntity circuito;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datacriacao")
	private Date dataCriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datadesativado")
	private Date datadesativado;

	public Long getIdcongregacao() {
		return idcongregacao;
	}

	public void setIdcongregacao(Long idcongregacao) {
		this.idcongregacao = idcongregacao;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

	public RegiaoJpaEntity getRegiao() {
		return regiao;
	}

	public void setRegiao(RegiaoJpaEntity regiao) {
		this.regiao = regiao;
	}

	public CircuitoJpaEntity getCircuito() {
		return circuito;
	}

	public void setCircuito(CircuitoJpaEntity circuito) {
		this.circuito = circuito;
	}
	
}
