package org.tperio.congregacao.adapter.out.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CongregacaoRepository extends JpaRepository<CongregacaoJpaEntity, Long> {

	Optional<CongregacaoJpaEntity> findByNomeAndNumero(String nome, String numero);
	Optional<CongregacaoJpaEntity> findByUuid(String uuid);
	
}
