package org.tperio.congregacao.adapter.in.web;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.tperio.circuito.adapter.in.web.CircuitoDto;
import org.tperio.circuito.domain.Circuito;
import org.tperio.congregacao.domain.Congregacao;
import org.tperio.regiao.adapter.in.web.RegiaoDto;
import org.tperio.regiao.domain.Regiao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class CongregacaoDto {

	private String uuid;
	
	@NotNull(message = "Preencha o nome")
	@Size(min = 3, max = 255, message = "Tamanho do nome deve ser entre 3 e 255 letras")
	private String nome;
	
	@Size(min = 1, max = 255, message = "Tamanho do número deve ser entre 1 e 255")
	private String numero;
	
	@NotNull(message = "Preencha a região")
	private String uuidregiao;
	
	@NotNull(message = "Preencha o circuito")
	private String uuidcircuito;
	
	private CircuitoDto circuitoDto;
	
	private RegiaoDto regiaoDto;
	
	public static CongregacaoDto fromCongregacao(Congregacao congregacao) {
		return CongregacaoDto.builder()
				.uuid(congregacao.getUuid())
				.nome(congregacao.getNome())
				.numero(congregacao.getNumero())
				.uuidregiao(congregacao.getRegiao().getUuid())
				.uuidcircuito(congregacao.getCircuito().getUuid())
				.circuitoDto(CircuitoDto.fromCircuito(congregacao.getCircuito()))
				.regiaoDto(RegiaoDto.fromRegiao(congregacao.getRegiao()))
				.build();
	}
	
	public Congregacao toCongregacao() {
		Congregacao congregacao = new Congregacao();
		congregacao.setUuid(this.uuid);
		congregacao.setNome(this.nome);
		congregacao.setNumero(this.numero);
		
		Regiao regiao = new Regiao();
		regiao.setUuid(this.uuidregiao);
		congregacao.setRegiao(regiao);

		Circuito circuito = new Circuito();
		circuito.setUuid(this.uuidcircuito);
		congregacao.setCircuito(circuito);
		
		return congregacao;
	}
}
