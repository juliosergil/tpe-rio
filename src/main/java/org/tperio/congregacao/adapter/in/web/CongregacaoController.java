package org.tperio.congregacao.adapter.in.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tperio.congregacao.application.port.in.CriaNovaCongregacaoUseCase;
import org.tperio.congregacao.application.port.in.RecuperaCongregacoesUseCase;
import org.tperio.congregacao.application.port.in.AtualizaCongregacaoUseCase;

import org.tperio.congregacao.domain.Congregacao;
import org.tperio.exception.TperioBusinessException;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/congregacao")
public class CongregacaoController {
	
	@Autowired private RecuperaCongregacoesUseCase recuperaCongregacoes;
	@Autowired private CriaNovaCongregacaoUseCase criaCongregacao;
	@Autowired private AtualizaCongregacaoUseCase atualizaCongregacaoUseCase;
	
	@GetMapping(path = "/find", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CongregacaoDto>> recuperaCongregacao() {
		List<Congregacao> congregacoes = recuperaCongregacoes.recuperaCongregacoesAtivas();
		List<CongregacaoDto> lista = congregacoes.stream().map(CongregacaoDto::fromCongregacao).collect(Collectors.toList());
		return ResponseEntity.ok(lista);
	}
	
	@PostMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CongregacaoDto> gravaCongregacao(@Valid @RequestBody CongregacaoDto congregacaoDto) throws TperioBusinessException {
		Congregacao cong = criaCongregacao.criaNovaCongregacao(congregacaoDto.toCongregacao());
		return ResponseEntity.ok(CongregacaoDto.fromCongregacao(cong));
	}

	@PutMapping(path = "/{uuid}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public CongregacaoDto atualizaCongregacao(@PathVariable String uuid,
    		@Valid @RequestBody CongregacaoDto congregacaoDto, 
    		@AuthenticationPrincipal UserDetails user) 
    				throws TperioBusinessException{
		Congregacao congregacao = atualizaCongregacaoUseCase.atualizaCongregacao(congregacaoDto.toCongregacao());
    	return CongregacaoDto.fromCongregacao(congregacao);
    }
	
	@SuppressWarnings("rawtypes")
	@DeleteMapping(path = "/{uuid}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity apagaCongregacao(
    		@PathVariable String uuid,
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException{
		atualizaCongregacaoUseCase.apagaPublicador(uuid);
    	return ResponseEntity.ok().build();
    }
	
}
