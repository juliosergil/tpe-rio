package org.tperio.rest.controller;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Profile({"dev"})
@RequestMapping("/")
public class RootController {
    
	@GetMapping
    public String swaggerUi() {
        return "redirect:/swagger-ui/";
    }
	
	@GetMapping("limpabase")
	public void limpaBase() {
		//Em dev vai limpar a base completamente
	}
}
