package org.tperio.exception;

import java.util.List;

import org.springframework.http.HttpStatus;

public class TperioException extends Exception {

	private static final long serialVersionUID = 1035230911732681071L;
	private final HttpStatus codigo;
	private final List<String> mensagens;
	
	@SuppressWarnings("unused")
	private TperioException() {
		this.codigo = null;
		this.mensagens = null;//para impedir exception sem mensagem e código
	}
	
	public TperioException(List<String> mensagens, HttpStatus codigo) {
		this.codigo = codigo;
		this.mensagens = mensagens;
	}
	
	public List<String> getMensagens() {
		return mensagens;
	}
	public HttpStatus getCodigo() {
		return codigo;
	}
}
