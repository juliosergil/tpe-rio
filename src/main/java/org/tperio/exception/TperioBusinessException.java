package org.tperio.exception;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TperioBusinessException extends Exception {
	
	private static final long serialVersionUID = 7356437613819037718L;
	private List<String> mensagens;

	public TperioBusinessException(List<String> mensagens) {
		this.mensagens = mensagens;
	}
	
	public void addMessage(String mensagem) {
		if(Objects.isNull(mensagens)) {
			mensagens = new ArrayList<>();
		}
		mensagens.add(mensagem);
	}

	public List<String> getMensagens() {
		return mensagens;
	}
	
}
