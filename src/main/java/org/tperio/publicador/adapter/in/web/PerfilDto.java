package org.tperio.publicador.adapter.in.web;

import org.tperio.publicador.domain.Perfil;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class PerfilDto {

	private String uuid;
	
	private String nome;
	
	public static PerfilDto fromPerfil(Perfil perfil) {
		return PerfilDto.builder()
				.nome(perfil.getNome())
				.uuid(perfil.getUuid())
				.build();
	}
}
