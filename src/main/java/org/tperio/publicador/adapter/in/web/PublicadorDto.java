package org.tperio.publicador.adapter.in.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;
import org.tperio.config.MessageKeys;
import org.tperio.congregacao.adapter.in.web.CongregacaoDto;
import org.tperio.congregacao.domain.Congregacao;
import org.tperio.evento.adapter.in.web.EventoAtivoDto;
import org.tperio.publicador.domain.Perfil;
import org.tperio.publicador.domain.Publicador;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class PublicadorDto {
	
	private String uuid;
	
	@NotNull(message = MessageKeys.PREENCHANOME)
	private String nome;
	
	@NotNull(message = MessageKeys.PREENCHAEMAIL)
	@Email(message = MessageKeys.EMAILINVALIDO)
	private String email;
	
	@NotNull(message = MessageKeys.PREENCHATELEFONE)
	private String telefone;
	
	@NotNull(message = MessageKeys.PREENCHAGENERO)
	private String genero;
	
	private String senha;
	
	@NotNull(message = MessageKeys.PREENCHACONGREGACAO)
	private String uuidcongregacao;
	
	@NotNull(message = MessageKeys.PREENCHAPERFIL)
	private String uuidperfil;
	
	private PerfilDto perfil;
	
	private List<EventoAtivoDto> eventosAtivos;
	
	private CongregacaoDto congregacao;
	
	public static PublicadorDto fromPublicador(Publicador publicador) {
		PublicadorDto dto = PublicadorDto.builder()
				.email(publicador.getEmail())
				.nome(publicador.getNome())
				.genero(publicador.getGenero())
				.telefone(publicador.getTelefone())
				.perfil(PerfilDto.fromPerfil(publicador.getPerfil()))
				.congregacao(CongregacaoDto.fromCongregacao(publicador.getCongregacao()))
				.uuid(publicador.getUuid())
				.build();
		
		if (!ObjectUtils.isEmpty(publicador.getEventoAtivo())) {
			dto.setEventosAtivos(publicador.getEventoAtivo().stream().map(EventoAtivoDto::fromEvento).collect(Collectors.toList()));
		}
		
		return dto;
	}

	public Publicador toPublicador() {
		Publicador publicador = new Publicador();
		BeanUtils.copyProperties(this, publicador);
		Congregacao cong = new Congregacao();
		cong.setUuid(this.getUuidcongregacao());
		publicador.setCongregacao(cong);
		Perfil perfilT = new Perfil(this.uuidperfil);
		publicador.setPerfil(perfilT);
		return publicador;
	}
}
