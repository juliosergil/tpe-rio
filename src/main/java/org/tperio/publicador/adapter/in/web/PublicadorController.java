package org.tperio.publicador.adapter.in.web;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.tperio.DTO.CredenciaisDTO;
import org.tperio.DTO.TokenDTO;
import org.tperio.enums.Perfil;
import org.tperio.exception.SenhaInvalidaException;
import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.application.port.in.AtualizaPublicadorUseCase;
import org.tperio.publicador.application.port.in.CriaNovoPublicadorUseCase;
import org.tperio.publicador.application.port.in.RecuperaPublicadorUseCase;
import org.tperio.publicador.domain.Publicador;
import org.tperio.service.JwtUtil;
import org.tperio.service.TelegramMessager;

import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/publicador")
@Slf4j
public class PublicadorController {
	
	@Autowired @Qualifier("recuperaPublicador") private RecuperaPublicadorUseCase recuperaPublicadorUseCase;
	@Autowired @Qualifier("recuperaPublicador") private CriaNovoPublicadorUseCase criaNovoPublicadorUseCase;
	@Autowired private AtualizaPublicadorUseCase atualizaPublicadorUseCase;
	@Autowired private TelegramMessager messanger;
	@Autowired private SessionRegistry sessionRegistry;
	
	@Autowired private JwtUtil jwtUtil;
	@Autowired private UsuarioServiceImpl usuarioService;
	@Autowired
    @Lazy
    private PasswordEncoder encoder;
	
	@GetMapping(path = "/find", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PublicadorDto> recuperaPublicador(
			@Valid @RequestParam("email") Optional<String> email,
			@Valid @RequestParam("congregacao") Optional<String> congrecacaoUuid,
			@Valid @RequestParam("circuito") Optional<String> circuitoUuid) throws TperioBusinessException {
		List<Publicador> publicadores = recuperaPublicadorUseCase.recuperaPublicadorPorFiltro(email, congrecacaoUuid, circuitoUuid);
		return publicadores.stream().map(PublicadorDto::fromPublicador).toList();
	}

	@GetMapping(path = "/desativados", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PublicadorDto> recuperaPublicadoresDesativados() throws TperioBusinessException {
		List<Publicador> publicadores = recuperaPublicadorUseCase.recuperaPublicadoresDesativados();
		return publicadores.stream().map(PublicadorDto::fromPublicador).toList();
	}
	
	@GetMapping(path = "/nome/{nome}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PublicadorDto> recuperaPublicadorPorNome(@Valid @PathVariable String nome) {
		List<Publicador> publicador = recuperaPublicadorUseCase.recuperaPublicadorPorNomeLike(nome);
		return publicador.stream().map(PublicadorDto::fromPublicador).collect(Collectors.toList());
	}
	
	@GetMapping(path = "/perfil/{perfil}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PublicadorDto> recuperaPublicadorPorPerfil(@Valid @PathVariable String perfil) throws TperioBusinessException {
		List<Publicador> listaPub = recuperaPublicadorUseCase.recuperaPublicadorPorPerfil(Perfil.valueOf(perfil));
		return listaPub.stream().map(PublicadorDto::fromPublicador).collect(Collectors.toList());
	}

	@GetMapping("/active-sessions")
	public int getActiveSessions() {
		return sessionRegistry.getAllPrincipals().size();
	}

    @PostMapping("/auth")
    public TokenDTO autenticar(@RequestBody CredenciaisDTO credenciais) throws JsonProcessingException {
		final List<Object> allPrincipals = sessionRegistry.getAllPrincipals();
		log.debug("total de logados: {}", allPrincipals.size());
		if(allPrincipals.size() >= 50) {
			throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "limite de acessos alcançados");
		}
        try{
        	PublicadorDto dto = PublicadorDto.builder()
        			.email(credenciais.getLogin())
        			.senha(credenciais.getSenha())
        			.build();
        	usuarioService.autenticar(dto);
        	Publicador p = recuperaPublicadorUseCase.recuperaPublicadorPorEmail(dto.getEmail().toLowerCase());
			/*if(p == null || !p.getCongregacao().getCircuito().getUuid().equals("03c61c99-7274-4f25-925e-827c3757099d")) {
				throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "acesso restrito para alguns circuitos");
			}
			if(p == null
					|| !p.isPerfilAdministrador()) {
				throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "acesso restrito para administradores e algum circuito");
			}*/

        	dto = PublicadorDto.fromPublicador(p);
            String token = jwtUtil.gerarToken(dto);
            return new TokenDTO(dto.getEmail(), token, dto.getEventosAtivos());
        } catch (UsernameNotFoundException | SenhaInvalidaException e ){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
    
    @PostMapping("")
    public PublicadorDto criaNovoPublicador(@Valid @RequestBody PublicadorDto publicadorDto) throws TperioBusinessException{
    	publicadorDto.setSenha(encoder.encode(publicadorDto.getSenha()));
    	Publicador publicador = criaNovoPublicadorUseCase.criaNovoPublicador(publicadorDto.toPublicador());
    	return PublicadorDto.fromPublicador(publicador);
    }
    
    @PutMapping("/{uuid}")
    public PublicadorDto atualizaPublicador(
    		@PathVariable String uuid,
    		@Valid @RequestBody PublicadorDto publicadorDto,
    		@AuthenticationPrincipal UserDetails user) throws TperioBusinessException {
    	publicadorDto.setUuid(uuid);
    	Publicador publicador = atualizaPublicadorUseCase.atualizaPublicador(publicadorDto.toPublicador());
    	return PublicadorDto.fromPublicador(publicador);
    }
    
    @PatchMapping("/{uuid}")
    public PublicadorDto atualizaPublicadorPublico(
    		@PathVariable String uuid,
    		@Valid @RequestBody PublicadorPatchDto publicadorPatchDto,
    		@AuthenticationPrincipal UserDetails user) throws TperioBusinessException {
    	publicadorPatchDto.setUuid(uuid);
    	Publicador publicador = atualizaPublicadorUseCase.atualizaPublicadorParcial(publicadorPatchDto.toPublicador());
    	return PublicadorDto.fromPublicador(publicador);
    }
    
    @SuppressWarnings("rawtypes")
	@DeleteMapping(path = "/{uuid}",
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity apagaPublicador(
    		@PathVariable String uuid,
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException {
		atualizaPublicadorUseCase.apagaPublicador(uuid);
    	return ResponseEntity.ok().build();
    }
    
    @PostMapping("/esquecisenha")
    public ResponseEntity<Object> esqueciminhasenha(@Valid @RequestBody String email) throws TperioBusinessException {
    	usuarioService.esqueciMinhaSenha(email);
    	return ResponseEntity.ok().build();
    }

    @Profile(value = "dev")
    @GetMapping(path = "/testmessage/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> testamensagem(@Valid @PathVariable String uuid) {
		messanger.enviaMensagem(uuid, "teste mensagem");
		return ResponseEntity.ok().build();
	}
    
}
