package org.tperio.publicador.adapter.in.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tperio.publicador.application.port.in.RecuperaPerfilUseCase;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/perfil")
public class PerfilController {
	
	@Autowired @Qualifier("recuperaPerfil") private RecuperaPerfilUseCase recuperaPerfilUseCase;
	
	@GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PerfilDto> recuperaPerfis() {
		return recuperaPerfilUseCase.recuperaTodosPerfis().stream().map(PerfilDto::fromPerfil).collect(Collectors.toList());
	}

}
