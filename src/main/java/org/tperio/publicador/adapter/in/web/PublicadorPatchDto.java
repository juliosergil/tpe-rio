package org.tperio.publicador.adapter.in.web;

import javax.validation.constraints.NotNull;

import org.tperio.config.MessageKeys;
import org.tperio.publicador.domain.Publicador;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class PublicadorPatchDto {

private String uuid;
	
	@NotNull(message = MessageKeys.PREENCHATELEFONE)
	private String telefone;
	
	private String senha;
	
	public static PublicadorPatchDto fromPublicador(Publicador publicador) {
		return PublicadorPatchDto.builder()
				.telefone(publicador.getTelefone())
				.uuid(publicador.getUuid())
				.build();
	}

	public Publicador toPublicador() {
		Publicador p = new Publicador();
		p.setUuid(uuid);
		p.setTelefone(telefone);
		p.setSenha(senha);
		return p;
	}
}
