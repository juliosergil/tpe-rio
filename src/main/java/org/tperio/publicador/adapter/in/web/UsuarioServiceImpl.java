package org.tperio.publicador.adapter.in.web;

import java.util.Arrays;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.tperio.exception.SenhaInvalidaException;
import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.adapter.out.persistence.PublicadorAdapter;
import org.tperio.publicador.domain.Publicador;
import org.tperio.service.EmailBroker;

@Service
public class UsuarioServiceImpl implements UserDetailsService {

    @Autowired
    private PublicadorAdapter adapter;

    @Autowired
    private EmailBroker emailBroker;

    @Autowired
    @Lazy
    private PasswordEncoder encoder;
    private Random rand = new Random();

    public UserDetails autenticar(PublicadorDto usuario){
        UserDetails user = loadUserByUsername(usuario.getEmail().toLowerCase());
        boolean senhasBatem = encoder.matches(usuario.getSenha(), user.getPassword());

        if (senhasBatem){
            return user;
        }

        throw new SenhaInvalidaException();
    }

    public void esqueciMinhaSenha(String email) throws TperioBusinessException {
    	Publicador pub = adapter.recuperaPublicadorPorEmail(email.toLowerCase());
    	if (ObjectUtils.isEmpty(pub)) {
    		throw new TperioBusinessException(Arrays.asList("Usuário não encontrado na base de dados."));
    	}
    	int numero = rand.nextInt(999999);
    	pub.setSenha(encoder.encode(numero+""));
    	adapter.atualizaSenha(pub);
    	emailBroker.enviaEmailNovaSenha(pub.getNome(), numero + "", email);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    	Publicador usuario = adapter.recuperaPublicadorPorEmail(username.toLowerCase());
    	if (usuario == null || usuario.getDatadesativado() != null) {
    		throw new UsernameNotFoundException("Usuário não encontrado na base de dados.");
    	}

        return User
                .builder()
                .username(usuario.getEmail())
                .password(usuario.getSenha())
                .roles(usuario.getPerfil().getNome())
                .build();
    }
}
