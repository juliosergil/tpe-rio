package org.tperio.publicador.adapter.out.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.tperio.circuito.adapter.out.persistence.CircuitoJpaEntity;
import org.tperio.congregacao.adapter.out.persistence.CongregacaoJpaEntity;

public interface PublicadorRepository extends JpaRepository<PublicadorJpaEntity, Long> {

	Optional<PublicadorJpaEntity> findByEmail(String email);

	Optional<PublicadorJpaEntity> findByEmailAndNome(String email, String nome);

	Optional<PublicadorJpaEntity> findByUuid(String uuid);
	
	Optional<PublicadorJpaEntity> findByTelefone(String telefone);

	List<PublicadorJpaEntity> findByPerfil(PerfilJpaEntity perfilJpa);

	List<PublicadorJpaEntity> findByDatadesativadoNotNull();

	List<PublicadorJpaEntity> findByNomeLike(String nome);

	List<PublicadorJpaEntity> findByCongregacao(CongregacaoJpaEntity congentity);

	@Query("SELECT pub FROM PublicadorJpaEntity pub "
			+ "WHERE "
			+ "pub.congregacao.circuito = :circentity ")
	List<PublicadorJpaEntity> findByCircuitoJpaEntity(CircuitoJpaEntity circentity);
	
}
