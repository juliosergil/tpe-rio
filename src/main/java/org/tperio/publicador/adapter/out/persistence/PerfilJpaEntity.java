package org.tperio.publicador.adapter.out.persistence;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

@Entity
@Table(name = "perfil")
public class PerfilJpaEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idperfil",unique = true)
	private Long idpublicador;
	
	private String uuid = UUID.randomUUID().toString();
	
	private String nome;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datacriacao")
	private Date datacriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datadesativado")
	private Date datadesativado;

	public Long getIdpublicador() {
		return idpublicador;
	}

	public void setIdpublicador(Long idpublicador) {
		this.idpublicador = idpublicador;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}
	
}
