package org.tperio.publicador.adapter.out.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PerfilRepository extends JpaRepository<PerfilJpaEntity, Long> {
	
	Optional<PerfilJpaEntity> findByUuid(String uuid);
	Optional<PerfilJpaEntity> findByNome(String nome);

}
