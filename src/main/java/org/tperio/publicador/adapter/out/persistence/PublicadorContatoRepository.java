package org.tperio.publicador.adapter.out.persistence;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PublicadorContatoRepository extends JpaRepository<PublicadorContatoJpaEntity, Long> {
	Optional<PublicadorContatoJpaEntity> findByPublicador(PublicadorJpaEntity publicador);
	Optional<PublicadorContatoJpaEntity> findByTelegram(String chatId);
}
