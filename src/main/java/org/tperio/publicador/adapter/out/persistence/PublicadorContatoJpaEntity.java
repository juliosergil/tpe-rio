package org.tperio.publicador.adapter.out.persistence;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "publicadorcontato")
public class PublicadorContatoJpaEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idpublicadorcontato",unique = true)
	private Long idpublicadorcontato;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "idpublicador", referencedColumnName = "idpublicador")
	private PublicadorJpaEntity publicador;
	
	@Column(name = "telegram")
	private String telegram;

	public Long getIdpublicadorcontato() {
		return idpublicadorcontato;
	}

	public void setIdpublicadorcontato(Long idpublicadorcontato) {
		this.idpublicadorcontato = idpublicadorcontato;
	}

	public PublicadorJpaEntity getPublicador() {
		return publicador;
	}

	public void setPublicador(PublicadorJpaEntity publicador) {
		this.publicador = publicador;
	}

	public String getTelegram() {
		return telegram;
	}

	public void setTelegram(String telegram) {
		this.telegram = telegram;
	}

}
