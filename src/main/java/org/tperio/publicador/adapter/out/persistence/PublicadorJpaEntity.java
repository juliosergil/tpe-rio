package org.tperio.publicador.adapter.out.persistence;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import org.tperio.congregacao.adapter.out.persistence.CongregacaoJpaEntity;

@Entity
@Table(name = "publicador")
public class PublicadorJpaEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idpublicador",unique = true)
	private Long idpublicador;

	private String uuid = UUID.randomUUID().toString();

	private String nome;

	private String email;

	private String senha;

	private String telefone;

	private String genero;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "idcongregacao", nullable=false)
	private CongregacaoJpaEntity congregacao;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "idperfil", nullable=false)
	private PerfilJpaEntity perfil;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datacriacao")
	private Date datacriacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datadesativado")
	private Date datadesativado;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idpublicador", referencedColumnName = "idpublicador", nullable = true)
	private PublicadorContatoJpaEntity contato;

	public PublicadorContatoJpaEntity getContato() {
		return contato;
	}

	public void setContato(PublicadorContatoJpaEntity contato) {
		this.contato = contato;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public PerfilJpaEntity getPerfil() {
		return perfil;
	}

	public void setPerfil(PerfilJpaEntity perfil) {
		this.perfil = perfil;
	}

	public CongregacaoJpaEntity getCongregacao() {
		return congregacao;
	}

	public void setCongregacao(CongregacaoJpaEntity congregacao) {
		this.congregacao = congregacao;
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

	public Long getIdpublicador() {
		return idpublicador;
	}

	public void setIdpublicador(Long idpublicador) {
		this.idpublicador = idpublicador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSenha() {
		return senha;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
