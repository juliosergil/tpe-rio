package org.tperio.publicador.adapter.out.persistence;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.tperio.circuito.adapter.out.persistence.CircuitoJpaEntity;
import org.tperio.circuito.adapter.out.persistence.CircuitoRepository;
import org.tperio.config.MessageKeys;
import org.tperio.congregacao.adapter.out.persistence.CongregacaoAdapter;
import org.tperio.congregacao.adapter.out.persistence.CongregacaoJpaEntity;
import org.tperio.congregacao.adapter.out.persistence.CongregacaoRepository;
import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.application.port.out.AtualizaPublicadorPort;
import org.tperio.publicador.application.port.out.CriaNovoPublicadorPort;
import org.tperio.publicador.application.port.out.RecuperaPerfilPort;
import org.tperio.publicador.application.port.out.RecuperaPublicadorPort;
import org.tperio.publicador.domain.Contato;
import org.tperio.publicador.domain.Perfil;
import org.tperio.publicador.domain.Publicador;
import org.tperio.service.EmailBroker;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class PublicadorAdapter implements RecuperaPublicadorPort, 
							CriaNovoPublicadorPort, 
							RecuperaPerfilPort,
							AtualizaPublicadorPort {
	
	private final PublicadorRepository publicadorRepository;
	private final PublicadorContatoRepository publicadorContatoRepository;
	private final PerfilRepository perfilRepository;
	private final CongregacaoRepository congregacaoRepository;
	private final CongregacaoAdapter congregacaoAdapter;
	private final CircuitoRepository circuitoRepository;
	
	@Autowired
    @Lazy
    private PasswordEncoder encoder;
	private final EmailBroker broker;
	private Random rand = new Random();

	@Override
	@Cacheable(value = "publicadoresName")
	public Publicador recuperaPublicadorPorEmail(String email) {
		PublicadorJpaEntity entity = publicadorRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados."));
		return toPublicador(entity);
	}

	@Override
	@CacheEvict(value = "publicadoresCache", allEntries = true)
	public Publicador criaNovoPublicador(Publicador publicador) throws TperioBusinessException {
		Optional<CongregacaoJpaEntity> congentity = congregacaoRepository.findByUuid(publicador.getCongregacao().getUuid());
		if(congentity.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.CONGREGACAONAOENCONTRADA));
		}
		PerfilJpaEntity perfilEntity = perfilRepository.findByUuid(publicador.getPerfil().getUuid()).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PERFILNAOENCONTRADO)));
		PublicadorJpaEntity entity = fromPublicador(publicador);
		entity.setCongregacao(congentity.get());
		entity.setPerfil(perfilEntity);
		int numero = rand.nextInt(999999);
		entity.setSenha(encoder.encode(numero+""));
		publicadorRepository.save(entity);
		broker.enviaEmailSenhaCadastro(entity.getNome(), numero+"", entity.getEmail());
		return toPublicador(entity);
	}

	@Override
	@Cacheable(value = "publicadoresCache")
	public Optional<Publicador> recuperaPublicadorPorEmailNome(String email, String nome) {
		Optional<PublicadorJpaEntity> opt = publicadorRepository.findByEmailAndNome(email, nome);
		if(opt.isPresent()) {
			return Optional.of(toPublicador(opt.get()));
		}
		return Optional.empty();
	}

	@Override
	public Optional<Publicador> recuperaPublicadorUuid(String uuid) {
		Optional<PublicadorJpaEntity> opt = publicadorRepository.findByUuid(uuid);
		if(opt.isPresent()) {
			return Optional.of(toPublicador(opt.get()));
		}
		return Optional.empty();
	}

	@CacheEvict(value = "publicadoresCache", allEntries = true)
	public Publicador atualizaSenha(Publicador publicador) {
		PublicadorJpaEntity entity = publicadorRepository.findByUuid(publicador.getUuid()).orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados."));
		entity.setSenha(publicador.getSenha());
		publicadorRepository.save(entity);
		return toPublicador(entity);
	}
	
	public Publicador toPublicador(PublicadorJpaEntity publicadorJpaEntity) {
		Publicador p = new Publicador();
		BeanUtils.copyProperties(publicadorJpaEntity, p);
		p.setPerfil(this.toPerfil(publicadorJpaEntity.getPerfil()));
		if(!ObjectUtils.isEmpty(publicadorJpaEntity.getContato())) {
			p.setContato(this.toContato(publicadorJpaEntity.getContato()));
		}
		p.setCongregacao(congregacaoAdapter.toCongregacao(publicadorJpaEntity.getCongregacao()));
		return p;
	}
	
	private Contato toContato(PublicadorContatoJpaEntity contatoEntity) {
		Contato contato = new Contato();
		BeanUtils.copyProperties(contatoEntity, contato);
		return contato;
	}

	public PublicadorJpaEntity fromPublicador(Publicador publicador) {
		PublicadorJpaEntity entity = new PublicadorJpaEntity();
		BeanUtils.copyProperties(publicador, entity);
		entity.setCongregacao(congregacaoAdapter.fromCongregacao(publicador.getCongregacao()));
		return entity;
	}

	@Override
	public Optional<Perfil> recuperaPerfilPorUuid(String uuid) {
		Optional<PerfilJpaEntity> opt = perfilRepository.findByUuid(uuid);
		if(opt.isPresent()) {
			return Optional.of(toPerfil(opt.get()));
		}
		return Optional.empty();
	}

	private Perfil toPerfil(PerfilJpaEntity perfilJpaEntity) {
		Perfil perfil = new Perfil(perfilJpaEntity.getUuid());
		BeanUtils.copyProperties(perfilJpaEntity, perfil);
		return perfil;
	}

	@Override
	public List<Perfil> recuperaPerfis() {
		return perfilRepository.findAll().stream().map(this::toPerfil).collect(Collectors.toList());
	}

	@Override
	public List<Publicador> recuperaPublicadorPorPerfil(Perfil perfil) throws TperioBusinessException {
		PerfilJpaEntity perfilJpa = perfilRepository.findByNome(perfil.getNome()).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PERFILNAOENCONTRADO)));
		return publicadorRepository.findByPerfil(perfilJpa).stream().map(this::toPublicador).collect(Collectors.toList());
	}

	@Override
	public Optional<Perfil> recuperaPerfilPorNome(String descricao) {
		Optional<PerfilJpaEntity> opt = perfilRepository.findByNome(descricao);
		if(opt.isPresent()) {
			return Optional.of(toPerfil(opt.get()));
		}
		return Optional.empty();
	}

	@Override
	public List<Publicador> recuperaPublicadorPorNomeLike(String nome) {
		List<PublicadorJpaEntity> lista = publicadorRepository.findByNomeLike("%" + nome + "%");
		return lista.stream().map(this::toPublicador).collect(Collectors.toList());
	}

	@Override
	@CacheEvict(value = "publicadoresCache", allEntries = true)
	public Publicador atualizaPublicador(Publicador publicador) {
		PublicadorJpaEntity entity = publicadorRepository.findByUuid(publicador.getUuid()).orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados."));
		CongregacaoJpaEntity congEntity = congregacaoRepository.findByUuid(publicador.getCongregacao().getUuid()).orElseThrow(() -> new UsernameNotFoundException(MessageKeys.CONGREGACAONAOENCONTRADA));
		PerfilJpaEntity perfilEntity = perfilRepository.findByUuid(publicador.getPerfil().getUuid()).orElseThrow(() -> new UsernameNotFoundException(MessageKeys.PERFILNAOENCONTRADO));
		Date dataCriacao = entity.getDatacriacao();
		Long id = entity.getIdpublicador();
		String senha = entity.getSenha();
		BeanUtils.copyProperties(publicador, entity);
		entity.setCongregacao(congEntity);
		entity.setPerfil(perfilEntity);
		entity.setDatacriacao(dataCriacao);
		entity.setIdpublicador(id);
		entity.setSenha(senha);
		publicadorRepository.save(entity);
		return toPublicador(entity);
	}

	@Override
	@CacheEvict(value = "publicadoresCache", allEntries = true)
	public void apagaPublicador(String uuid) {
		PublicadorJpaEntity entity = publicadorRepository.findByUuid(uuid).orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados."));
		entity.setDatadesativado(new Date());
		publicadorRepository.save(entity);
	}

	@Override
	public Optional<Publicador> consultaPublicadorPorEmail(String email) {
		Optional<PublicadorJpaEntity> opt = publicadorRepository.findByEmail(email);
		if(opt.isPresent()) {
			return Optional.of(toPublicador(opt.get()));
		}
		return Optional.empty();
	}

	@Override
	@CacheEvict(value = "publicadoresCache", allEntries = true)
	public Publicador atualizaPublicadorParcial(Publicador publicador) {
		PublicadorJpaEntity entity = publicadorRepository.findByUuid(publicador.getUuid()).orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados."));
		if(!ObjectUtils.isEmpty(publicador.getSenha())) {
			entity.setSenha(encoder.encode(publicador.getSenha()));
		}
		entity.setTelefone(publicador.getTelefone());
		publicadorRepository.save(entity);
		return toPublicador(entity);
	}

	@Override
	public Optional<Publicador> recuperaPublicadorPorTelefone(String telefone) {
		Optional<PublicadorJpaEntity> opt = publicadorRepository.findByTelefone(telefone);
		if(opt.isPresent()) {
			return Optional.of(toPublicador(opt.get()));
		}
		return Optional.empty();
	}

	public void criaPublicadorContatoTelegram(Publicador publicador, String chatId) {
		PublicadorJpaEntity entity = publicadorRepository.findByUuid(publicador.getUuid()).orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados."));
		Optional<PublicadorContatoJpaEntity> contopt = publicadorContatoRepository.findByPublicador(entity);
		PublicadorContatoJpaEntity contatoentity = null;
		if(contopt.isPresent()) {
			contatoentity = contopt.get();
			contatoentity.setTelegram(chatId);
		} else {
			contatoentity = new PublicadorContatoJpaEntity();
			contatoentity.setPublicador(entity);
			contatoentity.setTelegram(chatId);
		}
		publicadorContatoRepository.save(contatoentity);
	}
	
	public String recuperaContatoTelegram(Publicador publicador) {
		PublicadorJpaEntity entity = publicadorRepository.findByUuid(publicador.getUuid()).orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados."));
		PublicadorContatoJpaEntity contato = publicadorContatoRepository.findByPublicador(entity).orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado na base de dados."));
		return contato.getTelegram();
	}
	
	public boolean existeContatoTelegram(String chatId) {
		Optional<PublicadorContatoJpaEntity> contato = publicadorContatoRepository.findByTelegram(chatId);
		return contato.isPresent();
	}

	@Override
	public List<Publicador> recuperaPublicadoresPorCongregacaoUuid(String congregacaoUuid) throws TperioBusinessException {
		CongregacaoJpaEntity congentity = congregacaoRepository.findByUuid(congregacaoUuid).orElseThrow(() -> new TperioBusinessException(Arrays.asList("Congregação não foi encontrada")));
		List<PublicadorJpaEntity> listJpaPubli = publicadorRepository.findByCongregacao(congentity);
		return listJpaPubli.stream().map(this::toPublicador).toList();
	}

	@Override
	public List<Publicador> recuperaPublicadoresPorCircuitoUuid(String circuitoUuid) throws TperioBusinessException {
		CircuitoJpaEntity circentity = circuitoRepository.findByUuid(circuitoUuid).orElseThrow(() -> new TperioBusinessException(Arrays.asList("Circuito não foi encontrado")));
		List<PublicadorJpaEntity> listJpaPubli = publicadorRepository.findByCircuitoJpaEntity(circentity);
		return listJpaPubli.stream().map(this::toPublicador).toList();
	}

	@Override
	public List<Publicador> recuperaPublicadoresAtivos() throws TperioBusinessException {
		List<PublicadorJpaEntity> listJpaPubli = publicadorRepository.findAll();
		return listJpaPubli.stream().map(this::toPublicador).filter(pub -> pub.getDatadesativado() == null).toList();
	}

	@Override
	public List<Publicador> recuperaPublicadoresDesativados() {
		List<PublicadorJpaEntity> listJpaPubli = publicadorRepository.findByDatadesativadoNotNull();
		return listJpaPubli.stream().map(this::toPublicador).toList();
	}

}
