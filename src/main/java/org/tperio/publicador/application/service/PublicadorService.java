package org.tperio.publicador.application.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.tperio.config.MessageKeys;
import org.tperio.congregacao.application.port.out.RecuperaCongregacoesPort;
import org.tperio.evento.application.port.out.RecuperaEventoPort;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.application.port.in.CriaNovoPublicadorUseCase;
import org.tperio.publicador.application.port.in.RecuperaPerfilUseCase;
import org.tperio.publicador.application.port.in.RecuperaPublicadorUseCase;
import org.tperio.publicador.application.port.out.CriaNovoPublicadorPort;
import org.tperio.publicador.application.port.out.RecuperaPerfilPort;
import org.tperio.publicador.application.port.out.RecuperaPublicadorPort;
import org.tperio.publicador.domain.Perfil;
import org.tperio.publicador.domain.Publicador;

public class PublicadorService implements RecuperaPublicadorUseCase, CriaNovoPublicadorUseCase, RecuperaPerfilUseCase {
	
	private RecuperaPublicadorPort recuperaPublicadorPort;
	private CriaNovoPublicadorPort criaNovoPublicadorPort;
	private RecuperaPerfilPort recuperaPerfilPort;
	private RecuperaCongregacoesPort recuperaCongregacoesPort;
	private RecuperaEventoPort recuperaEventoPort;
	
	public PublicadorService(
			RecuperaPublicadorPort recuperaPublicadorPort, 
			CriaNovoPublicadorPort criaNovoPublicadorPort, 
			RecuperaCongregacoesPort recuperaCongregacoesPort,
			RecuperaPerfilPort recuperaPerfilPort,
			RecuperaEventoPort recuperaEventoPort) {
		this.recuperaPublicadorPort = recuperaPublicadorPort;
		this.criaNovoPublicadorPort = criaNovoPublicadorPort;
		this.recuperaCongregacoesPort = recuperaCongregacoesPort;
		this.recuperaPerfilPort = recuperaPerfilPort;
		this.recuperaEventoPort = recuperaEventoPort;
	}

	@Override
	public Publicador recuperaPublicadorPorEmail(String email) {
		Publicador pub = recuperaPublicadorPort.recuperaPublicadorPorEmail(email);
		//TODO hack pra permitir acesso apenas de adm
		/*if(!pub.isPerfilAdministrador()){
			return null;
		}*/
		List<Evento> eventos = recuperaEventoPort.recuperaEventosAtivos();
		if (eventos != null && !eventos.isEmpty()) {
			eventos = eventos.stream().filter(ev -> pub.achouPublicador(ev.getPublicadores())).collect(Collectors.toList());
		}
		pub.setEventoAtivo(eventos);
		return pub;
	}

	@Override
	public Publicador criaNovoPublicador(Publicador publicador) throws TperioBusinessException {
		publicador.validaPublicador();
		publicadorJaExiste(publicador);
		congregacaoExiste(publicador.getCongregacao().getUuid());
		perfilExiste(publicador.getPerfil());
		publicador.setUuid(UUID.randomUUID().toString());
		publicador.setDatacriacao(new Date());
		return criaNovoPublicadorPort.criaNovoPublicador(publicador);
	}

	private void perfilExiste(Perfil perfil) throws TperioBusinessException {
		recuperaPerfilPort.recuperaPerfilPorUuid(perfil.getUuid()).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PERFILNAOENCONTRADO)));
	}

	private void congregacaoExiste(String uuid) throws TperioBusinessException {
		recuperaCongregacoesPort.recuperaCongregacaoPorUuid(uuid).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.CONGREGACAONAOENCONTRADA)));
	}
	
	private void publicadorJaExiste(Publicador publicador) throws TperioBusinessException {
		Optional<Publicador> opt = recuperaPublicadorPort.consultaPublicadorPorEmail(publicador.getEmail());
		if(opt.isPresent()) {
			throw new TperioBusinessException(Arrays.asList("Publicador já existe."));
		}
	}

	@Override
	public List<Perfil> recuperaTodosPerfis() {
		return recuperaPerfilPort.recuperaPerfis();
	}

	@Override
	public List<Publicador> recuperaPublicadorPorPerfil(org.tperio.enums.Perfil perfilenum) throws TperioBusinessException {
		Perfil perfil = recuperaPerfilPort.recuperaPerfilPorNome(perfilenum.getDescricao()).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PERFILNAOENCONTRADO)));
		return recuperaPublicadorPort.recuperaPublicadorPorPerfil(perfil)
				.stream()
				.filter(p -> p.getDatadesativado() == null)
				.collect(Collectors.toList());
	}

	@Override
	public List<Publicador> recuperaPublicadorPorNomeLike(String nome) {
		return recuperaPublicadorPort.recuperaPublicadorPorNomeLike(nome)
				.stream()
				.filter(p -> p.getDatadesativado() == null)
				.collect(Collectors.toList());
	}

	@Override
	public Publicador recuperaPublicadorUuid(String uuid) throws TperioBusinessException {
		Publicador pub = recuperaPublicadorPort.recuperaPublicadorUuid(uuid).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PUBLICADORNAOENCONTRADO)));
		List<Evento> eventos = recuperaEventoPort.recuperaEventosAtivos();
		if (eventos != null && !eventos.isEmpty()) {
			eventos = eventos.stream().filter(ev -> pub.achouPublicador(ev.getPublicadores())).collect(Collectors.toList());
		}
		pub.setEventoAtivo(eventos);
		return pub;
	}

	@Override
	public List<Publicador> recuperaPublicadorPorFiltro(Optional<String> email, Optional<String> congregacaoUuid, Optional<String> circuitoUuid)
			throws TperioBusinessException {
		if(congregacaoUuid.isPresent()) {
			return recuperaPublicadorPort.recuperaPublicadoresPorCongregacaoUuid(congregacaoUuid.get());
		}
		
		if(circuitoUuid.isPresent()) {
			return recuperaPublicadorPort.recuperaPublicadoresPorCircuitoUuid(circuitoUuid.get());
		}
		
		if(email.isPresent()) {
			return Arrays.asList(recuperaPublicadorPort.recuperaPublicadorPorEmail(email.get()));
		}
		return new ArrayList<>();
	}

	@Override
	public List<Publicador> recuperaPublicadoresDesativados() {
		return recuperaPublicadorPort.recuperaPublicadoresDesativados()
			.stream()
			.collect(Collectors.toList());
	}

}
