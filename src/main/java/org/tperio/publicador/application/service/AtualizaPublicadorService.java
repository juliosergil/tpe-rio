package org.tperio.publicador.application.service;

import java.util.Arrays;
import java.util.Optional;

import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.application.port.in.AtualizaPublicadorUseCase;
import org.tperio.publicador.application.port.out.AtualizaPublicadorPort;
import org.tperio.publicador.application.port.out.RecuperaPublicadorPort;
import org.tperio.publicador.domain.Publicador;

public class AtualizaPublicadorService implements AtualizaPublicadorUseCase {
	
	private AtualizaPublicadorPort atualizaPublicadorPort;
	private RecuperaPublicadorPort recuperaPublicadorPort;

	public AtualizaPublicadorService(
			AtualizaPublicadorPort atualizaPublicadorPort,
			RecuperaPublicadorPort recuperaPublicadorPort) {
		this.atualizaPublicadorPort = atualizaPublicadorPort;
		this.recuperaPublicadorPort = recuperaPublicadorPort;
	}

	@Override
	public Publicador atualizaPublicador(Publicador publicador) throws TperioBusinessException {
		publicador.validaPublicador();
		Optional<Publicador> temp = recuperaPublicadorPort.consultaPublicadorPorEmail(publicador.getEmail());
		if(temp.isPresent() && !temp.get().getUuid().equals(publicador.getUuid())) {
			throw new TperioBusinessException(Arrays.asList("Publicador já existe."));
		}
		return atualizaPublicadorPort.atualizaPublicador(publicador);
	}

	@Override
	public void apagaPublicador(String uuid) throws TperioBusinessException {
		atualizaPublicadorPort.apagaPublicador(uuid);
	}

	@Override
	public Publicador atualizaPublicadorParcial(Publicador publicador) throws TperioBusinessException {
		Optional<Publicador> temp = recuperaPublicadorPort.consultaPublicadorPorEmail(publicador.getEmail());
		if(temp.isPresent() && !temp.get().getUuid().equals(publicador.getUuid())) {
			throw new TperioBusinessException(Arrays.asList("Publicador já existe."));
		}
		return atualizaPublicadorPort.atualizaPublicadorParcial(publicador);
	}

}
