package org.tperio.publicador.application.port.in;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.tperio.enums.Perfil;
import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.domain.Publicador;

public interface RecuperaPublicadorUseCase {

	Publicador recuperaPublicadorPorEmail(String email);
	List<Publicador> recuperaPublicadorPorPerfil(Perfil perfil) throws TperioBusinessException;
	List<Publicador> recuperaPublicadorPorNomeLike(@Valid String nome);
	Publicador recuperaPublicadorUuid(String uuid) throws TperioBusinessException;
	List<Publicador> recuperaPublicadorPorFiltro(Optional<String> email, Optional<String> congregacaoUuid, Optional<String> circuitoUuid) throws TperioBusinessException;

    List<Publicador> recuperaPublicadoresDesativados();
}
