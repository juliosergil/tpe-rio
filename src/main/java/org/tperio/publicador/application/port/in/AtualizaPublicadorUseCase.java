package org.tperio.publicador.application.port.in;

import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.domain.Publicador;

public interface AtualizaPublicadorUseCase {

	Publicador atualizaPublicador(Publicador publicador) throws TperioBusinessException;
	void apagaPublicador(String uuid) throws TperioBusinessException;
	Publicador atualizaPublicadorParcial(Publicador publicador)throws TperioBusinessException;

}
