package org.tperio.publicador.application.port.in;

import java.util.List;

import org.tperio.publicador.domain.Perfil;

public interface RecuperaPerfilUseCase {

	List<Perfil> recuperaTodosPerfis();
	
}
