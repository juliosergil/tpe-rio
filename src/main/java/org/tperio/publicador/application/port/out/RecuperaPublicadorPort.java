package org.tperio.publicador.application.port.out;

import java.util.List;
import java.util.Optional;

import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.domain.Perfil;
import org.tperio.publicador.domain.Publicador;

public interface RecuperaPublicadorPort {

	Publicador recuperaPublicadorPorEmail(String email);
	
	Optional<Publicador> recuperaPublicadorPorEmailNome(String email, String nome);
	
	Optional<Publicador> consultaPublicadorPorEmail(String email);
	
	List<Publicador> recuperaPublicadorPorPerfil(Perfil perfil) throws TperioBusinessException;
	
	Optional<Publicador> recuperaPublicadorUuid(String uuid);

	List<Publicador> recuperaPublicadorPorNomeLike(String nome);
	
	Optional<Publicador> recuperaPublicadorPorTelefone(String telefone);

	List<Publicador> recuperaPublicadoresPorCongregacaoUuid(String congregacaoUuid) throws TperioBusinessException;

	List<Publicador> recuperaPublicadoresPorCircuitoUuid(String congregacaoUuid) throws TperioBusinessException;
	
	List<Publicador> recuperaPublicadoresAtivos() throws TperioBusinessException;

	List<Publicador> recuperaPublicadoresDesativados();
}
