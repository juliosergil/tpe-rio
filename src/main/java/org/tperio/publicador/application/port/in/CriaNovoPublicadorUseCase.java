package org.tperio.publicador.application.port.in;

import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.domain.Publicador;

public interface CriaNovoPublicadorUseCase {
	
	Publicador criaNovoPublicador(Publicador publicador) throws TperioBusinessException;

}
