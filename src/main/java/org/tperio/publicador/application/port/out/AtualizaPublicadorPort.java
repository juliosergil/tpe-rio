package org.tperio.publicador.application.port.out;

import org.tperio.publicador.domain.Publicador;

public interface AtualizaPublicadorPort {
	
	/**
	 * Este método não atualiza a senha do publicador. Foi planejado apenas para uma atualização do lado do admin.
	 * */
	Publicador atualizaPublicador(Publicador publicador);
	/**
	 * Este método atualiza apenas senha e telefone. Foi planejado para o perfil do publicador.
	 * */
	Publicador atualizaPublicadorParcial(Publicador publicador);
	void apagaPublicador(String uuid);

}
