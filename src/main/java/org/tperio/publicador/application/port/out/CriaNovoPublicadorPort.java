package org.tperio.publicador.application.port.out;

import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.domain.Publicador;

public interface CriaNovoPublicadorPort {
	
	Publicador criaNovoPublicador(Publicador publicador) throws TperioBusinessException;

}
