package org.tperio.publicador.application.port.out;

import java.util.List;
import java.util.Optional;

import org.tperio.publicador.domain.Perfil;

public interface RecuperaPerfilPort {
	
	Optional<Perfil> recuperaPerfilPorUuid(String uuid);
	List<Perfil> recuperaPerfis();
	Optional<Perfil> recuperaPerfilPorNome(String descricao);

}
