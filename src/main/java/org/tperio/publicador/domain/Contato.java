package org.tperio.publicador.domain;

public class Contato {

	private String telegram;

	public String getTelegram() {
		return telegram;
	}

	public void setTelegram(String telegram) {
		this.telegram = telegram;
	}

}
