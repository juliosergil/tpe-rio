package org.tperio.publicador.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.tperio.congregacao.domain.Congregacao;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;

public class Publicador {
	private Long idpublicador;
	private String uuid;
	private String nome;
	private String email;
	private String senha;
	private String genero;
	private String telefone;
	private Date datacriacao;
	private Date datadesativado;
	private Perfil perfil;
	private Congregacao congregacao;
	private Contato contato;
	private List<Evento> eventoAtivo;
	
	public Publicador (String uuid) {
		this.uuid = uuid;
	}
	
	public Publicador () {}

	public void validaPublicador() throws TperioBusinessException {
		List<String> sb = new ArrayList<>();
		if (Objects.isNull(nome)) {
			sb.add("Preencha o nome.");
		}
		if (Objects.isNull(email)) {
			sb.add("Preencha o email.");
		}
		if (Objects.isNull(senha)) {
			sb.add("Preencha a senha.");
		}
		if (Objects.isNull(telefone)) {
			sb.add("Preencha o telefone.");
		}
		if (Objects.isNull(congregacao)) {
			sb.add("Preencha a congregação.");
		}
		if (!sb.isEmpty()) {
			throw new TperioBusinessException(sb);
		}
	}
	
	public boolean possuiEventoAtivo() {
		return !Objects.isNull(this.eventoAtivo) && !this.eventoAtivo.isEmpty();
	}
	
	public boolean estaNoEvento(String eventoUuid) {
		return possuiEventoAtivo()
				&& this.eventoAtivo.stream()
				.anyMatch(ev -> ev.getUuid().equals(eventoUuid));
	}
	
	public boolean achouPublicador(List<Publicador> publicadores) {
		return !publicadores.stream()
				.filter(pub -> pub.getUuid().equals(this.getUuid()))
				.collect(Collectors.toList())
				.isEmpty();
	}

	public Congregacao getCongregacao() {
		return congregacao;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public List<Evento> getEventoAtivo() {
		return eventoAtivo;
	}

	public void setEventoAtivo(List<Evento> eventoAtivo) {
		this.eventoAtivo = eventoAtivo;
	}

	public void setCongregacao(Congregacao congregacao) {
		this.congregacao = congregacao;
	}

	public Long getIdpublicador() {
		return idpublicador;
	}

	public boolean isPerfilAdministrador() {
		return perfil.getNome().equals("ADMIN");
	}

	public boolean isPerfilAdministradorPonto() {
		return perfil.getNome().equals("ADMINPONTO");
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public void setIdpublicador(Long idpublicador) {
		this.idpublicador = idpublicador;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Contato getContato() {
		return contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone.replaceAll("[^0-9]", "");
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

}
