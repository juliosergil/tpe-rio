package org.tperio.publicador.domain;

import java.util.Date;

public class Perfil {
	
	private Long idperfil;
	
	private String uuid;
	
	private String nome;
	
	private Date datacriacao;
	
	private Date datadesativado;

	public Perfil(String uuidperfil) {
		this.uuid = uuidperfil;
	}

	public Long getIdperfil() {
		return idperfil;
	}

	public void setIdperfil(Long idperfil) {
		this.idperfil = idperfil;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

	
}
