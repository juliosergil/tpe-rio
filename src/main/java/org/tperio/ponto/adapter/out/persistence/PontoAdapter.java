package org.tperio.ponto.adapter.out.persistence;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.application.port.out.AtualizaPontoPort;
import org.tperio.ponto.application.port.out.CriaNovoPontoPort;
import org.tperio.ponto.application.port.out.RecuperaPontoPort;
import org.tperio.ponto.domain.Ponto;
import org.tperio.ponto.domain.PontoConfiguracao;
import org.tperio.publicador.adapter.out.persistence.PublicadorAdapter;
import org.tperio.publicador.adapter.out.persistence.PublicadorJpaEntity;
import org.tperio.publicador.adapter.out.persistence.PublicadorRepository;
import org.tperio.regiao.adapter.out.persistence.RegiaoAdapter;
import org.tperio.regiao.adapter.out.persistence.RegiaoJpaEntity;
import org.tperio.regiao.adapter.out.persistence.RegiaoRepository;

import lombok.RequiredArgsConstructor;


@Component
@RequiredArgsConstructor
@Slf4j
public class PontoAdapter implements CriaNovoPontoPort, RecuperaPontoPort, AtualizaPontoPort {
	
	private final PontoRepository pontoRepository;
	private final PublicadorRepository publicadorRepository;
	private final PublicadorAdapter publicadorAdapter;
	private final RegiaoAdapter regiaoAdapter;
	private final RegiaoRepository regiaoRepository;

	@Override
	@CacheEvict(value = "pontosCache", allEntries = true)
	public Ponto criaNovoPonto(Ponto ponto) throws TperioBusinessException {
		Optional<PublicadorJpaEntity> responsavel = publicadorRepository.findByUuid(ponto.getResponsavel().getUuid());
		if(responsavel.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.RESPONSAVELNAOENCONTRADO));
		}
		Optional<PublicadorJpaEntity> auxiliar = publicadorRepository.findByUuid(ponto.getAuxiliar().getUuid());
		if(auxiliar.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.AUXILIARNAOENCONTRADO));
		}
		Optional<PublicadorJpaEntity> publicador = publicadorRepository.findByUuid(ponto.getPublicador().getUuid());
		if(publicador.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PUBLICADORNAOENCONTRADO));
		}
		Optional<RegiaoJpaEntity> regiao = regiaoRepository.findByUuid(ponto.getRegiao().getUuid());
		if(regiao.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.REGIAONAOENCONTRADA));
		}
		PontoJpaEntity entity = fromPonto(ponto);
		entity.setAuxiliar(auxiliar.get());
		entity.setPublicador(publicador.get());
		entity.setResponsavel(responsavel.get());
		entity.setRegiao(regiao.get());
		if(!ObjectUtils.isEmpty(ponto.getPontoConfiguracao())) {//Criando uma conf
			PontoConfiguracaoJpaEntity confJpa = new PontoConfiguracaoJpaEntity();
			BeanUtils.copyProperties(ponto.getPontoConfiguracao(), confJpa);
			entity.setConfiguracao(confJpa);
			entity.getConfiguracao().setPonto(entity);
		}
		pontoRepository.save(entity);
		return toPonto(entity);
	}

	@Override
	public Optional<Ponto> recuperaPonto(String nome, String endereco) {
		Optional<PontoJpaEntity> pontoopt = pontoRepository.findByNomeAndEndereco(nome, endereco);
		if(pontoopt.isPresent()) {
			return Optional.of(toPonto(pontoopt.get()));
		}
		return Optional.empty();
	}

	@Override
	@Cacheable(value = "pontosCache")
	public List<Ponto> recuperaPontosAtivos() {
		List<PontoJpaEntity> pontosJpa = pontoRepository.findAllByDatadesativadoIsNull();
		return pontosJpa.stream().filter(ponto -> ponto.getDatadesativado() == null).map(this::toPonto).collect(Collectors.toList());
	}

	@Override
	public Optional<Ponto> recuperaPonto(String uuid) {
		Optional<PontoJpaEntity> pontoopt = pontoRepository.findByUuid(uuid);
		if(pontoopt.isPresent()) {
			return Optional.of(toPonto(pontoopt.get()));
		}
		return Optional.empty();
	}

	@Override
	public List<Ponto> recuperaPontos() {
		List<PontoJpaEntity> pontosJpa = pontoRepository.findAll();
		return pontosJpa.stream().map(this::toPonto).collect(Collectors.toList());
	}

	@Override
	@CacheEvict(value = "pontosCache", allEntries = true)
	public Ponto atualizaPonto(Ponto ponto) throws TperioBusinessException {
		Optional<PontoJpaEntity> pontoJpaOpt = pontoRepository.findByUuid(ponto.getUuid());
		if(pontoJpaOpt.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PONTONAOENCONTRADO));
		}
		PontoJpaEntity entity = pontoJpaOpt.get();
		Optional<PublicadorJpaEntity> responsavel = publicadorRepository.findByUuid(ponto.getResponsavel().getUuid());
		if(responsavel.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.RESPONSAVELNAOENCONTRADO));
		}
		
		Optional<PublicadorJpaEntity> auxiliar = publicadorRepository.findByUuid(ponto.getAuxiliar().getUuid());
		if(auxiliar.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.AUXILIARNAOENCONTRADO));
		}
		
		Optional<PublicadorJpaEntity> publicador = publicadorRepository.findByUuid(ponto.getPublicador().getUuid());
		if(publicador.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PUBLICADORNAOENCONTRADO));
		}
		
		Optional<RegiaoJpaEntity> regiao = regiaoRepository.findByUuid(ponto.getRegiao().getUuid());
		if(regiao.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.REGIAONAOENCONTRADA));
		}
		
		Long id = entity.getIdponto();
		Date dataCriacao = entity.getDatacriacao();
		PontoConfiguracaoJpaEntity pcje = null;
		if(!ObjectUtils.isEmpty(entity.getConfiguracao())) {
			pcje = entity.getConfiguracao();
		}
		BeanUtils.copyProperties(fromPonto(ponto), entity);
		entity.setPublicador(publicador.get());
		entity.setResponsavel(responsavel.get());
		entity.setIdponto(id);
		entity.setRegiao(regiao.get());
		entity.setAuxiliar(auxiliar.get());
		entity.setDatacriacao(dataCriacao);
		if(!ObjectUtils.isEmpty(pcje) && !ObjectUtils.isEmpty(ponto.getPontoConfiguracao())) {//Atualizando um conf
			BeanUtils.copyProperties(ponto.getPontoConfiguracao(), pcje);
			entity.setConfiguracao(pcje);
			entity.getConfiguracao().setPonto(entity);
		}
		if(ObjectUtils.isEmpty(pcje) && !ObjectUtils.isEmpty(ponto.getPontoConfiguracao())) {//Criando uma conf
			PontoConfiguracaoJpaEntity confJpa = new PontoConfiguracaoJpaEntity();
			BeanUtils.copyProperties(ponto.getPontoConfiguracao(), confJpa);
			entity.setConfiguracao(confJpa);
			entity.getConfiguracao().setPonto(entity);
		}
		if(!ObjectUtils.isEmpty(pcje) && ObjectUtils.isEmpty(ponto.getPontoConfiguracao())) {//Removendo uma conf
			entity.setConfiguracao(null);
		}
		
		pontoRepository.save(entity);
		return toPonto(entity);
	}
	
	public PontoJpaEntity fromPonto(Ponto ponto) {
		
		PontoJpaEntity entity = new PontoJpaEntity();
		BeanUtils.copyProperties(ponto, entity);
		return entity;
	}

	public Ponto toPonto(PontoJpaEntity pontoJpa) {
		Ponto ponto = new Ponto(pontoJpa.getUuid());
		BeanUtils.copyProperties(pontoJpa, ponto);
		ponto.setResponsavel(publicadorAdapter.toPublicador(pontoJpa.getResponsavel()));
		ponto.setAuxiliar(publicadorAdapter.toPublicador(pontoJpa.getAuxiliar()));
		if(!ObjectUtils.isEmpty(pontoJpa.getConfiguracao())) {
			ponto.setPontoConfiguracao(toPontoConfiguracao(pontoJpa.getConfiguracao()));
		}
		if(!ObjectUtils.isEmpty(pontoJpa.getRegiao())) {
			ponto.setRegiao(regiaoAdapter.toRegiao(pontoJpa.getRegiao()));
		}
		return ponto;
	}
	
	public PontoConfiguracao toPontoConfiguracao(PontoConfiguracaoJpaEntity entity) {
		PontoConfiguracao pc = new PontoConfiguracao();
		BeanUtils.copyProperties(entity, pc);
		return pc;
	}

	@Override
	public void desativaPonto(String uuid) throws TperioBusinessException {
		PontoJpaEntity pontoEntityOrig = pontoRepository.findByUuid(uuid)
				.orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PONTONAOENCONTRADO)));
		pontoEntityOrig.setDatadesativado(new Date());
		pontoRepository.save(pontoEntityOrig);
	}

}
