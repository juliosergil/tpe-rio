package org.tperio.ponto.adapter.out.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PontoRepository extends JpaRepository<PontoJpaEntity, Long> {
	
	Optional<PontoJpaEntity> findByNomeAndEndereco(String nome, String endereco);

	Optional<PontoJpaEntity> findByUuid(String uuid);

	//@Cacheable(value = "pontosCache", key="#root.methodName", condition="#uuid != null")
	List<PontoJpaEntity> findAllByDatadesativadoIsNull();
}
