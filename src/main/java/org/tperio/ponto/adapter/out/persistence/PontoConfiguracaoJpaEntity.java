package org.tperio.ponto.adapter.out.persistence;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "pontoconfiguracao")
public class PontoConfiguracaoJpaEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idpontoconfiguracao",unique = true)
	private Long idpontoconfiguracao;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idponto", referencedColumnName = "idponto")
    private PontoJpaEntity ponto;
	
	@Column(name = "tipo")
	private String tipo;
	
	@Column(name = "qthorasturno")
	private Integer horasporturno;
	
	@Column(name = "horariosdiarios")
	private String horariosdiarios;

	public Long getIdpontoconfiguracao() {
		return idpontoconfiguracao;
	}

	public void setIdpontoconfiguracao(Long idpontoconfiguracao) {
		this.idpontoconfiguracao = idpontoconfiguracao;
	}

	public PontoJpaEntity getPonto() {
		return ponto;
	}

	public void setPonto(PontoJpaEntity ponto) {
		this.ponto = ponto;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Integer getHorasporturno() {
		return horasporturno;
	}

	public void setHorasporturno(Integer horasporturno) {
		this.horasporturno = horasporturno;
	}

	public String getHorariosdiarios() {
		return horariosdiarios;
	}

	public void setHorariosdiarios(String horariosdiarios) {
		this.horariosdiarios = horariosdiarios;
	}
	
}
