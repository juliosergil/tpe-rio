package org.tperio.ponto.adapter.out.persistence;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

import org.tperio.publicador.adapter.out.persistence.PublicadorJpaEntity;
import org.tperio.regiao.adapter.out.persistence.RegiaoJpaEntity;

@Entity
@Table(name = "ponto")
public class PontoJpaEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idponto",unique = true)
	private Long idponto;
	
	private String uuid = UUID.randomUUID().toString();
	
	private String nome;
	
	private String endereco;
	
	private String latitude;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "idregiao", referencedColumnName = "idregiao")
	private RegiaoJpaEntity regiao;
	
	private String longitude;
	
	private String enderecodeposito;
	
	private String latitudedeposito;
	
	private String longitudedeposito;
	
	@Column(name = "horainicio")
	private String horainicio;
	
	@Column(name = "horafim")
	private String horafim;
	
	private Integer turistico;
	
	private Integer evento;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "idresponsavel", referencedColumnName = "idpublicador")
	private PublicadorJpaEntity responsavel;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "idauxiliar", referencedColumnName = "idpublicador")
	private PublicadorJpaEntity auxiliar;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "idpublicador", referencedColumnName = "idpublicador")
	private PublicadorJpaEntity publicador;

	@OneToOne(mappedBy = "ponto", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private PontoConfiguracaoJpaEntity configuracao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datacriacao")
	private Date datacriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datadesativado")
	private Date datadesativado;

	public Long getIdponto() {
		return idponto;
	}

	public void setIdponto(Long idponto) {
		this.idponto = idponto;
	}

	public String getUuid() {
		return uuid;
	}

	public PublicadorJpaEntity getPublicador() {
		return publicador;
	}

	public void setPublicador(PublicadorJpaEntity publicador) {
		this.publicador = publicador;
	}
	
	public Integer getTuristico() {
		return turistico;
	}

	public void setTuristico(Integer turistico) {
		this.turistico = turistico;
	}

	public Integer getEvento() {
		return evento;
	}

	public void setEvento(Integer evento) {
		this.evento = evento;
	}

	public String getHorainicio() {
		return horainicio;
	}

	public void setHorainicio(String horainicio) {
		this.horainicio = horainicio;
	}

	public String getHorafim() {
		return horafim;
	}

	public void setHorafim(String horafim) {
		this.horafim = horafim;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getNome() {
		return nome;
	}

	public RegiaoJpaEntity getRegiao() {
		return regiao;
	}

	public void setRegiao(RegiaoJpaEntity regiao) {
		this.regiao = regiao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public PontoConfiguracaoJpaEntity getConfiguracao() {
		return configuracao;
	}

	public void setConfiguracao(PontoConfiguracaoJpaEntity configuracao) {
		this.configuracao = configuracao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getEnderecodeposito() {
		return enderecodeposito;
	}

	public void setEnderecodeposito(String enderecodeposito) {
		this.enderecodeposito = enderecodeposito;
	}

	public String getLatitudedeposito() {
		return latitudedeposito;
	}

	public void setLatitudedeposito(String latitudedeposito) {
		this.latitudedeposito = latitudedeposito;
	}

	public String getLongitudedeposito() {
		return longitudedeposito;
	}

	public void setLongitudedeposito(String longitudedeposito) {
		this.longitudedeposito = longitudedeposito;
	}

	public PublicadorJpaEntity getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(PublicadorJpaEntity responsavel) {
		this.responsavel = responsavel;
	}

	public PublicadorJpaEntity getAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar(PublicadorJpaEntity auxiliar) {
		this.auxiliar = auxiliar;
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

}
