package org.tperio.ponto.adapter.in.web;

import org.tperio.ponto.domain.PontoConfiguracao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class PontoConfiguracaoDto {
	private Integer horasporturno;
	private String tipo;
	private String horariosdiarios;
	
	public PontoConfiguracao toPontoConfiguracao() {
		PontoConfiguracao conf = new PontoConfiguracao();
		conf.setHorasporturno(horasporturno);
		conf.setHorariosdiarios(horariosdiarios);
		conf.setTipo(tipo);
		return conf;
	}

	public static PontoConfiguracaoDto fromPontoConfiguracao(PontoConfiguracao pontoConfiguracao) {
		return PontoConfiguracaoDto
				.builder()
				.horasporturno(pontoConfiguracao.getHorasporturno())
				.tipo(pontoConfiguracao.getTipo())
				.horariosdiarios(pontoConfiguracao.getHorariosdiarios())
				.build();
	}
}
