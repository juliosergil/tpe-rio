package org.tperio.ponto.adapter.in.web;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.application.port.in.AtualizaPontoUseCase;
import org.tperio.ponto.application.port.in.CriaNovoPontoUseCase;
import org.tperio.ponto.application.port.in.RecuperaPontoUseCase;
import org.tperio.ponto.domain.Ponto;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/ponto")
public class PontoController {
	
	@Autowired private CriaNovoPontoUseCase criaNovoPontoUseCase;
	@Autowired private RecuperaPontoUseCase recuperaPontoUseCase;
	@Autowired private AtualizaPontoUseCase atualizaPontoUseCase;

	@PostMapping(path = "", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	//@CacheEvict(value = "pontos", allEntries = true)
    public PontoDto criaNovoPonto(
    		@Valid @RequestBody PontoDto pontoDto, 
    		@AuthenticationPrincipal UserDetails user) 
    				throws TperioBusinessException{
    	Ponto ponto = criaNovoPontoUseCase.criaNovoPonto(pontoDto.toPonto());
    	return PontoDto.fromPonto(ponto);
    }
	
	@PutMapping(path = "/{uuid}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	//@CacheEvict(value = "pontos", allEntries = true)
    public PontoDto atualizaPonto(@PathVariable String uuid,
    		@Valid @RequestBody PontoDto pontoDto, 
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException{
    	Ponto ponto = atualizaPontoUseCase.atualizaPonto(pontoDto.toPonto());
    	return PontoDto.fromPonto(ponto);
    }
	
	@GetMapping(path = "/ativos",
		produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PontoDto> recuperaPontosAtivos() throws TperioBusinessException {
		List<Ponto> pontos = recuperaPontoUseCase.recuperaTodosPontoAtivos();
		return pontos.stream().map(PontoDto::fromPonto).collect(Collectors.toList());
	}

	@GetMapping(path = "",
		produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PontoDto> recuperaPontos() {
		List<Ponto> pontos = recuperaPontoUseCase.recuperaTodosPontos();
		return pontos.stream().map(PontoDto::fromPonto).collect(Collectors.toList());
	}
	
	@GetMapping(path = "/{uuid}",
		consumes = MediaType.APPLICATION_JSON_VALUE,
		produces = MediaType.APPLICATION_JSON_VALUE)
	public PontoDto recuperaPontoPorUuid(@PathVariable String uuid) throws TperioBusinessException {
		Ponto ponto = recuperaPontoUseCase.recuperaPontoPorUuid(uuid);
		return PontoDto.fromPonto(ponto);
	}
	
	@GetMapping(path = "/regiao/{regiao}",
		produces = MediaType.APPLICATION_JSON_VALUE)
	public List<PontoDto> recuperaPontosPorRegiao(@PathVariable String regiao) throws TperioBusinessException {
		List<Ponto> ponto = recuperaPontoUseCase.recuperaPontosPorRegiao(regiao);
		return ponto.stream().map(PontoDto::fromPonto).collect(Collectors.toList());
	}
	
	@SuppressWarnings("rawtypes")
	@DeleteMapping(path = "/{uuid}", 
			consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity apagaPonto(
    		@PathVariable String uuid,
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException{
		atualizaPontoUseCase.apagaPonto(uuid);
    	return ResponseEntity.ok().build();
    }
	
}
