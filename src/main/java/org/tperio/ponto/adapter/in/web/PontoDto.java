package org.tperio.ponto.adapter.in.web;

import javax.validation.constraints.NotNull;

import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;
import org.tperio.config.MessageKeys;
import org.tperio.evento.adapter.in.web.EventoAtivoDto;
import org.tperio.ponto.domain.Ponto;
import org.tperio.ponto.domain.PontoConfiguracao;
import org.tperio.publicador.adapter.in.web.PublicadorDto;
import org.tperio.publicador.domain.Publicador;
import org.tperio.regiao.adapter.in.web.RegiaoDto;
import org.tperio.regiao.domain.Regiao;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class PontoDto {

	private String uuid;

	@NotNull(message = MessageKeys.PREENCHANOME)
	private String nome;
	
	@NotNull(message = MessageKeys.PREENCHAENDERECO)
	private String endereco;
	
	@NotNull(message = MessageKeys.PREENCHALAT)
	private String latitude;
	
	@NotNull(message = MessageKeys.PREENCHALONG)
	private String longitude;
	
	@NotNull(message = MessageKeys.PREENCHAENDERECODEP)
	private String enderecodeposito;
	
	@NotNull(message = MessageKeys.PREENCHALATDEP)
	private String latitudedeposito;
	
	@NotNull(message = MessageKeys.PREENCHAREGIAO)
	private String uuidregiao;
	
	@NotNull(message = MessageKeys.PREENCHALONGDEP)
	private String longitudedeposito;
	
	@NotNull(message = MessageKeys.PREENCHARESPONSAVEL)
	private String responsaveluuid;
	
	@NotNull(message = MessageKeys.PREENCHAAUXILIAR)
	private String auxiliaruuid;
	
	private Integer turistico;
	
	private String horainicio;
	
	private String horafim;
	
	private Integer evento;
	
	private PontoConfiguracaoDto pontoConfiguracao;
	
	private String publicadoruuid;
	
	private PublicadorDto responsavel;
	
	private PublicadorDto auxiliar;
	
	private RegiaoDto regiao;

	private Date datadesativado;
	
	private List<EventoAtivoDto> eventosAtivos;

	public static PontoDto fromPonto(Ponto ponto) {
		PontoDto dto = PontoDto.builder()
				.auxiliar(PublicadorDto.fromPublicador(ponto.getAuxiliar()))
				.endereco(ponto.getEndereco())
				.enderecodeposito(ponto.getEnderecodeposito())
				.latitude(ponto.getLatitude())
				.latitudedeposito(ponto.getLatitudedeposito())
				.longitude(ponto.getLongitude())
				.turistico(ponto.getTuristico())
				.horainicio(ponto.getHorainicio())
				.horafim(ponto.getHorafim())
				.evento(ponto.getEvento())
				.longitudedeposito(ponto.getLongitudedeposito())
				.nome(ponto.getNome())
				.datadesativado(ponto.getDatadesativado())
				.uuidregiao(ponto.getRegiao().getUuid())
				.regiao(RegiaoDto.fromRegiao(ponto.getRegiao()))
				.responsavel(PublicadorDto.fromPublicador(ponto.getResponsavel()))
				.uuid(ponto.getUuid())
				.build();
		if (!ObjectUtils.isEmpty(ponto.getPontoConfiguracao())) {
			dto.setPontoConfiguracao(PontoConfiguracaoDto.fromPontoConfiguracao(ponto.getPontoConfiguracao()));
		}
		if (!ObjectUtils.isEmpty(ponto.getEventosAtivos())) {
			dto.setEventosAtivos(
					ponto.getEventosAtivos().stream().map(EventoAtivoDto::fromEvento).collect(Collectors.toList())
			);
		}
		return dto;
	}

	public Ponto toPonto() {
		Ponto ponto = new Ponto(this.getUuid());
		BeanUtils.copyProperties(this, ponto);
		Publicador auxiliarN = new Publicador();
		auxiliarN.setUuid(this.auxiliaruuid);
		ponto.setAuxiliar(auxiliarN);
		Publicador responsavelN = new Publicador();
		responsavelN.setUuid(this.responsaveluuid);
		ponto.setResponsavel(responsavelN);
		Regiao regiao = new Regiao();
		regiao.setUuid(this.uuidregiao);
		ponto.setRegiao(regiao);
		Publicador publiN = new Publicador();
		publiN.setUuid(this.publicadoruuid);
		ponto.setPublicador(publiN);
		if(!ObjectUtils.isEmpty(this.pontoConfiguracao)) {
			PontoConfiguracao conf = this.pontoConfiguracao.toPontoConfiguracao();
			ponto.setPontoConfiguracao(conf);
		}
		
		return ponto;
	}
	
}
