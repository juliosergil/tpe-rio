package org.tperio.ponto.application.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.cache.annotation.CachePut;
import org.tperio.config.MessageKeys;
import org.tperio.evento.application.port.out.RecuperaEventoPort;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.application.port.in.AtualizaPontoUseCase;
import org.tperio.ponto.application.port.in.CriaNovoPontoUseCase;
import org.tperio.ponto.application.port.in.RecuperaPontoUseCase;
import org.tperio.ponto.application.port.out.AtualizaPontoPort;
import org.tperio.ponto.application.port.out.CriaNovoPontoPort;
import org.tperio.ponto.application.port.out.RecuperaPontoPort;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.application.port.out.RecuperaPublicadorPort;
import org.tperio.publicador.domain.Publicador;

public class PontoService implements CriaNovoPontoUseCase, RecuperaPontoUseCase, AtualizaPontoUseCase {

	private CriaNovoPontoPort pontoPort;
	private RecuperaPublicadorPort recuperaPublicadorPort;
	private RecuperaPontoPort recuperaPontoPort;
	private AtualizaPontoPort atualizaPontoPort;
	private RecuperaEventoPort recuperaEventoPort;

	public PontoService(final CriaNovoPontoPort pontoPort,
			final RecuperaPublicadorPort recuperaPublicadorPort,
			final RecuperaPontoPort recuperaPontoPort,
			final AtualizaPontoPort atualizaPontoPort,
			final RecuperaEventoPort recuperaEventoPort) {
		this.recuperaPontoPort = recuperaPontoPort;
		this.pontoPort = pontoPort;
		this.recuperaPublicadorPort = recuperaPublicadorPort;
		this.atualizaPontoPort = atualizaPontoPort;
		this.recuperaEventoPort = recuperaEventoPort;
	}

	@Override
	//@CachePut(value = "pontos", key = "#uuid", condition="#uuid!=null")
	public Ponto criaNovoPonto(final Ponto ponto) throws TperioBusinessException {
		Publicador responsavel = recuperaPublicador(ponto.getResponsavel().getUuid(), "Responsável");
		Publicador auxiliar = recuperaPublicador(ponto.getAuxiliar().getUuid(), "Auxiliar");
		Publicador publicador = recuperaPublicador(ponto.getPublicador().getUuid(), "Publicador logado");
		ponto.setResponsavel(responsavel);
		ponto.setAuxiliar(auxiliar);
		ponto.setPublicador(publicador);
		ponto.validaPonto();
		ponto.setDatacriacao(new Date());
		ponto.setUuid(UUID.randomUUID().toString());
		pontoPort.criaNovoPonto(ponto);
		return ponto;
	}

	private Publicador recuperaPublicador(final String uuid, final String tipo) throws TperioBusinessException {
		Optional<Publicador> publicador = recuperaPublicadorPort.recuperaPublicadorUuid(uuid);
		if (publicador.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(tipo + " não encontrado."));
		}
		return publicador.get();
	}

	@Override
	public List<Ponto> recuperaTodosPontoAtivos() throws TperioBusinessException {
		List<Ponto> pontos = recuperaPontoPort.recuperaPontosAtivos();
		return pontos.stream().map(this::trataEventoAtivo).collect(Collectors.toList());
	}

	@Override
	public Ponto recuperaPontoPorUuid(final String uuid) throws TperioBusinessException {
		Optional<Ponto> ponto = recuperaPontoPort.recuperaPonto(uuid);
		if (ponto.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.PONTONAOENCONTRADO));
		}
		return trataEventoAtivo(ponto.get());
	}

	@Override
	public Ponto atualizaPonto(final Ponto ponto) throws TperioBusinessException {
		recuperaPontoPorUuid(ponto.getUuid());
		Publicador responsavel = recuperaPublicador(ponto.getResponsavel().getUuid(), "Responsável");
		ponto.setResponsavel(responsavel);

		Publicador auxiliar = recuperaPublicador(ponto.getAuxiliar().getUuid(), "Auxiliar");
		ponto.setAuxiliar(auxiliar);

		Publicador publicador = recuperaPublicador(ponto.getPublicador().getUuid(), "Publicador logado");

		ponto.setPublicador(publicador);
		ponto.validaPonto();
		atualizaPontoPort.atualizaPonto(ponto);
		return ponto;
	}

	@Override
	public List<Ponto> recuperaPontosPorRegiao(final String regiao) throws TperioBusinessException {
		List<Ponto> pontos = recuperaPontoPort.recuperaPontosAtivos();
		return pontos.stream().filter(pt -> pt.getRegiao().getUuid().equals(regiao)).map(this::trataEventoAtivo).collect(Collectors.toList());
	}

	@Override
	public List<Ponto> recuperaTodosPontos() {
		List<Ponto> pontos = recuperaPontoPort.recuperaPontos();
		return pontos.stream().collect(Collectors.toList());
	}

	private Ponto trataEventoAtivo(Ponto ponto) {
		List<Evento> eventos = recuperaEventoPort.recuperaEventosAtivos().stream()
			.filter(ev -> ponto.achouPonto(ev.getPontos(), ponto))
			.collect(Collectors.toList());
		if (eventos != null && !eventos.isEmpty()) {
			ponto.setEventosAtivos(eventos);
		}
		return ponto;
	}

	@Override
	public void apagaPonto(final String uuid) throws TperioBusinessException {
		recuperaPontoPort.recuperaPonto(uuid).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.PONTONAOENCONTRADO)));
		atualizaPontoPort.desativaPonto(uuid);
	}

}
