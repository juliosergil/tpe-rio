package org.tperio.ponto.application.port.out;

import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;

public interface CriaNovoPontoPort {

	Ponto criaNovoPonto(Ponto ponto) throws TperioBusinessException;
	
}
