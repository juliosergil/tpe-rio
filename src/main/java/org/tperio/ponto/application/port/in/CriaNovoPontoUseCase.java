package org.tperio.ponto.application.port.in;

import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;

public interface CriaNovoPontoUseCase {

	Ponto criaNovoPonto(Ponto ponto) throws TperioBusinessException;
}
