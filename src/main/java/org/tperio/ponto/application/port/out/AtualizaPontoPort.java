package org.tperio.ponto.application.port.out;

import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;

public interface AtualizaPontoPort {

	Ponto atualizaPonto(Ponto ponto) throws TperioBusinessException;
	void desativaPonto(String uuid) throws TperioBusinessException;
}
