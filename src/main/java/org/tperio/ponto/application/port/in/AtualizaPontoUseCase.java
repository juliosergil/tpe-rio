package org.tperio.ponto.application.port.in;

import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;

public interface AtualizaPontoUseCase {
	
	Ponto atualizaPonto(Ponto ponto) throws TperioBusinessException;
	void apagaPonto(String uuid) throws TperioBusinessException;
	
}
