package org.tperio.ponto.application.port.out;

import java.util.List;
import java.util.Optional;

import org.tperio.ponto.domain.Ponto;

public interface RecuperaPontoPort {

	Optional<Ponto> recuperaPonto(String nome, String endereco);

	List<Ponto> recuperaPontosAtivos();
	
	Optional<Ponto> recuperaPonto(String uuid);

	List<Ponto> recuperaPontos();
}
