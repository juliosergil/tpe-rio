package org.tperio.ponto.application.port.in;

import java.util.List;

import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;

public interface RecuperaPontoUseCase {

	List<Ponto> recuperaTodosPontoAtivos() throws TperioBusinessException;
	Ponto recuperaPontoPorUuid(String uuid) throws TperioBusinessException;
	List<Ponto> recuperaPontosPorRegiao(String regiao)throws TperioBusinessException;

	List<Ponto> recuperaTodosPontos();
}
