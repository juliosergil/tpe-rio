package org.tperio.ponto.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.tperio.config.MessageKeys;
import org.tperio.config.Messages;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.domain.Publicador;
import org.tperio.regiao.domain.Regiao;

public final class Ponto {

	private String uuid;
	private String nome;
	private String endereco;
	private String latitude;
	private String longitude;
	private String enderecodeposito;
	private String latitudedeposito;
	private String longitudedeposito;
	private Regiao regiao;
	private Integer turistico;
	private Integer evento;
	private Publicador responsavel;
	private Publicador auxiliar;
	private Publicador publicador;
	private String horainicio;
	private String horafim;
	private Date datacriacao;
	private Date datadesativado;
	private List<Evento> eventosAtivos;
	private PontoConfiguracao pontoConfiguracao;

	public Ponto(final String uuid) {
		this.uuid = uuid;
	}

	public void validaPonto() throws TperioBusinessException {
		List<String> sb = new ArrayList<>();
		if (Objects.isNull(nome)) {
			adicionaMensagem(sb, MessageKeys.PREENCHANOME);
		}
		if (Objects.isNull(endereco)) {
			adicionaMensagem(sb, MessageKeys.PREENCHAENDERECO);
		}
		if (Objects.isNull(latitude)) {
			adicionaMensagem(sb, MessageKeys.PREENCHALAT);
		}
		if (Objects.isNull(longitude)) {
			adicionaMensagem(sb, MessageKeys.PREENCHALONG);
		}
		if (Objects.isNull(regiao)) {
			adicionaMensagem(sb, MessageKeys.PREENCHAREGIAO);
		}
		if (Objects.isNull(enderecodeposito)) {
			adicionaMensagem(sb, MessageKeys.PREENCHAENDERECODEP);
		}
		if (Objects.isNull(latitudedeposito)) {
			adicionaMensagem(sb, MessageKeys.PREENCHALATDEP);
		}
		if (Objects.isNull(longitudedeposito)) {
			adicionaMensagem(sb, MessageKeys.PREENCHALONGDEP);
		}
		if (Objects.isNull(responsavel)) {
			adicionaMensagem(sb, MessageKeys.PREENCHARESPONSAVEL);
		} else {
			if (!responsavel.isPerfilAdministrador() && !responsavel.isPerfilAdministradorPonto()) {
				adicionaMensagem(sb, MessageKeys.RESPONSAVELSEMPERMISSAO);
			}
		}
		if (Objects.isNull(auxiliar)) {
			adicionaMensagem(sb, MessageKeys.PREENCHAAUXILIAR);
		} else {
			if (!auxiliar.isPerfilAdministrador() && !auxiliar.isPerfilAdministradorPonto()) {
				adicionaMensagem(sb, MessageKeys.AUXILIARSEMPERMISSAO);
			}
		}

		if (!sb.isEmpty()) {
			throw new TperioBusinessException(sb);
		}
	}
	
	public void adicionaEventoAtivo(List<Evento> eventos) {
		List<Evento> eventosFim = eventos.stream()
			.filter(ev -> this.achouPonto(ev.getPontos(), this))
			.collect(Collectors.toList());
		if (eventosFim != null && !eventosFim.isEmpty()) {
			this.setEventosAtivos(eventosFim);
		}
	}
	
	public boolean pontoPossuiEventoAtivo() {
		return !Objects.isNull(this.eventosAtivos) && this.eventosAtivos.size() > 0;
	}
	
	public Integer getTuristico() {
		return turistico;
	}

	public void setTuristico(Integer turistico) {
		this.turistico = turistico;
	}

	public String getHorainicio() {
		return horainicio;
	}

	public void setHorainicio(String horainicio) {
		this.horainicio = horainicio;
	}

	public String getHorafim() {
		return horafim;
	}

	public void setHorafim(String horafim) {
		this.horafim = horafim;
	}

	public Integer getEvento() {
		return evento;
	}

	public void setEvento(Integer evento) {
		this.evento = evento;
	}

	public boolean achouPonto(List<Ponto> pontos, Ponto ponto) {
		return !pontos.stream().filter(pt -> pt.getUuid().equals(ponto.getUuid())).collect(Collectors.toList()).isEmpty();
	}


	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

	private void adicionaMensagem(List<String> sb, String key) {
		sb.add(Messages.getInstance().getMessage(key));
	}

	public Publicador getPublicador() {
		return publicador;
	}

	public void setPublicador(final Publicador publicador) {
		this.publicador = publicador;
	}

	public List<Evento> getEventosAtivos() {
		return eventosAtivos;
	}

	public void setEventosAtivos(List<Evento> eventosAtivos) {
		this.eventosAtivos = eventosAtivos;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(final String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(final String endereco) {
		this.endereco = endereco;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(final String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(final String longitude) {
		this.longitude = longitude;
	}

	public String getEnderecodeposito() {
		return enderecodeposito;
	}

	public void setEnderecodeposito(final String enderecodeposito) {
		this.enderecodeposito = enderecodeposito;
	}

	public String getLatitudedeposito() {
		return latitudedeposito;
	}

	public void setLatitudedeposito(final String latitudedeposito) {
		this.latitudedeposito = latitudedeposito;
	}

	public PontoConfiguracao getPontoConfiguracao() {
		return pontoConfiguracao;
	}

	public void setPontoConfiguracao(PontoConfiguracao pontoConfiguracao) {
		this.pontoConfiguracao = pontoConfiguracao;
	}

	public String getLongitudedeposito() {
		return longitudedeposito;
	}

	public void setLongitudedeposito(final String longitudedeposito) {
		this.longitudedeposito = longitudedeposito;
	}

	public Publicador getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(final Publicador responsavel) {
		this.responsavel = responsavel;
	}

	public Publicador getAuxiliar() {
		return auxiliar;
	}

	public void setAuxiliar(final Publicador auxiliar) {
		this.auxiliar = auxiliar;
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public void setDatacriacao(final Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(final Date datadesativado) {
		this.datadesativado = datadesativado;
	}

}
