package org.tperio.ponto.domain;

public class PontoConfiguracao {

	private String tipo;
	private Integer horasporturno;
	private String horariosdiarios;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Integer getHorasporturno() {
		return horasporturno;
	}
	public void setHorasporturno(Integer horasporturno) {
		this.horasporturno = horasporturno;
	}
	public String getHorariosdiarios() {
		return horariosdiarios;
	}
	public void setHorariosdiarios(String horariosdiarios) {
		this.horariosdiarios = horariosdiarios;
	}
	
}
