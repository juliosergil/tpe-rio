package org.tperio.evento.adapter.out.persistence;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import jakarta.persistence.*;

import org.tperio.ponto.adapter.out.persistence.PontoJpaEntity;
import org.tperio.publicador.adapter.out.persistence.PublicadorJpaEntity;

@Entity
@Table(name = "evento")
public class EventoJpaEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idevento",unique = true)
	private Long idevento;
	
	private String uuid = UUID.randomUUID().toString();
	
	private String nome;
	
	private String caminhobanner;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_inicio")
	private Date dataInicio;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "data_fim")
	private Date dataFim;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datacriacao")
	private Date datacriacao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datadesativado")
	private Date datadesativado;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="pontosevento",
		joinColumns={@JoinColumn(name="idevento")},
		inverseJoinColumns={@JoinColumn(name="idponto")})
	private List<PontoJpaEntity> pontos = new ArrayList<>();
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name="publicadoresevento", 
		joinColumns={@JoinColumn(name="idevento")}, 
		inverseJoinColumns={@JoinColumn(name="idpublicador")})
	private List<PublicadorJpaEntity> publicadores = new ArrayList<>();

	public List<PontoJpaEntity> getPontos() {
		return pontos;
	}

	public void setPontos(List<PontoJpaEntity> pontos) {
		this.pontos = pontos;
	}

	public Long getIdevento() {
		return idevento;
	}

	public void setIdevento(Long idevento) {
		this.idevento = idevento;
	}

	public List<PublicadorJpaEntity> getPublicadores() {
		return publicadores;
	}

	public void setPublicadores(List<PublicadorJpaEntity> publicadores) {
		this.publicadores = publicadores;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCaminhobanner() {
		return caminhobanner;
	}

	public void setCaminhobanner(String caminhobanner) {
		this.caminhobanner = caminhobanner;
	}

	public Date getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
}
