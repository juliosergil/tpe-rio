package org.tperio.evento.adapter.out.persistence;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventoRepository extends JpaRepository<EventoJpaEntity, Long> {
	
	List<EventoJpaEntity> findByNome(String nome);

	Optional<EventoJpaEntity> findByUuid(String uuid);

	List<EventoJpaEntity> findAllByDatadesativadoIsNull();
}
