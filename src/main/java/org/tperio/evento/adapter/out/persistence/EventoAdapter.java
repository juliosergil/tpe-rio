package org.tperio.evento.adapter.out.persistence;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.tperio.StringUteis;
import org.tperio.config.MessageKeys;
import org.tperio.evento.application.port.out.AtualizaEventoPort;
import org.tperio.evento.application.port.out.CriaEventoPort;
import org.tperio.evento.application.port.out.DesativaEventoPort;
import org.tperio.evento.application.port.out.RecuperaEventoPort;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.adapter.out.persistence.PontoAdapter;
import org.tperio.ponto.adapter.out.persistence.PontoJpaEntity;
import org.tperio.ponto.adapter.out.persistence.PontoRepository;
import org.tperio.publicador.adapter.out.persistence.PublicadorAdapter;
import org.tperio.publicador.adapter.out.persistence.PublicadorJpaEntity;
import org.tperio.publicador.adapter.out.persistence.PublicadorRepository;
import org.tperio.service.FileUploadService;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class EventoAdapter implements RecuperaEventoPort, CriaEventoPort, AtualizaEventoPort, DesativaEventoPort {
	
	private final EventoRepository eventoRepository;
	private final PontoRepository pontoRepository;
	private final FileUploadService fileUploadService;
	private final PontoAdapter pontoAdapter;
	private final PublicadorRepository publicadorRepository;
	private final PublicadorAdapter publicadorAdapter;

	@Override
	public List<Evento> recuperaEventoPorDataInicioEDataFim(Date dataInicio, Date dataFim)
			throws TperioBusinessException {
		//TODO eventoRepository.findByDataInicioAndDataFim(dataInicio, dataFim);
		return List.of();
	}

	@Override
	public List<Evento> recuperaEventoPorNome(String nome) throws TperioBusinessException {
		List<EventoJpaEntity> lista = eventoRepository.findByNome(nome);
		if(!ObjectUtils.isEmpty(lista)) {
			return lista.stream().map(this::toEvento).collect(Collectors.toList());
		}
		return List.of();
	}
	
	private Evento toEvento(EventoJpaEntity entity) {
		Evento eventoT = new Evento();
		BeanUtils.copyProperties(entity, eventoT);
		if(!ObjectUtils.isEmpty(entity.getPontos())) {
			eventoT.getPontos().addAll(entity.getPontos().stream()
					.map(pontoAdapter::toPonto).collect(Collectors.toList()));
		}
		if(!ObjectUtils.isEmpty(entity.getPublicadores())) {
			eventoT.getPublicadores().addAll(entity.getPublicadores().stream()
					.map(publicadorAdapter::toPublicador).collect(Collectors.toList()));
		}
		return eventoT;
	}

	@Override
	@CacheEvict(value = "eventosCache", allEntries = true)
	public Evento criaEvento(Evento evento) throws TperioBusinessException {
		EventoJpaEntity entity = new EventoJpaEntity();
		if(evento.getBanner() != null 
				&& evento.getBanner().length > 0
				&& !StringUteis.isNullOrEmpty(evento.getCaminhobanner())) {
			fileUploadService.uploadEventoBanner(evento);
		} else {
			evento.setCaminhobanner(null);
			evento.setBanner(null);
		}
		BeanUtils.copyProperties(evento, entity);
		if(!ObjectUtils.isEmpty(evento.getPontos())) {
			List<Optional<PontoJpaEntity>> pontos = evento.getPontos().stream().map(pt -> pontoRepository.findByUuid(pt.getUuid())).collect(Collectors.toList());
			entity.getPontos().addAll(pontos.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
		}
		if(!ObjectUtils.isEmpty(evento.getPublicadores())) {
			List<Optional<PublicadorJpaEntity>> pubs = evento.getPublicadores().stream().map(pt -> publicadorRepository.findByUuid(pt.getUuid())).collect(Collectors.toList());
			entity.getPublicadores().addAll(pubs.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
		}
		entity = eventoRepository.save(entity);
		
		return this.toEvento(entity);
	}

	@Override
	public List<Evento> recuperaEventosAtivos() {
		return eventoRepository.findAllByDatadesativadoIsNull().stream().map(this::toEvento).filter(e -> !e.isDesativado()).collect(Collectors.toList());
	}

	@Override
	public Optional<Evento> recuperaEventoPorUuid(String eventoUuid) {
		Optional<EventoJpaEntity> entity = eventoRepository.findByUuid(eventoUuid);
		if (entity.isPresent()) {
			return Optional.of(this.toEvento(entity.get()));
		}
		return Optional.empty();
	}

	@Override
	@Cacheable(value = "eventosCache")
	public List<Evento> recuperaEventosAtivosAgendamento() {
		return eventoRepository.findAllByDatadesativadoIsNull().stream().map(this::toEventoAgendamento).filter(e -> !e.isDesativado()).collect(Collectors.toList());
	}

	@Override
	public List<Evento> recuperaEventos() {
		return eventoRepository.findAll().stream().map(this::toEvento).collect(Collectors.toList());
	}

	private Evento toEventoAgendamento(EventoJpaEntity entity) {
		Evento eventoT = new Evento();
		BeanUtils.copyProperties(entity, eventoT);
		if(!ObjectUtils.isEmpty(entity.getPontos())) {
			eventoT.getPontos().addAll(entity.getPontos().stream()
					.map(pontoAdapter::toPonto).collect(Collectors.toList()));
		}
		return eventoT;
	}

	@Override
	@CacheEvict(value = "eventosCache", allEntries = true)
	public Evento atualizaEvento(Evento evento) throws TperioBusinessException {
		EventoJpaEntity entity = eventoRepository.findByUuid(evento.getUuid()).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.EVENTONAOENCONTRADO)));
		if(deveUpparBanner(evento)) {
			fileUploadService.uploadEventoBanner(evento);
		} else {
			evento.setCaminhobanner(null);
			evento.setBanner(null);
		}
		if(deveDeletarBanner(evento, entity)){
			fileUploadService.apagaEventoBanner(entity.getCaminhobanner());
		}
		Long id = entity.getIdevento();
		Date dataCriacao = entity.getDatacriacao();
		BeanUtils.copyProperties(fromEvento(evento), entity);
		entity.setIdevento(id);
		entity.setDatacriacao(dataCriacao);
		if(!ObjectUtils.isEmpty(evento.getPontos())) {
			List<Optional<PontoJpaEntity>> pontos = evento.getPontos().stream().map(pt -> pontoRepository.findByUuid(pt.getUuid())).collect(Collectors.toList());
			entity.getPontos().addAll(pontos.stream()
					.filter(Optional::isPresent)
					.filter(pt -> notExistsIn(entity.getPontos(), pt.get()))
					.map(Optional::get)
					.collect(Collectors.toList()));
		}
		if(!ObjectUtils.isEmpty(evento.getPublicadores())) {
			List<Optional<PublicadorJpaEntity>> publicadores = evento.getPublicadores().stream().map(pt -> publicadorRepository.findByUuid(pt.getUuid())).collect(Collectors.toList());
			entity.getPublicadores().addAll(publicadores.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
		}
		eventoRepository.save(entity);
		
		return toEvento(entity);
	}

	private boolean notExistsIn(List<PontoJpaEntity> pontos, PontoJpaEntity ponto) {
		return pontos.stream().filter(p -> p.getUuid() == ponto.getUuid()).findFirst().isEmpty();
	}
	
	private EventoJpaEntity fromEvento(Evento evento) {
		EventoJpaEntity entity = new EventoJpaEntity();
		if(!deveUpparBanner(evento)) {
			evento.setCaminhobanner(null);
			evento.setBanner(null);
		}
		BeanUtils.copyProperties(evento, entity);
		if(!ObjectUtils.isEmpty(evento.getPontos())) {
			List<Optional<PontoJpaEntity>> pontos = evento.getPontos().stream().map(pt -> pontoRepository.findByUuid(pt.getUuid())).collect(Collectors.toList());
			entity.getPontos().addAll(pontos.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
		}
		return entity;
	}
	
	private boolean deveUpparBanner(Evento evento) {
		return evento.getBanner() != null 
				&& evento.getBanner().length > 0
				&& !StringUteis.isNullOrEmpty(evento.getCaminhobanner());
	}

	private boolean deveDeletarBanner(Evento evento, EventoJpaEntity entity) {
		return !StringUteis.isNullOrEmpty(entity.getCaminhobanner()) && evento.getBanner() != null && evento.getBanner().length > 0;
	}

	@Override
	public void desativaEvento(String uuid) throws TperioBusinessException {
		EventoJpaEntity entity = eventoRepository.findByUuid(uuid).orElseThrow(() -> new TperioBusinessException(List.of(MessageKeys.EVENTONAOENCONTRADO)));
		entity.setDatadesativado(new Date());
		eventoRepository.save(entity);
	}
}
