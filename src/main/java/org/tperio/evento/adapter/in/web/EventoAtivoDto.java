package org.tperio.evento.adapter.in.web;

import java.time.LocalDate;

import org.tperio.agendamento.adapter.in.web.AgendamentoDto;
import org.tperio.evento.domain.Evento;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class EventoAtivoDto {
	String uuid;
	String nome;
	@JsonFormat(pattern = "dd/MM/yyyy")
	LocalDate dataInicio;
	@JsonFormat(pattern = "dd/MM/yyyy")
	LocalDate dataFim;
	String caminhobanner;
	
	public static EventoAtivoDto fromEvento(Evento evento) {
		return EventoAtivoDto.builder()
				.uuid(evento.getUuid())
				.nome(evento.getNome())
				.dataInicio(AgendamentoDto.convertToLocalDate(evento.getDataInicio()))
				.dataFim(AgendamentoDto.convertToLocalDate(evento.getDataFim()))
				.caminhobanner(evento.getCaminhobanner())
				.build();
	}
}
