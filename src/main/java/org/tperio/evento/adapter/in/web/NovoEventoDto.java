package org.tperio.evento.adapter.in.web;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.springframework.util.ObjectUtils;
import org.tperio.agendamento.adapter.in.web.AgendamentoDto;
import org.tperio.config.MessageKeys;
import org.tperio.evento.domain.Evento;
import org.tperio.ponto.adapter.in.web.PontoDto;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.adapter.in.web.PublicadorDto;
import org.tperio.publicador.domain.Publicador;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class NovoEventoDto {
	
	String uuid;
	@NotNull(message = MessageKeys.PREENCHADATA)
	String nome;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = MessageKeys.PREENCHADATAINICIAL)
	LocalDate dataInicio;
	@JsonFormat(pattern = "dd/MM/yyyy")
	@NotNull(message = MessageKeys.PREENCHADATAFINAL)
	LocalDate dataFim;
	String caminhobanner;
	byte[] arquivo;
	List<String> pontosuuid;
	List<String> publicadoresuuid;
	Date datadesativado;
	@Builder.Default
	List<PontoDto> pontos = new ArrayList<>();
	@Builder.Default
	List<PublicadorDto> publicadores = new ArrayList<>();
	
	public Evento toEvento() {
		Evento evento = new Evento();
		evento.setNome(nome);
		if(arquivo != null) {
			evento.setCaminhobanner(caminhobanner);
			evento.setBanner(Arrays.copyOf(arquivo, arquivo.length));
		}
		if(!ObjectUtils.isEmpty(pontosuuid)) {
			evento.getPontos().addAll(pontosuuid.stream().map(Ponto::new).collect(Collectors.toList()));
		}
		if(!ObjectUtils.isEmpty(publicadoresuuid)) {
			evento.getPublicadores().addAll(publicadoresuuid.stream().map(Publicador::new).collect(Collectors.toList()));
		}
		evento.setDataInicio(AgendamentoDto.convertToDateUsingInstant(dataInicio));
		evento.setDataFim(AgendamentoDto.convertToDateUsingInstant(dataFim));
		return evento;
	}
	public static NovoEventoDto fromEvento(Evento evento) {
		NovoEventoDto dto = NovoEventoDto.builder()
				.dataInicio(AgendamentoDto.convertToLocalDate(evento.getDataInicio()))
				.dataFim(AgendamentoDto.convertToLocalDate(evento.getDataFim()))
				.nome(evento.getNome())
				.uuid(evento.getUuid())
				.datadesativado(evento.getDatadesativado())
				.caminhobanner(evento.getCaminhobanner())
				.build();
		if(!ObjectUtils.isEmpty(evento.getPontos())) {
			dto.getPontos().addAll(
					evento.getPontos()
					.stream()
					.map(PontoDto::fromPonto)
					.collect(Collectors.toList()));
		}
		if(!ObjectUtils.isEmpty(evento.getPublicadores())) {
			dto.getPublicadores().addAll(
					evento.getPublicadores()
					.stream()
					.map(PublicadorDto::fromPublicador)
					.collect(Collectors.toList()));
		}
		return dto;
	}

}
