package org.tperio.evento.adapter.in.web;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.tperio.evento.application.port.in.AdicionaPontoEventoUseCase;
import org.tperio.evento.application.port.in.AdicionaPublicadorEventoUseCase;
import org.tperio.evento.application.port.in.AtualizaEventoUseCase;
import org.tperio.evento.application.port.in.CriaEventoUseCase;
import org.tperio.evento.application.port.in.DesativaEventoUseCase;
import org.tperio.evento.application.port.in.RecuperaEventoUseCase;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/evento")
@RequiredArgsConstructor
public class EventoController {
	
	private final CriaEventoUseCase criaNovoEventoUseCase;
	private final AtualizaEventoUseCase atualizaEventoUseCase;
	private final AdicionaPontoEventoUseCase adicionaPontoEventoUseCase;
	private final AdicionaPublicadorEventoUseCase adicionaPublicadorEventoUseCase;
	private final RecuperaEventoUseCase recuperaEventoUseCase;
	private final DesativaEventoUseCase desativaEventoUseCase;
	private final ObjectMapper mapper;

	@PostMapping(path = "", 
			consumes = {"multipart/form-data", MediaType.APPLICATION_JSON_VALUE},
			produces = MediaType.APPLICATION_JSON_VALUE)
    public NovoEventoDto criaNovoEvento(
    		@RequestPart("banner") Optional<MultipartFile> banneropt,
    		@Valid @RequestParam("evento") String novoEventoStr,
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException, IOException {
		NovoEventoDto novoEventoDto = mapper.readValue(novoEventoStr, NovoEventoDto.class);
		if(banneropt.isPresent()) {
			novoEventoDto.setCaminhobanner(banneropt.get().getOriginalFilename());
			novoEventoDto.setArquivo(Arrays.copyOf(banneropt.get().getBytes(), banneropt.get().getBytes().length));
		}
		Evento evento = criaNovoEventoUseCase.criaNovoEvento(novoEventoDto.toEvento());
    	return NovoEventoDto.fromEvento(evento);
    }
	
	@PutMapping(path = "", 
			consumes = {"multipart/form-data", MediaType.APPLICATION_JSON_VALUE},
			produces = MediaType.APPLICATION_JSON_VALUE)
    public NovoEventoDto atualizaEvento(
    		@RequestPart("banner") Optional<MultipartFile> banneropt,
    		@Valid @RequestParam("evento") String eventoStr,
    		@AuthenticationPrincipal UserDetails user)
    				throws TperioBusinessException, IOException {
		EventoDto novoEventoDto = mapper.readValue(eventoStr, EventoDto.class);
		if(banneropt.isPresent()) {
			novoEventoDto.setCaminhobanner(banneropt.get().getOriginalFilename());
			novoEventoDto.setArquivo(Arrays.copyOf(banneropt.get().getBytes(), banneropt.get().getBytes().length));
		}
		Evento evento = atualizaEventoUseCase.atualizaEvento(novoEventoDto.toEvento());
    	return NovoEventoDto.fromEvento(evento);
    }

	@DeleteMapping(path = "/{uuid}",
		produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity apagaPublicador(
		@PathVariable String uuid,
		@AuthenticationPrincipal UserDetails user)
		throws TperioBusinessException {
		desativaEventoUseCase.desativaEvento(uuid);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping(path = "/ativos", 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public List<NovoEventoDto> recuperaEventosAtivos() throws TperioBusinessException {
		return recuperaEventoUseCase.recuperaEventosAtivos().stream().map(NovoEventoDto::fromEvento).collect(Collectors.toList());
    }

	@GetMapping(path = "",
		produces = MediaType.APPLICATION_JSON_VALUE)
	public List<NovoEventoDto> recuperaEventos() {
		return recuperaEventoUseCase.recuperaEventos().stream().map(NovoEventoDto::fromEvento).collect(Collectors.toList());
	}

	@GetMapping(path = "/agendamento/ativos",
			produces = MediaType.APPLICATION_JSON_VALUE)
	public List<NovoEventoDto> recuperaEventosAtivosParaAgendamento() throws TperioBusinessException {
		return recuperaEventoUseCase.recuperaEventosAtivosAgendamento().stream().map(NovoEventoDto::fromEvento).collect(Collectors.toList());
	}
	
	@PostMapping(path = "/{uuid}/ponto", 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> adicionaPontosEventoAtivos(
    		@PathVariable String uuid,
    		@Valid @RequestBody List<String> pontosUuid) throws TperioBusinessException {
		adicionaPontoEventoUseCase.adicionaPontosEvento(uuid, pontosUuid);
		return ResponseEntity.noContent().build();
    }
	
	@PostMapping(path = "/{uuid}/publicador", 
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> adicionaPublicadoresEventoAtivo(
    		@PathVariable String uuid,
    		@Valid @RequestBody List<String> publicadoresUuid) throws TperioBusinessException {
		adicionaPublicadorEventoUseCase.adicionaPublicadoresEvento(uuid, publicadoresUuid);
		return ResponseEntity.noContent().build();
    }
}
