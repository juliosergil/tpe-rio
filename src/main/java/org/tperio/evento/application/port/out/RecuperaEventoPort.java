package org.tperio.evento.application.port.out;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;

public interface RecuperaEventoPort {

	/**
	 * Recupera eventos usando datas
	 * */
	List<Evento> recuperaEventoPorDataInicioEDataFim(Date dataInicio, Date dataFim) throws TperioBusinessException;
	
	/**
	 * Recupera eventos por nome
	 * */
	List<Evento> recuperaEventoPorNome(String nome) throws TperioBusinessException;
	
	/**
	 * Recupera todos os eventos ativos, ou seja: com data de desativação igual a nulo.
	 * */
	List<Evento> recuperaEventosAtivos();

	Optional<Evento> recuperaEventoPorUuid(String eventoUuid);

	List<Evento> recuperaEventosAtivosAgendamento();

    List<Evento> recuperaEventos();
}
