package org.tperio.evento.application.port.out;

import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;

public interface AtualizaEventoPort {
	
	Evento atualizaEvento(Evento evento) throws TperioBusinessException;

}
