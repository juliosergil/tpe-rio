package org.tperio.evento.application.port.in;

import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;

public interface CriaEventoUseCase {

	Evento criaNovoEvento(Evento evento) throws TperioBusinessException;
	
}
