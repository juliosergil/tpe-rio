package org.tperio.evento.application.port.in;

import java.util.List;

import org.tperio.exception.TperioBusinessException;

public interface AdicionaPublicadorEventoUseCase {

	void adicionaPublicadoresEvento(String eventoUuid, List<String> uuidsPublicadoress) throws TperioBusinessException;
}
