package org.tperio.evento.application.port.in;

import org.tperio.exception.TperioBusinessException;

public interface DesativaEventoUseCase {

    void desativaEvento(String uuid) throws TperioBusinessException;
}
