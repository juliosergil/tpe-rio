package org.tperio.evento.application.port.out;

import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;

public interface CriaEventoPort {

	Evento criaEvento(Evento evento) throws TperioBusinessException;
}
