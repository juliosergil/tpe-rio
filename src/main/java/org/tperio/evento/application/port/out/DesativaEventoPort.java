package org.tperio.evento.application.port.out;

import org.tperio.exception.TperioBusinessException;

public interface DesativaEventoPort {

    void desativaEvento(String uuid) throws TperioBusinessException;
}
