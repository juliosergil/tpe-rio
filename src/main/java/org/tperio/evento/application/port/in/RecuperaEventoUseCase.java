package org.tperio.evento.application.port.in;

import java.util.List;
import java.util.Optional;

import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;

public interface RecuperaEventoUseCase {

	List<Evento> recuperaEventosAtivos() throws TperioBusinessException;

	List<Evento> recuperaEventos();

	List<Evento> recuperaEventosAtivosAgendamento();
}
