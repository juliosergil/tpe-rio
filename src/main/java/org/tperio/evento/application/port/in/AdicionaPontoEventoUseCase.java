package org.tperio.evento.application.port.in;

import java.util.List;

import org.tperio.exception.TperioBusinessException;

public interface AdicionaPontoEventoUseCase {

	void adicionaPontosEvento(String eventoUuid, List<String> uuidsPontos) throws TperioBusinessException;
}
