package org.tperio.evento.application.service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.cache.annotation.CachePut;
import org.tperio.config.MessageKeys;
import org.tperio.evento.application.port.in.AdicionaPontoEventoUseCase;
import org.tperio.evento.application.port.in.AdicionaPublicadorEventoUseCase;
import org.tperio.evento.application.port.in.AtualizaEventoUseCase;
import org.tperio.evento.application.port.in.CriaEventoUseCase;
import org.tperio.evento.application.port.in.RecuperaEventoUseCase;
import org.tperio.evento.application.port.out.AtualizaEventoPort;
import org.tperio.evento.application.port.out.CriaEventoPort;
import org.tperio.evento.application.port.out.RecuperaEventoPort;
import org.tperio.evento.domain.Evento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.application.port.out.RecuperaPontoPort;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.application.port.out.RecuperaPublicadorPort;
import org.tperio.publicador.domain.Publicador;

public class EventoService implements CriaEventoUseCase, 
										RecuperaEventoUseCase, 
										AdicionaPontoEventoUseCase,
										AdicionaPublicadorEventoUseCase,
										AtualizaEventoUseCase {
	
	private RecuperaPontoPort recuperaPontoPort;
	private RecuperaEventoPort recuperaEventoPort;
	private CriaEventoPort criaEventoPort;
	private AtualizaEventoPort atualizaEventoPort;
	private RecuperaPublicadorPort recuperaPublicadorPort;
	
	public EventoService(RecuperaEventoPort recuperaEventoPort,
			CriaEventoPort criaEventoPort,
			RecuperaPontoPort recuperaPontoPort,
			AtualizaEventoPort atualizaEventoPort,
			RecuperaPublicadorPort recuperaPublicadorPort) {
		this.recuperaEventoPort = recuperaEventoPort;
		this.criaEventoPort = criaEventoPort;
		this.recuperaPontoPort = recuperaPontoPort;
		this.atualizaEventoPort = atualizaEventoPort;
		this.recuperaPublicadorPort = recuperaPublicadorPort;
	}

	@Override
	//@CachePut(value = "eventos", key = "#uuid", condition="#uuid!=null")
	public Evento criaNovoEvento(Evento evento) throws TperioBusinessException {
		evento.validaEvento();
		if(!Objects.isNull(evento.getPontos()) && !evento.getPontos().isEmpty()) {
			evento.getPontos().stream().map(ponto -> recuperaPontoPort.recuperaPonto(ponto.getUuid()));
		}
		if(!Objects.isNull(evento.getPublicadores()) && !evento.getPublicadores().isEmpty()) {
			evento.getPublicadores().stream().map(pub -> recuperaPublicadorPort.recuperaPublicadorUuid(pub.getUuid()));
			
		}
		List<Evento> listaevento = recuperaEventoPort.recuperaEventoPorNome(evento.getNome());
		if(!Objects.isNull(listaevento) && !listaevento.isEmpty()) {
			throw new TperioBusinessException(Arrays.asList(MessageKeys.EVENTOJAEXISTE));
		}
		evento.setDatacriacao(new Date());
		evento.setUuid(UUID.randomUUID().toString());
		return criaEventoPort.criaEvento(evento);
	}

	@Override
	public List<Evento> recuperaEventosAtivos() throws TperioBusinessException {
		return recuperaEventoPort.recuperaEventosAtivos();
	}

	@Override
	public List<Evento> recuperaEventos() {
		return recuperaEventoPort.recuperaEventos();
	}

	@Override
	public List<Evento> recuperaEventosAtivosAgendamento() {
		return recuperaEventoPort.recuperaEventosAtivosAgendamento();
	}

	@Override
	public void adicionaPontosEvento(String eventoUuid, List<String> uuidsPontos) throws TperioBusinessException {
		Evento evento = recuperaEventoPort.recuperaEventoPorUuid(eventoUuid).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.EVENTONAOENCONTRADO)));
		List<Optional<Ponto>> pontos = uuidsPontos.stream().map(str -> recuperaPontoPort.recuperaPonto(str)).collect(Collectors.toList());
		evento.setPontos(pontos.stream().filter(Optional::isPresent).map(Optional::get).filter(pt -> !pt.pontoPossuiEventoAtivo()).collect(Collectors.toList()));
		atualizaEventoPort.atualizaEvento(evento);
	}

	@Override
	public Evento atualizaEvento(Evento evento) throws TperioBusinessException {
		evento.validaEvento();
		recuperaEventoPort.recuperaEventoPorUuid(evento.getUuid()).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.EVENTONAOENCONTRADO)));
		List<Optional<Ponto>> pontos = evento.getPontos().stream().map(str -> recuperaPontoPort.recuperaPonto(str.getUuid())).collect(Collectors.toList());
		List<Optional<Publicador>> publicadores = evento.getPublicadores().stream().map(str -> recuperaPublicadorPort.recuperaPublicadorUuid(str.getUuid())).collect(Collectors.toList());
		evento.setPontos(pontos.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
		evento.setPublicadores(publicadores.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
		return atualizaEventoPort.atualizaEvento(evento);
	}

	@Override
	public void adicionaPublicadoresEvento(String eventoUuid, List<String> uuidsPublicadoress)
			throws TperioBusinessException {
		Evento evento = recuperaEventoPort.recuperaEventoPorUuid(eventoUuid).orElseThrow(() -> new TperioBusinessException(Arrays.asList(MessageKeys.EVENTONAOENCONTRADO)));
		List<Optional<Publicador>> pontos = uuidsPublicadoress.stream().map(str -> recuperaPublicadorPort.recuperaPublicadorUuid(str)).collect(Collectors.toList());
		evento.setPublicadores(pontos.stream().filter(Optional::isPresent).map(Optional::get).collect(Collectors.toList()));
		atualizaEventoPort.atualizaEvento(evento);
	}

}
