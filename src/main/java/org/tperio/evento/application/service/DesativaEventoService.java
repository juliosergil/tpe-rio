package org.tperio.evento.application.service;

import org.tperio.evento.application.port.in.DesativaEventoUseCase;
import org.tperio.evento.application.port.out.DesativaEventoPort;
import org.tperio.exception.TperioBusinessException;

public class DesativaEventoService implements DesativaEventoUseCase {

    private final DesativaEventoPort desativaEventoPort;

    public DesativaEventoService(DesativaEventoPort desativaEventoPort) {
        this.desativaEventoPort = desativaEventoPort;
    }

    @Override
    public void desativaEvento(String uuid) throws TperioBusinessException {
        desativaEventoPort.desativaEvento(uuid);
    }
}
