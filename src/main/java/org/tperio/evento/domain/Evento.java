package org.tperio.evento.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.tperio.config.MessageKeys;
import org.tperio.config.Messages;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.domain.Publicador;

public class Evento {
	
	String uuid;
	String nome;
	Date dataInicio;
	Date dataFim;
	String caminhobanner;
	List<Ponto> pontos = new ArrayList<>();
	List<Publicador> publicadores = new ArrayList<>();
	Date datacriacao;
	Date datadesativado;
	byte[] banner;
	
	public void validaEvento() throws TperioBusinessException {
		List<String> sb = new ArrayList<>();
		if(Objects.isNull(nome)) {
			adicionaMensagem(sb, MessageKeys.PREENCHANOME);
		}
		if(Objects.isNull(dataInicio)) {
			adicionaMensagem(sb, MessageKeys.PREENCHADATAINICIAL);
		}
		if(Objects.isNull(dataFim)) {
			adicionaMensagem(sb, MessageKeys.PREENCHADATAFINAL);
		}
		if(!sb.isEmpty()) {
			throw new TperioBusinessException(sb);
		}
	}

	public boolean isDesativado() {
		return !Objects.isNull(datadesativado) || dataFim.before(new Date());
	}
	
	private void adicionaMensagem(List<String> sb, String key) {
		sb.add(Messages.getInstance().getMessage(key));
	}
	
	public byte[] getBanner() {
		return banner;
	}

	public void setBanner(byte[] banner) {
		this.banner = banner;
	}

	public Date getDatacriacao() {
		return datacriacao;
	}

	public void setDatacriacao(Date datacriacao) {
		this.datacriacao = datacriacao;
	}

	public List<Publicador> getPublicadores() {
		return publicadores;
	}

	public void setPublicadores(List<Publicador> publicadores) {
		this.publicadores = publicadores;
	}

	public Date getDatadesativado() {
		return datadesativado;
	}

	public void setDatadesativado(Date datadesativado) {
		this.datadesativado = datadesativado;
	}

	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}
	public Date getDataFim() {
		return dataFim;
	}
	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}
	public String getCaminhobanner() {
		return caminhobanner;
	}
	public void setCaminhobanner(String caminhobanner) {
		this.caminhobanner = caminhobanner;
	}
	public List<Ponto> getPontos() {
		return pontos;
	}
	public void setPontos(List<Ponto> pontos) {
		this.pontos = pontos;
	}
	
}
