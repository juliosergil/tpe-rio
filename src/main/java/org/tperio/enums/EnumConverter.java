package org.tperio.enums;


import java.util.Optional;

import org.hibernate.service.spi.ServiceException;
import org.springframework.core.convert.converter.Converter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public abstract class EnumConverter<E extends Enum<E>> implements Converter<String, E> {

    private final Class<E> enumType;

    @Override
    public E convert(final String source) {
        try {
            return Optional.of(transform(source)).orElseThrow(IllegalArgumentException::new);
        } catch (IllegalArgumentException | ServiceException err) {
            log.error("Não foi possivel encontrar o valor para o ENUM. Type: [{}] Value: [{}]", source, enumType.getCanonicalName(), err);
            throw new RuntimeException("Erro");
        }
    }

    protected E transform(String source) {
        return E.valueOf(enumType, source.toUpperCase());
    }

}

