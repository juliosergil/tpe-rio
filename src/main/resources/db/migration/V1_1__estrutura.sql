CREATE TABLE IF NOT EXISTS circuito (
  idcircuito INT NOT NULL AUTO_INCREMENT,
  uuid VARCHAR(45) NOT NULL,
  nome VARCHAR(255) NOT NULL,
  datacriacao DATETIME NOT NULL,
  datadesativado DATETIME NULL,
  CONSTRAINT circuito_pk PRIMARY KEY (idcircuito)
);

INSERT INTO circuito(`idcircuito`, `nome`, `uuid`, `datacriacao`) 
VALUES (1, 'RJ22', 'd3a84da2-7eb8-11ec-90d6-0242ac120008', NOW());

CREATE TABLE IF NOT EXISTS congregacao (
  idcongregacao INT NOT NULL AUTO_INCREMENT,
  idcircuito INT(10) NOT NULL,
  uuid VARCHAR(45) NOT NULL,
  nome VARCHAR(255) NOT NULL,
  regiao VARCHAR(255) NOT NULL,
  numero VARCHAR(255) NULL,
  datacriacao DATETIME NOT NULL,
  datadesativado DATETIME NULL,
  CONSTRAINT congregacao_pk PRIMARY KEY (idcongregacao),
  KEY fk_circuito_congregacao_idx (idcircuito),
  CONSTRAINT fk_circuito_congregacao FOREIGN KEY (idcircuito) REFERENCES circuito (idcircuito)
);

INSERT INTO congregacao(`idcongregacao`, `idcircuito`, `regiao`, `nome`, `uuid`, `numero`, `datacriacao`) 
VALUES (1, 1, 'ZONASUL', 'Humaitá', 'd3a84da2-7eb8-11ec-90d6-0242ac120004', '123456', NOW());

CREATE TABLE IF NOT EXISTS perfil (
  idperfil INT NOT NULL AUTO_INCREMENT,
  uuid VARCHAR(45) NOT NULL,
  nome VARCHAR(255) NOT NULL,
  datacriacao DATETIME NOT NULL,
  datadesativado DATETIME NULL,
  CONSTRAINT perfil_pk PRIMARY KEY (idperfil)
);

INSERT INTO perfil(idperfil, `nome`, `uuid`, `datacriacao`) VALUES (1, 'ADMIN', 'd3a84da2-7eb8-11ec-90d6-0242ac120005', NOW());
INSERT INTO perfil(idperfil, `nome`, `uuid`, `datacriacao`) VALUES (2, 'ADMINPONTO', 'd3a84da2-7eb8-11ec-90d6-0242ac120006', NOW());
INSERT INTO perfil(idperfil, `nome`, `uuid`, `datacriacao`) VALUES (3, 'PUBLICADOR', 'd3a84da2-7eb8-11ec-90d6-0242ac120007', NOW());
INSERT INTO perfil(idperfil, `nome`, `uuid`, `datacriacao`) VALUES (4, 'SUPORTE', 'd3a84da2-7eb8-11ec-90d6-0242ac120008', NOW());

CREATE TABLE IF NOT EXISTS publicador (
  idpublicador INT NOT NULL AUTO_INCREMENT,
  uuid VARCHAR(45) NOT NULL,
  email VARCHAR(255) NOT NULL,
  nome VARCHAR(255) NOT NULL,
  senha VARCHAR(255) NOT NULL,
  genero CHAR(1) NOT NULL,
  telefone VARCHAR(45) NOT NULL,
  idcongregacao INT(10) NULL,
  idperfil INT(10) NOT NULL,
  datacriacao DATETIME NOT NULL,
  datadesativado DATETIME NULL,
  publicador_idpublicador INT NULL,
  CONSTRAINT publicador_pk PRIMARY KEY (idpublicador),
  KEY fk_publicador_publicador1_idx (publicador_idpublicador),
  KEY fk_publicador_perfil_idx (idperfil),
  CONSTRAINT fk_publicador_publicador1 FOREIGN KEY (publicador_idpublicador) REFERENCES publicador (idpublicador),
  CONSTRAINT fk_publicador_perfil FOREIGN KEY (idperfil) REFERENCES perfil (idperfil)
);

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `genero`,  `idcongregacao`, `idperfil`, `datacriacao`) 
VALUES (1, 'marcao', 'd3a84da2-7eb8-11ec-90d6-0242ac120003', 'gabrieldeantonio@protonmail.com', 
	'$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 'M', 1, 1, NOW()
);


CREATE TABLE IF NOT EXISTS ponto (
  idponto int(11) NOT NULL AUTO_INCREMENT,
  nome varchar(150) NOT NULL,
  uuid VARCHAR(45) NOT NULL,
  endereco varchar(150) NOT NULL,
  regiao varchar(150) NOT NULL,
  latitude varchar(45) DEFAULT NULL,
  longitude varchar(45) DEFAULT NULL,
  idresponsavel int(11) NOT NULL,
  idauxiliar int(11) NOT NULL,
  enderecodeposito varchar(255) NOT NULL,
  latitudedeposito varchar(45) DEFAULT NULL,
  longitudedeposito varchar(45) DEFAULT NULL,
  turistico tinyint(1) DEFAULT '0',
  regular tinyint(1) NOT NULL DEFAULT '0',
  idpublicador int(11) NOT NULL,
  datacriacao datetime NOT NULL,
  datadesativado datetime DEFAULT NULL,
  ativo tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (idponto),
  KEY fk_ponto_publicador1_idx (idresponsavel),
  KEY fk_ponto_publicador2_idx (idauxiliar),
  KEY fk_ponto_publicador3_idx (idpublicador),
  CONSTRAINT fk_ponto_publicador1 FOREIGN KEY (idresponsavel) REFERENCES publicador (idpublicador),
  CONSTRAINT fk_ponto_publicador2 FOREIGN KEY (idauxiliar) REFERENCES publicador (idpublicador),
  CONSTRAINT fk_ponto_publicador3 FOREIGN KEY (idpublicador) REFERENCES publicador (idpublicador)
);

CREATE TABLE IF NOT EXISTS agendamento (
  idagendamento int(11) NOT NULL AUTO_INCREMENT,
  idponto int(11) NOT NULL,
  uuid VARCHAR(45) NOT NULL,
  horainicio varchar(150) NOT NULL,
  horafim varchar(45) DEFAULT NULL,
  data date DEFAULT NULL,
  idprincipal int(11) NOT NULL,
  idauxiliar int(11) NULL,
  idpublicador int(11) NOT NULL,
  datacriacao datetime NOT NULL,
  datadesativado datetime DEFAULT NULL,
  PRIMARY KEY (idagendamento),
  KEY fk_agendamento_publicador1_idx (idprincipal),
  KEY fk_agendamento_publicador2_idx (idauxiliar),
  KEY fk_agendamento_publicador3_idx (idpublicador),
  KEY fk_agendamento_ponto1_idx (idponto),
  CONSTRAINT fk_agendamento_publicador1 FOREIGN KEY (idprincipal) REFERENCES publicador (idpublicador),
  CONSTRAINT fk_agendamento_publicador2 FOREIGN KEY (idauxiliar) REFERENCES publicador (idpublicador),
  CONSTRAINT fk_agendamento_publicador3 FOREIGN KEY (idpublicador) REFERENCES publicador (idpublicador),
  CONSTRAINT fk_agendamento_ponto1 FOREIGN KEY (idponto) REFERENCES ponto (idponto)
);

CREATE TABLE IF NOT EXISTS evento (
  idevento int(11) NOT NULL AUTO_INCREMENT,
  uuid VARCHAR(45) NOT NULL,
  nome varchar(150) NOT NULL,
  caminhobanner varchar(250) NULL,
  data_inicio date NOT NULL,
  data_fim date NOT NULL,
  datacriacao datetime NOT NULL,
  datadesativado datetime DEFAULT NULL,
  PRIMARY KEY (idevento)
);

CREATE TABLE IF NOT EXISTS pontosevento (
  idpontosevento int(11) NOT NULL AUTO_INCREMENT,
  idevento int(11) NOT NULL,
  idponto int(11) NOT NULL,
  PRIMARY KEY (idpontosevento),
  KEY fk_pontosevento_ponto1_idx (idponto),
  KEY fk_pontosevento_evento1_idx (idevento),
  CONSTRAINT fk_pontosevento_ponto1 FOREIGN KEY (idponto) REFERENCES ponto (idponto),
  CONSTRAINT fk_pontosevento_evento1 FOREIGN KEY (idevento) REFERENCES evento (idevento)
);

CREATE TABLE IF NOT EXISTS publicadoresevento (
  idpublicadoresevento int(11) NOT NULL AUTO_INCREMENT,
  idevento int(11) NOT NULL,
  idpublicador int(11) NOT NULL,
  PRIMARY KEY (idpublicadoresevento),
  KEY fk_publicadoresevento_ponto1_idx (idpublicador),
  KEY fk_publicadoresevento_evento1_idx (idevento),
  CONSTRAINT fk_publicadoresevento_publicador1 FOREIGN KEY (idpublicador) REFERENCES publicador (idpublicador),
  CONSTRAINT fk_publicadoresevento_evento1 FOREIGN KEY (idevento) REFERENCES evento (idevento)
);

CREATE TABLE IF NOT EXISTS publicadorcontato (
  idpublicadorcontato int(11) NOT NULL AUTO_INCREMENT,
  idpublicador int(11) NOT NULL,
  telegram varchar(100) NULL,
  PRIMARY KEY (idpublicadorcontato),
  KEY fk_publicadorcontato_publicador1_idx (idpublicador),
  CONSTRAINT fk_publicadorcontato_publicador1 FOREIGN KEY (idpublicador) REFERENCES publicador (idpublicador)
);
