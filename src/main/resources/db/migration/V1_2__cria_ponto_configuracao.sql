CREATE TABLE IF NOT EXISTS pontoconfiguracao (
  idpontoconfiguracao int(11) NOT NULL AUTO_INCREMENT,
  horainicio varchar(15) NULL,
  horafim varchar(15) NULL,
  tipo varchar(25) NOT NULL,
  horariosdiarios TEXT NULL,
  idponto int(11) NOT NULL,
  PRIMARY KEY (idpontoconfiguracao),
  KEY fk_pontoconfiguracao_ponto1_idx (idponto),
  CONSTRAINT fk_pontosconfiguracao_ponto1 FOREIGN KEY (idponto) REFERENCES ponto (idponto)
);
