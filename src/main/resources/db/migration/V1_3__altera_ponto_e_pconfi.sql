ALTER TABLE `ponto` DROP `regular`;
ALTER TABLE `ponto` ADD COLUMN evento tinyint(1) NULL DEFAULT '0';
ALTER TABLE `ponto` ADD COLUMN horainicio varchar(5) NULL;
ALTER TABLE `ponto` ADD COLUMN horafim varchar(5) NULL;
ALTER TABLE `pontoconfiguracao` ADD COLUMN qthorasturno varchar(5) NULL DEFAULT '1';
ALTER TABLE `pontoconfiguracao` DROP `horainicio`;
ALTER TABLE `pontoconfiguracao` DROP `horafim`;