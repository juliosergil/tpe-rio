CREATE TABLE IF NOT EXISTS regiao (
  idregiao INT NOT NULL AUTO_INCREMENT,
  uuid VARCHAR(45) NOT NULL,
  nome VARCHAR(255) NOT NULL,
  datacriacao DATETIME NOT NULL,
  datadesativado DATETIME NULL,
  CONSTRAINT regiao_pk PRIMARY KEY (idregiao)
);

ALTER TABLE `congregacao` ADD COLUMN idregiao int NOT NULL DEFAULT '0';
ALTER TABLE `congregacao` DROP `regiao`;

ALTER TABLE `ponto` ADD COLUMN idregiao int NOT NULL DEFAULT '0';
ALTER TABLE `ponto` DROP `regiao`;