package org.tperio.evento;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.testutils.BaseControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;

class PontoEventoControllerTest extends BaseControllerTest {
	
	@Test
	@Sql(scripts = {"classpath:db/pontoevento/testaAdicionaPontoEventoComSucesso.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/pontoevento/apagaAdicionaPontoEventoComSucesso.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaAdicionaPontoEventoComSucesso() throws Exception {
		String json = new ObjectMapper().writeValueAsString(Arrays.asList("testeaddpontoeventouuid"));
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/evento/testepontoeventouuid/ponto")
		        .content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
				/*
				 * .andExpect(jsonPath("$").exists()) .andExpect(jsonPath("$.nome").exists())
				 * .andExpect(jsonPath("$.nome", is("Evento teste")))
				 * .andExpect(jsonPath("$.dataInicio").exists())
				 * .andExpect(jsonPath("$.dataFim").exists())
				 * .andExpect(jsonPath("$.pontos").exists())
				 * .andExpect(jsonPath("$.pontos").isArray())
				 */
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
}
