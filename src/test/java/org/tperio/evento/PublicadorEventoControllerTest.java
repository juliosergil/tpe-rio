package org.tperio.evento;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.testutils.BaseControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;

class PublicadorEventoControllerTest extends BaseControllerTest {

	@Test
	@Sql(scripts = {"classpath:db/publicadorevento/testaAdicionarPublicadoresAoEvento.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/publicadorevento/apagaAdicionarPublicadoresAoEvento.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaAdicionarPublicadoresAoEvento() throws Exception {
		String json = new ObjectMapper().writeValueAsString(Arrays.asList("testeaddpontoeventouuid"));
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/evento/testepubeventouuid/publicador")
		        .content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
}
