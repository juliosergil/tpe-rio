package org.tperio.evento;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.tperio.evento.adapter.in.web.EventoDto;
import org.tperio.evento.adapter.in.web.NovoEventoDto;
import org.tperio.testutils.BaseControllerTest;

class EventoControllerTest extends BaseControllerTest {

	@Test
	@Sql(scripts = {"classpath:db/evento/testaCriaEventoComSucesso.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/evento/apagaCriaEventoComSucesso.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaCriaEventoComBannerEPontosComSucesso() throws Exception {
		NovoEventoDto dto = NovoEventoDto.builder()
				.dataInicio(LocalDate.now())
				.dataFim(LocalDate.now())
				.pontosuuid(Arrays.asList("testepontoativoeventouuid"))
				.nome("Evento teste 2")
				.build();
		Path resourceDirectory = Paths.get("src","test","resources");
		File file = new File(resourceDirectory + "/tartaruga.jpeg");
		FileInputStream fis = new FileInputStream(file);
		MockMultipartFile mockMultipartFile = new MockMultipartFile("banner", file.getName(),
		          "image/jpeg", fis);
		String json = mapper.writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/evento")
		        .file(mockMultipartFile).param("evento",json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Evento teste 2")))
                .andExpect(jsonPath("$.dataInicio").exists())
                .andExpect(jsonPath("$.dataFim").exists())
                .andExpect(jsonPath("$.pontos").exists())
                .andExpect(jsonPath("$.pontos").isArray())
                .andExpect(jsonPath("$.pontos[0].uuid").exists())
                .andExpect(jsonPath("$.pontos[0].uuid", is("testepontoativoeventouuid")))
                .andReturn();
		String content = result.getResponse().getContentAsString();
		Assertions.assertNotNull(content);
		
		NovoEventoDto novoEventoDto = mapper.readValue(content, NovoEventoDto.class);
		File file2 = new File(resourceDirectory + "/banners/" + novoEventoDto.getCaminhobanner());
		assertThat(file2.delete()).isTrue();
	}
	
	@Test
	@Sql(scripts = {"classpath:db/evento/limpaTodosEventos.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaCriaEventoSemBannerESemPontoComSucesso() throws Exception {
		NovoEventoDto dto = NovoEventoDto.builder()
				.dataInicio(LocalDate.now())
				.dataFim(LocalDate.now())
				.nome("Evento teste")
				.build();
		String json = mapper.writeValueAsString(dto);
		System.out.println(json);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.multipart("/api/evento")
		        .param("evento",json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Evento teste")))
                .andExpect(jsonPath("$.dataInicio").exists())
                .andExpect(jsonPath("$.dataFim").exists())
                .andExpect(jsonPath("$.pontos").exists())
                .andExpect(jsonPath("$.pontos").isArray())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		System.out.println(content);
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/evento/criaevento.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/evento/deletaevento.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD
)
	void testaAtualizaEventoComSucesso() throws Exception {
		EventoDto dto = EventoDto.builder()
				.uuid("testepontoeventocriauuid")
				.dataInicio(LocalDate.now())
				.dataFim(LocalDate.now())
				.pontosuuid(Arrays.asList("testepontoativoeventocriauuid"))
				.nome("Evento teste 2")
				.build();
		String json = mapper.writeValueAsString(dto);
		MockMultipartHttpServletRequestBuilder builder =
	            MockMvcRequestBuilders.multipart("/api/evento");
	    builder.with(new RequestPostProcessor() {
			
			@Override
			public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
				request.setMethod(HttpMethod.PUT.name());
	            return request;
			}
		});
		MvcResult result = mockMvc.perform(builder
				.param("evento",json))
                .andExpect(status().isOk())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Evento teste 2")))
                .andExpect(jsonPath("$.dataInicio").exists())
                .andExpect(jsonPath("$.dataFim").exists())
                .andExpect(jsonPath("$.pontos").exists())
                .andExpect(jsonPath("$.pontos").isArray())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/evento/testaRecuperarEventosAtivosComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/evento/apagaRecuperarEventosAtivosComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaRecuperarEventosAtivosComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/evento/ativos")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$[0].nome").exists())
                .andExpect(jsonPath("$[0].nome", is("Teste evento")))
                .andExpect(jsonPath("$[0].dataInicio").exists())
                .andExpect(jsonPath("$[0].dataFim").exists())
                .andExpect(jsonPath("$[0].pontos").exists())
                .andExpect(jsonPath("$[0].pontos").isArray())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
}
