package org.tperio.publicador;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.testutils.BaseControllerTest;

class PerfilControllerTest extends BaseControllerTest {
	
	@Test
	void testaRecuperarPerfisComSucesso() throws Exception {
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/perfil")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].nome").exists())
                .andExpect(jsonPath("$[0].nome", is("ADMIN")))
                .andExpect(status().isOk()).andReturn();
		String content = result.getResponse().getContentAsString();
		Assertions.assertNotNull(content);
	}

}
