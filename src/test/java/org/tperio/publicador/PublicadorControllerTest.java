package org.tperio.publicador;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.DTO.CredenciaisDTO;
import org.tperio.publicador.adapter.in.web.PublicadorDto;
import org.tperio.publicador.adapter.in.web.PublicadorPatchDto;
import org.tperio.testutils.BaseControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;

class PublicadorControllerTest extends BaseControllerTest {

	@Autowired
    @Lazy
    private PasswordEncoder encoder;
	
	@Test
	@Sql(scripts = {"classpath:db/publicador/gravaPublicadorEmail.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/publicador/apagaPublicador.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD
	)
	void testaRecuperarPublicadorPorEmailComSucesso() throws Exception {
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/publicador/find?email=gabrieldeantonio@protonPESQUISA2.me")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/publicador/gravaPublicador.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/publicador/apagaPublicador.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD
	)
	void testaRecuperarPublicadorPorNomeComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/publicador/nome/te")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/publicador/gravaPublicador.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/publicador/apagaPublicador.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD
	)
	void testaRecuperarPublicadoresAdminPonto() throws Exception {
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/publicador/perfil/ADMIN")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Disabled
	@Test
	void testaEsqueciMinhaSenhaComSucesso() throws Exception {
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/publicador/esquecisenha")
				.content("gabrieldeantonio@protonmail.com")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	void criaSenha() {
		String senha = encoder.encode("123456");
		assertNotNull(senha);
		System.out.println(senha);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/publicador/gravaPublicador.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/publicador/apagaPublicador.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD
	)
	void testaCriaPublicadorComSucesso() throws Exception {
		PublicadorDto dto = PublicadorDto.builder()
				.nome("Botafogo")
				.email("teste@gmail.com")
				.senha("12345678")
				.genero("M")
				.uuidperfil("d3a84da2-7eb8-11ec-90d6-0242ac120005")
				.telefone("(21) 98165-5159")
				.uuidcongregacao("teste-grava-publicador")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/publicador")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Botafogo")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.congregacao").exists())
                .andExpect(jsonPath("$.congregacao.nome").exists())
                .andExpect(jsonPath("$.congregacao.numero").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/publicador/testaAtualizaPublicadorComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaAtualizaPublicadorComSucesso() throws Exception {
		PublicadorDto dto = PublicadorDto.builder()
				.nome("Botafogo")
				.email("gabrieldeantonio@protonUPDATE.me")
				.senha("12345678")
				.genero("M")
				.uuid("teste-atualiza-pub-com-sucesso")
				.uuidperfil("d3a84da2-7eb8-11ec-90d6-0242ac120005")
				.telefone("(21) 98165-5159")
				.uuidcongregacao("teste-atualiza-pub-sucesso")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/api/publicador/teste-atualiza-pub-com-sucesso")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Botafogo")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.congregacao").exists())
                .andExpect(jsonPath("$.congregacao.nome").exists())
                .andExpect(jsonPath("$.congregacao.numero").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/publicador/testaAtualizaPublicadorParcialComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaAtualizaPublicadorParcialComSucesso() throws Exception {
		PublicadorPatchDto dto = PublicadorPatchDto.builder()
				.senha("12345678")
				.uuid("teste-atualiza-pub-patch-com-sucesso")
				.telefone("(21) 98165-5159")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.patch("/api/publicador/teste-atualiza-pub-patch-com-sucesso")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("marcao")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.congregacao").exists())
                .andExpect(jsonPath("$.congregacao.nome").exists())
                .andExpect(jsonPath("$.congregacao.numero").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/publicador/testaApagaPublicadorComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaApagarPublicadorComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.delete("/api/publicador/teste-deleta-pub-com-sucesso")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/publicador/testaRecuperaPublicadorComEventoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaRecuperaPublicadorComEventoComSucesso() throws Exception {
		CredenciaisDTO credenciais = CredenciaisDTO.builder()
				.login("gabrieldeantonio@protonUPDATEEVE.me")
				.senha("123456")
				.build();
		String send = new ObjectMapper().writeValueAsString(credenciais);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/publicador/auth")
				.content(send)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.login").exists())
                .andExpect(jsonPath("$.login", is("gabrieldeantonio@protonupdateeve.me")))
                .andExpect(jsonPath("$.token").exists())
                .andExpect(jsonPath("$.eventos").exists())
                .andExpect(jsonPath("$.eventos").isArray())
                .andExpect(jsonPath("$.eventos").isNotEmpty())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
		System.out.println(content);
	}
	
}
