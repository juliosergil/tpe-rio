package org.tperio.ponto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.domain.Ponto;

class PontoTest {
	
	@Test
	void testaValidacao() {
		Ponto p = new Ponto("");
		try {
			p.validaPonto();
		} catch (TperioBusinessException e) {
			assertThat(e).isNotNull();
			//assertThat(e.getMensagens()).anyMatch(Messages.getInstance().getMessage(MessageKeys.PREENCHANOME));
		}
	}

}
