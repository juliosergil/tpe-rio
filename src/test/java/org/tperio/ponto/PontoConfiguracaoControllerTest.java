package org.tperio.ponto;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.ponto.adapter.in.web.PontoConfiguracaoDto;
import org.tperio.ponto.adapter.in.web.PontoDto;
import org.tperio.testutils.BaseControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class PontoConfiguracaoControllerTest extends BaseControllerTest {
	
	private Map<String, String> horarios = Map.of("segunda", "10-19", "terca", "9-18");

	@Test
	@Sql(scripts = {"classpath:db/ponto/gravaPublicadorResponsavelAuxiliar.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/ponto/apagaPublicadorResponsavelAuxiliar.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD
	)
	void testaCriaPontoComConfiguracaoComSucesso() throws Exception {
		 
		PontoConfiguracaoDto confDto = PontoConfiguracaoDto
				.builder()
				.horasporturno(3)
				.tipo("RESIDENCIAL")
				.horariosdiarios(mapper.writeValueAsString(horarios))
				.build();
		PontoDto dto = PontoDto.builder()
				.nome("Botafogo")
				.endereco("Teste ponto")
				.latitude("123")
				.longitude("321")
				.uuidregiao("teste-uuid-atualiza-regiao-novo-ponto")
				.pontoConfiguracao(confDto)
				.enderecodeposito("Teste deposito")
				.latitudedeposito("456")
				.longitudedeposito("654")
				.publicadoruuid("teste-publicador-responsavel")
				.responsaveluuid("teste-publicador-responsavel2")
				.auxiliaruuid("teste-publicador-responsavel")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		log.debug("*****************----------------- DEBUG---------------------*********************");
		log.debug(json);
		log.debug("*****************----------------- DEBUG---------------------*********************");
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/ponto")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Botafogo")))
                .andExpect(jsonPath("$.regiao").exists())
                .andExpect(jsonPath("$.regiao.uuid", is("teste-uuid-atualiza-regiao-novo-ponto")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.pontoConfiguracao").exists())
                .andExpect(jsonPath("$.pontoConfiguracao.horasporturno", is(3)))
                .andExpect(jsonPath("$.pontoConfiguracao.tipo", is("RESIDENCIAL")))
                .andExpect(jsonPath("$.responsavel.uuid").exists())
                .andExpect(jsonPath("$.auxiliar.uuid").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		log.debug(content);
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/ponto/testaAtualizaPontoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/ponto/apagaAtualizaPontoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD
	)
	void testaAtualizaPontoComSucesso() throws Exception {
		PontoConfiguracaoDto confDto = PontoConfiguracaoDto
				.builder()
				.horasporturno(3)
				.tipo("RESIDENCIAL")
				.horariosdiarios(mapper.writeValueAsString(horarios))
				.build();
		PontoDto dto = PontoDto.builder()
				.nome("Teste update ponto")
				.uuid("d3a84da2-7eb8-11ec-90d6-0242ac120066")
				.publicadoruuid("pubatualizaponto1")
				.endereco("Teste ponto")
				.latitude("123")
				.longitude("321")
				.uuidregiao("teste-uuid-atualiza-regiao-atualiza-ponto")
				.pontoConfiguracao(confDto)
				.enderecodeposito("Teste deposito")
				.latitudedeposito("456")
				.longitudedeposito("654")
				.responsaveluuid("pubatualizaponto1")
				.auxiliaruuid("pubatualizaponto2")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/api/ponto/"+ "d3a84da2-7eb8-11ec-90d6-0242ac120066")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Teste update ponto")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.pontoConfiguracao").exists())
                .andExpect(jsonPath("$.pontoConfiguracao.horasporturno", is(3)))
                .andExpect(jsonPath("$.pontoConfiguracao.tipo", is("RESIDENCIAL")))
                .andExpect(jsonPath("$.responsavel.uuid").exists())
                .andExpect(jsonPath("$.auxiliar.uuid").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
}
