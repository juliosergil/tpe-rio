package org.tperio.ponto;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.ponto.adapter.in.web.PontoDto;
import org.tperio.testutils.BaseControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;

class PontoControllerTest extends BaseControllerTest {

	@Test
	@Sql(scripts = {"classpath:db/ponto/testaRecuperaPontosAtivos.sql"}, 
			config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
			executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/ponto/apagaRecuperaPontosAtivos.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaRecuperaPontosAtivosComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/ponto")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].nome").exists())
                .andExpect(jsonPath("$[0].nome", is("Teste")))
                .andExpect(jsonPath("$[0].regiao").exists())
                .andExpect(jsonPath("$[0].regiao.uuid", is("teste-uuid-atualiza-regiao")))
                .andExpect(jsonPath("$[0].uuid").exists())
                .andExpect(jsonPath("$[0].uuid").isNotEmpty())
                .andExpect(jsonPath("$[0].responsavel.uuid").exists())
                .andExpect(jsonPath("$[0].auxiliar.uuid").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/ponto/testaRecuperaPontosPorRegiao.sql"}, 
			config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
			executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	void testaRecuperaPontosPorRegiaoComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/ponto/regiao/teste-recupera-pontos-regiao")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].nome").exists())
                .andExpect(jsonPath("$[0].nome", is("Teste")))
                .andExpect(jsonPath("$[0].regiao").exists())
                .andExpect(jsonPath("$[0].regiao.uuid", is("teste-recupera-pontos-regiao")))
                .andExpect(jsonPath("$[0].uuid").exists())
                .andExpect(jsonPath("$[0].uuid").isNotEmpty())
                .andExpect(jsonPath("$[0].responsavel.uuid").exists())
                .andExpect(jsonPath("$[0].auxiliar.uuid").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/ponto/gravaPublicadorResponsavelAuxiliar.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/ponto/apagaPublicadorResponsavelAuxiliar.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD
)
	void testaCriaPontoComSucesso() throws Exception {
		PontoDto dto = PontoDto.builder()
				.nome("Botafogo")
				.endereco("Teste ponto")
				.latitude("123")
				.longitude("321")
				.uuidregiao("teste-uuid-atualiza-regiao-novo-ponto")
				.enderecodeposito("Teste deposito")
				.latitudedeposito("456")
				.longitudedeposito("654")
				.publicadoruuid("teste-publicador-responsavel")
				.responsaveluuid("teste-publicador-responsavel")
				.auxiliaruuid("teste-publicador-responsavel2")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/ponto")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Botafogo")))
                .andExpect(jsonPath("$.regiao").exists())
                .andExpect(jsonPath("$.regiao.uuid", is("teste-uuid-atualiza-regiao-novo-ponto")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.responsavel.uuid").exists())
                .andExpect(jsonPath("$.auxiliar.uuid").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/ponto/testaRecuperaPontoPorUuid.sql"}, 
			config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
			executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	void testaRecuperaPontoPorUuidComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/ponto/" + "d3a84da2-7eb8-11ec-90d6-0242ac120008")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Teste")))
                .andExpect(jsonPath("$.regiao").exists())
                .andExpect(jsonPath("$.regiao.uuid", is("teste-uuid-atualiza-regiao")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.responsavel.uuid").exists())
                .andExpect(jsonPath("$.auxiliar.uuid").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		Assertions.assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/ponto/testaRecuperaPontoPorUuidComEventoAtivo.sql"}, 
			config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
			executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	void testaRecuperaPontoPorUuidComEventoAtivoComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/ponto/testpontouuidcomevento")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Teste")))
                .andExpect(jsonPath("$.regiao").exists())
                .andExpect(jsonPath("$.eventoAtivo").exists())
                .andExpect(jsonPath("$.regiao.uuid", is("teste-uuid-atualiza-regiao")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.responsavel.uuid").exists())
                .andExpect(jsonPath("$.auxiliar.uuid").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		Assertions.assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/ponto/testaAtualizaPontoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	@Sql(scripts = {"classpath:db/ponto/apagaAtualizaPontoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.AFTER_TEST_METHOD
	)
	void testaAtualizaPontoComSucesso() throws Exception {
		PontoDto dto = PontoDto.builder()
				.nome("Teste update ponto")
				.uuid("d3a84da2-7eb8-11ec-90d6-0242ac120066")
				.publicadoruuid("pubatualizaponto1")
				.endereco("Teste ponto")
				.latitude("123")
				.longitude("321")
				.uuidregiao("teste-uuid-atualiza-regiao-atualiza-ponto")
				.enderecodeposito("Teste deposito")
				.latitudedeposito("456")
				.longitudedeposito("654")
				.responsaveluuid("pubatualizaponto1")
				.auxiliaruuid("pubatualizaponto2")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/api/ponto/"+ "d3a84da2-7eb8-11ec-90d6-0242ac120066")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Teste update ponto")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.responsavel.uuid").exists())
                .andExpect(jsonPath("$.auxiliar.uuid").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/ponto/testaApagaPontoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaApagaPontoComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.delete("/api/ponto/teste-apaga-ponto-sucesso")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
}
