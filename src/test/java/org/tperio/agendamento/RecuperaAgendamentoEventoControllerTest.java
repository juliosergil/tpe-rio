package org.tperio.agendamento;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.testutils.BaseControllerTest;

class RecuperaAgendamentoEventoControllerTest extends BaseControllerTest {

	@Test
	@Sql(scripts = {"classpath:db/agendamento/testaRecuperaAgendamentosComEventoPorDataAguardandoParComSucesso.sql"}, 
			config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
			executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
		)
	@Sql(scripts = {"classpath:db/agendamento/limpaRecuperaAgendamentosComEventoPorDataAguardandoParComSucesso.sql"}, 
			config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
			executionPhase = ExecutionPhase.AFTER_TEST_METHOD
		)
	void testaRecuperaAgendamentosPorDataAguardandoParComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/agendamento?datainicio=20/6/2022&datafim=28/6/2022&aguardandopar=true")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].horainicio").exists())
                .andExpect(jsonPath("$[0].ponto.eventoAtivo").exists())
                .andExpect(jsonPath("$[0].horafim").exists())
                .andExpect(jsonPath("$[0].data").exists())
                .andExpect(jsonPath("$[0].principal").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		System.out.println(content);
		assertNotNull(content);
	}
	
}
