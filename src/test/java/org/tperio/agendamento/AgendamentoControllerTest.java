package org.tperio.agendamento;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.agendamento.adapter.in.web.AgendamentoDto;
import org.tperio.testutils.BaseControllerTest;


class AgendamentoControllerTest extends BaseControllerTest {

	@Test
	@Sql(scripts = {"classpath:db/agendamento/testaCriaAgendamentoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaCriaAgendamentoComSucesso() throws Exception {
		AgendamentoDto dto = AgendamentoDto.builder()
				.horainicio("11")
				.horafim("12")
				.data(LocalDate.now())
				.pontouuid("testepontoagendamentouuid")
				.publicadoruuid("testeagendamento1")
				.principaluuid("testeagendamento2")
				.auxiliaruuid("testeagendamento1")
				.build();
		String json = mapper.writeValueAsString(dto);
		System.out.println(json);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/agendamento")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.horainicio").exists())
                .andExpect(jsonPath("$.horainicio", is("11")))
                .andExpect(jsonPath("$.horafim").exists())
                .andExpect(jsonPath("$.horafim", is("12")))
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/agendamento/testaAtualizaAgendamentoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaAtualizaAgendamentoComSucesso() throws Exception {
		AgendamentoDto dto = AgendamentoDto.builder()
				.horainicio("11")
				.horafim("13")
				.data(LocalDate.now())
				.pontouuid("testerecpubagendamento-atualizaagendamento")
				.publicadoruuid("testerecpubagendamento1-atualizaagendamento")
				.principaluuid("testerecpubagendamento2-atualizaagendamento")
				.auxiliaruuid("testerecpubagendamento1-atualizaagendamento")
				.build();
		String json = mapper.writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/api/agendamento/teste-atualiza-com-sucesso-agendamento")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid", is("teste-atualiza-com-sucesso-agendamento")))
                .andExpect(jsonPath("$.horainicio").exists())
                .andExpect(jsonPath("$.horainicio", is("11")))
                .andExpect(jsonPath("$.horafim").exists())
                .andExpect(jsonPath("$.horafim", is("13")))
                .andReturn();
		String content = result.getResponse().getContentAsString();
		System.out.println(content);
		assertNotNull(content);
	}
	
	@Test
	void testaCriaAgendamentoValidacao() throws Exception {
		AgendamentoDto dto = AgendamentoDto.builder()
				.horainicio("")
				.horafim("321")
				.data(LocalDate.now())
				.pontouuid("")
				.publicadoruuid("d3a84da2-7eb8-11ec-90d6-0242ac120003")
				.principaluuid("d3a84da2-7eb8-11ec-90d6-0242ac120004")
				.auxiliaruuid("d3a84da2-7eb8-11ec-90d6-0242ac120005")
				.build();
		String json = mapper.writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/agendamento")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString(StandardCharsets.UTF_8);
		assertNotNull(content);
		assertTrue(content.contains("Hora de início possui o formato inválido"));
	}
	
	@Test
	@Sql(scripts = {"classpath:db/agendamento/testaApagarAgendamentoComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaApagarAgendamentoComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.delete("/api/agendamento/teste-apaga-com-sucesso-agendamento")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
}
