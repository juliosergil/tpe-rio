package org.tperio.agendamento;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.tperio.agendamento.adapter.out.persistence.AgendamentoAdapter;
import org.tperio.agendamento.adapter.out.persistence.AgendamentoScheduler;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.exception.TperioBusinessException;
import org.tperio.ponto.adapter.out.persistence.PontoAdapter;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.domain.Contato;
import org.tperio.publicador.domain.Publicador;
import org.tperio.service.EmailBroker;
import org.tperio.service.TelegramMessager;

class AgendamentoSchedulerTest {

	@Mock private AgendamentoAdapter agendamentoAdapter;
	@Mock private PontoAdapter pontoAdapter;
	@Mock private EmailBroker emailBroker;
	@Mock private TelegramMessager messager;
	
	@InjectMocks
	private AgendamentoScheduler scheduler;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	void testScheduler() throws TperioBusinessException {
		when(pontoAdapter.recuperaPontosAtivos()).thenReturn(Arrays.asList(criaPonto()));
		when(agendamentoAdapter.recuperaAgendamentosPorDataPonto(any(), any()))
			.thenReturn(Arrays.asList(criaAgendamento("7", "9"), criaAgendamento("9", "10")));
		doNothing().when(messager).enviaMensagem(anyString(), anyString());
		scheduler.programacaoDiaria();
		verify(pontoAdapter, Mockito.times(1)).recuperaPontosAtivos();
		verify(emailBroker, Mockito.times(2)).enviaEmailProgramacaoDia(any(), any(), any(), any());
		verify(messager, Mockito.times(2)).enviaMensagem(anyString(), anyString());
	}
	
	private Agendamento criaAgendamento(String horainicio, String horafim) {
		LocalDate today = LocalDate.now();
		LocalDate tomorrow = today.plusDays(1);
		Date dateF = Date.from(tomorrow.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Agendamento agendamento = new Agendamento();
		agendamento.setPonto(criaPonto());
		agendamento.setPrincipal(criaPublicadorPrincipal());
		agendamento.setAuxiliar(criaPublicadorAuxiliar());
		agendamento.setHorainicio(horainicio);
		agendamento.setHorafim(horafim);
		agendamento.setData(dateF);
		return agendamento;
	}
	
	private Ponto criaPonto() {
		Ponto ponto = new Ponto("123");
		ponto.setNome("Teste ponto");
		ponto.setResponsavel(criaPublicadorPrincipal());
		ponto.setAuxiliar(criaPublicadorAuxiliar());
		ponto.setEndereco("teste endereco");
		ponto.setEnderecodeposito("teste endereco deposito");
		return ponto;
	}
	
	private Publicador criaPublicadorPrincipal() {
		Publicador principal = new Publicador();
		Contato contato = new Contato();
		contato.setTelegram("1");
		principal.setUuid("123");
		principal.setContato(contato);
		principal.setGenero("M");
		principal.setEmail("teste@teste.com");
		principal.setNome("Marcão");
		principal.setTelefone("21981655159");
		return principal;
	}
	
	private Publicador criaPublicadorAuxiliar() {
		Publicador auxiliar = new Publicador();
		auxiliar.setGenero("F");
		auxiliar.setEmail("testea@teste.com");
		auxiliar.setNome("Gabriel");
		auxiliar.setTelefone("21981675009");
		return auxiliar;
	}
	
}
