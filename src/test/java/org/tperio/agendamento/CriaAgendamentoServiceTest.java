package org.tperio.agendamento;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.tperio.agendamento.application.port.out.CriaNovoAgendamentoPort;
import org.tperio.agendamento.application.port.out.RecuperaAgendamentoPort;
import org.tperio.agendamento.application.service.CriaAgendamentoService;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.message.port.out.MessageBrokerPort;
import org.tperio.ponto.application.port.out.RecuperaPontoPort;
import org.tperio.ponto.application.service.PontoService;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.application.port.out.RecuperaPublicadorPort;
import org.tperio.publicador.application.service.PublicadorService;
import org.tperio.publicador.domain.Publicador;

class CriaAgendamentoServiceTest {

	@Mock private RecuperaAgendamentoPort recuperaAgendamento;
	@Mock private CriaNovoAgendamentoPort criaNovoAgendamento;
	@Mock private RecuperaPontoPort recuperaPontoPort;
	@Mock private RecuperaPublicadorPort recuperaPublicadorPort;
	@Mock private MessageBrokerPort messageBrokerPort;
	@Mock private PontoService pontoService;
	@Mock private PublicadorService publicadorService;

	@InjectMocks
	private CriaAgendamentoService service;

	@BeforeEach
	void init() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testaCriaNovoAtendimentoComHorariosConflitantes() throws TperioBusinessException {
		assertNotNull(service);
		Agendamento agendamento = criaAgendamento("7", "10");
		Agendamento agendamentoConflito = criaAgendamento("8", "9");
		agendamentoConflito.setPonto(new Ponto("321"));
		when(pontoService.recuperaPontoPorUuid(anyString())).thenReturn(new Ponto("123"));
		when(publicadorService.recuperaPublicadorUuid(any()))
			.thenReturn(criaPublicadorPrincipal())
			.thenReturn(criaPublicadorAuxiliar());
		when(recuperaAgendamento.recuperaAgendamentosPorDataPonto(any(), any()))
			.thenReturn(null);
		when(recuperaAgendamento.recuperaAgendamentosPorDataPublicador(any(), any(), any()))
		.thenReturn(Arrays.asList(agendamentoConflito));
		when(criaNovoAgendamento.criaNovoAgendamento(any())).thenReturn(agendamento);
		doNothing().when(messageBrokerPort).mensagemAgendamentoFeitoComSucesso(any(), any());
		TperioBusinessException exception = assertThrows(TperioBusinessException.class, () -> {
			service.criaNovoAgendamento(agendamento);
	    });
	    assertTrue(exception.getMensagens().stream().anyMatch(msg -> msg.equals(MessageKeys.AGENDAMENTOJAEXISTE)));
	}

	private Ponto criaPonto() {
		Ponto ponto = new Ponto("123");
		ponto.setNome("Teste ponto");
		ponto.setResponsavel(criaPublicadorPrincipal());
		ponto.setAuxiliar(criaPublicadorAuxiliar());
		ponto.setEndereco("teste endereco");
		ponto.setEnderecodeposito("teste endereco deposito");
		return ponto;
	}

	private Publicador criaPublicadorPrincipal() {
		Publicador principal = new Publicador();
		principal.setGenero("M");
		principal.setEmail("teste@teste.com");
		principal.setNome("Marcão");
		principal.setTelefone("21981655159");
		return principal;
	}

	private Publicador criaPublicadorAuxiliar() {
		Publicador auxiliar = new Publicador();
		auxiliar.setGenero("F");
		auxiliar.setEmail("testea@teste.com");
		auxiliar.setNome("Gabriel");
		auxiliar.setTelefone("21981675009");
		return auxiliar;
	}

	private Agendamento criaAgendamento(String horainicio, String horafim) {
		Agendamento agendamento = new Agendamento();
		agendamento.setPonto(criaPonto());
		agendamento.setPrincipal(criaPublicadorPrincipal());
		agendamento.setAuxiliar(criaPublicadorAuxiliar());
		agendamento.setPublicador(criaPublicadorAuxiliar());
		agendamento.setHorainicio(horainicio);
		agendamento.setHorafim(horafim);
		agendamento.setData(new Date());
		return agendamento;
	}
}
