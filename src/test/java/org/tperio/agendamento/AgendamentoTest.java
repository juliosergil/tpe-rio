package org.tperio.agendamento;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.config.MessageKeys;
import org.tperio.exception.TperioBusinessException;
import org.tperio.publicador.domain.Publicador;

class AgendamentoTest {

	@Test
	void testaInicioFimAgendamentoEntreAgendadas() throws TperioBusinessException {
		Agendamento agendamento = criaAgendamento("13", "16");
		List<Agendamento> agendamentos2 = Arrays.asList(criaAgendamento("12", "14"),
				criaAgendamento("15", "17"));
		TperioBusinessException exception = assertThrows(TperioBusinessException.class, () -> {
			agendamento.validaAgendamento(agendamentos2);
	    });
	    assertTrue(exception.getMensagens().stream().anyMatch(msg -> msg.equals(MessageKeys.AGENDAMENTOJAEXISTE)));
	}

	@Test
	void testaInicioAgendamentoNoMeioDeAgendada() throws TperioBusinessException {
		Agendamento agendamento = criaAgendamento("13", "16");
		List<Agendamento> agendamentos2 = Arrays.asList(criaAgendamento("12", "15"),
				criaAgendamento("16", "17"));
		TperioBusinessException exception = assertThrows(TperioBusinessException.class, () -> {
			agendamento.validaAgendamento(agendamentos2);
	    });
	    assertTrue(exception.getMensagens().stream().anyMatch(msg -> msg.equals(MessageKeys.AGENDAMENTOJAEXISTE)));
	}

	@Test
	void testaAgendamentoDentroDeHoraAgendada() throws TperioBusinessException {
		Agendamento agendamento = criaAgendamento("13", "14");
		List<Agendamento> agendamentos2 = Arrays.asList(criaAgendamento("9", "12"),
				criaAgendamento("12", "16"));
		TperioBusinessException exception = assertThrows(TperioBusinessException.class, () -> {
			agendamento.validaAgendamento(agendamentos2);
	    });
	    assertTrue(exception.getMensagens().stream().anyMatch(msg -> msg.equals(MessageKeys.AGENDAMENTOJAEXISTE)));
	}

	@Test
	void testaAgendamentoContemHoraAgendada() throws TperioBusinessException {
		Agendamento agendamento = criaAgendamento("13", "16");
		List<Agendamento> agendamentos4 = Arrays.asList(criaAgendamento("9", "12"),
				criaAgendamento("14", "15"));
		TperioBusinessException exception = assertThrows(TperioBusinessException.class, () -> {
			agendamento.validaAgendamento(agendamentos4);
	    });
	    assertTrue(exception.getMensagens().stream().anyMatch(msg -> msg.equals(MessageKeys.AGENDAMENTOJAEXISTE)));
	}

	@Test
	void testaHoraInicioAgendamentoIgualHoraFimAgendada() throws TperioBusinessException {
		Agendamento agendamento = criaAgendamento("12", "13");
		List<Agendamento> agendamentos = Arrays.asList(criaAgendamento("9", "12"), criaAgendamento("14", "15"));
		assertThat(agendamentos).hasSize(2);
		agendamento.validaAgendamento(agendamentos);
	}

	@Test
	void testaHoraFimAgendamentoIgualHoraInicioAgendada() throws TperioBusinessException {
		Agendamento agendamento = criaAgendamento("12", "13");
		List<Agendamento> agendamentos = Arrays.asList(criaAgendamento("9", "12"), criaAgendamento("13", "15"));
		assertThat(agendamentos).hasSize(2);
		agendamento.validaAgendamento(agendamentos);
	}

	@Test
	void testaHoraFimAgendamentoIgualHoraFimAgendada() throws TperioBusinessException {
		Agendamento agendamento = criaAgendamento("11", "13");
		List<Agendamento> agendamentos = Arrays.asList(criaAgendamento("12", "13"));
		TperioBusinessException exception = assertThrows(TperioBusinessException.class, () -> {
			agendamento.validaAgendamento(agendamentos);
	    });
	    assertTrue(exception.getMensagens().stream().anyMatch(msg -> msg.equals(MessageKeys.AGENDAMENTOJAEXISTE)));
	}

	private Agendamento criaAgendamento(final String horaInicio, final String horaFim) {
		Agendamento ag = new Agendamento();
		Publicador pub = new Publicador();
		pub.setUuid("123");
		Publicador pubAuxi = new Publicador();
		pubAuxi.setUuid("auxi");
		ag.setAuxiliar(pubAuxi);
		ag.setPrincipal(pub);
		ag.setPublicador(pub);
		ag.setData(new Date());
		ag.setDatacriacao(new Date());
		ag.setHorainicio(horaInicio);
		ag.setHorafim(horaFim);
		return ag;
	}
}
