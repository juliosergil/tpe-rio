package org.tperio.agendamento;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.testutils.BaseControllerTest;

class RecuperaAgendamentoControllerTest extends BaseControllerTest {

	@Test
	@Sql(scripts = {"classpath:db/agendamento/testaRecuperaAgendamentoPublicadorComSucesso.sql"}, 
		config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
		executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
	)
	void testaRecuperaAgendamentoPublicadorComSucesso() throws Exception {
		LocalDate date = LocalDate.now().minusDays(2);
		LocalDate dateFim = LocalDate.now().plusDays(2);
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String publicadorUuid = "testerecpubagendamento1";
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/agendamento/publicador/"+ publicadorUuid +"?data=" + dtf.format(date) + "&datafim=" + dtf.format(dateFim))
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].horainicio").exists())
                .andExpect(jsonPath("$[0].horafim").exists())
                .andExpect(jsonPath("$[0].data").exists())
                .andExpect(jsonPath("$[0].principal").exists())
                .andExpect(jsonPath("$[0].auxiliar").exists())
                .andExpect(jsonPath("$[0].ponto").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
		mockMvc.perform(MockMvcRequestBuilders.get("/api/agendamento/publicador/"+ publicadorUuid +"?data=" + dtf.format(date) + "&datafim=" + dtf.format(dateFim))
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].horainicio").exists())
                .andExpect(jsonPath("$[0].horafim").exists())
                .andExpect(jsonPath("$[0].data").exists())
                .andExpect(jsonPath("$[0].principal").exists())
                .andExpect(jsonPath("$[0].auxiliar").exists())
                .andExpect(jsonPath("$[0].ponto").exists())
                .andReturn();
		content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/agendamento/testaRecuperaAgendamentosPorDataPontoComSucesso.sql"}, 
			config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
			executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
		)
	void testaRecuperaAgendamentosPorDataPontoComSucesso() throws Exception {
		String data = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/agendamento/ponto/d3a84da2-7eb8-11ec-90d6-0242ac120099?data=" + data)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].horainicio").exists())
                .andExpect(jsonPath("$[0].horafim").exists())
                .andExpect(jsonPath("$[0].data").exists())
                .andExpect(jsonPath("$[0].principal").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/agendamento/testaRecuperaAgendamentosPorDataAguardandoParComSucesso.sql"}, 
			config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
			executionPhase = ExecutionPhase.BEFORE_TEST_METHOD
		)
	void testaRecuperaAgendamentosPorDataAguardandoParComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/agendamento?datainicio=20/6/2022&datafim=28/6/2022&aguardandopar=true")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].horainicio").exists())
                .andExpect(jsonPath("$[0].horafim").exists())
                .andExpect(jsonPath("$[0].data").exists())
                .andExpect(jsonPath("$[0].principal").exists())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
}
