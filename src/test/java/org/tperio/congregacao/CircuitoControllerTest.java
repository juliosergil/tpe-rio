package org.tperio.congregacao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.circuito.adapter.in.web.CircuitoDto;
import org.tperio.testutils.BaseControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;

class CircuitoControllerTest extends BaseControllerTest {
	
	@Test
	void testaCriaCircuitoComSucesso() throws Exception {
		CircuitoDto dto = CircuitoDto.builder()
				.nome("RJ23")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/circuito")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("RJ23")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	void testaRecuperarCircuitosAtivosComSucesso() throws Exception {
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/circuito")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].nome").exists())
                .andExpect(jsonPath("$[0].nome", is("RJ22")))
                .andExpect(jsonPath("$[0].uuid").exists())
                .andExpect(jsonPath("$[0].uuid").isNotEmpty())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/congregacao/testaAtualizaCircuitoComSucesso.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	void testaAtualizaCircuitoComSucesso() throws Exception {
		CircuitoDto dto = CircuitoDto.builder()
				.uuid("testa-atualizacao-circuito-sucesso")
				.nome("RJ26")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/api/circuito/" + "testa-atualizacao-circuito-sucesso")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("RJ26")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid", is("testa-atualizacao-circuito-sucesso")))
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
}
