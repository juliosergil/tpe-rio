package org.tperio.congregacao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.congregacao.adapter.in.web.CongregacaoDto;
import org.tperio.testutils.BaseControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;

class CongregacaoControllerTest extends BaseControllerTest {
	
	@Test
	@Sql(scripts = {"classpath:db/congregacao/testaAtualizaCongregacaoComSucesso.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	void testaAtualizaCongregacaoComSucesso() throws Exception {
		CongregacaoDto dto = CongregacaoDto.builder()
				.uuid("teste-atualiza-congregacao-sucesso")
				.nome("Teste update Congregacao")
				.uuidcircuito("d3a84da2-7eb8-11ec-90d6-0242ac120008")
				.numero("654321")
				.uuidregiao("teste-uuid-congregacao-atualiza")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/api/congregacao/" + "teste-atualiza-congregacao-sucesso")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Teste update Congregacao")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid", is("teste-atualiza-congregacao-sucesso")))
                .andExpect(jsonPath("$.numero").exists())
                .andExpect(jsonPath("$.numero", is("654321")))
                .andExpect(jsonPath("$.circuitoDto").exists())
                .andExpect(jsonPath("$.circuitoDto.uuid", is("d3a84da2-7eb8-11ec-90d6-0242ac120008")))
                .andExpect(jsonPath("$.regiaoDto").exists())
                .andExpect(jsonPath("$.regiaoDto.uuid", is("teste-uuid-congregacao-atualiza")))
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/congregacao/testaApagaCongregacaoComSucesso.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	void testaApagaCongregacaoComSucesso() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.delete("/api/congregacao/teste-apaga-cong-sucesso")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/congregacao/recuperaCongregacao.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/congregacao/apagaregiao.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaRecuperarCongregacoesAtivasComSucesso() throws Exception {
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/congregacao/find")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].nome").exists())
                .andExpect(jsonPath("$[0].nome", is("Humaitá")))
                .andExpect(jsonPath("$[0].numero").exists())
                .andExpect(jsonPath("$[0].numero", is("123456")))
                .andExpect(jsonPath("$[0].uuid").exists())
                .andExpect(jsonPath("$[0].uuid").isNotEmpty())
                .andExpect(jsonPath("$[0].regiaoDto").exists())
                .andExpect(jsonPath("$[0].regiaoDto.uuid", is("teste-uuid-atualiza-regiao")))
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/congregacao/gravaregiao.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/congregacao/apagaregiao.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaCriaCongregacaoComSucesso() throws Exception {
		CongregacaoDto dto = CongregacaoDto.builder()
				.nome("Botafogo")
				.numero("654321")
				.uuidregiao("teste-uuid-atualiza-regiao")
				.uuidcircuito("d3a84da2-7eb8-11ec-90d6-0242ac120008")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/congregacao")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("Botafogo")))
                .andExpect(jsonPath("$.numero").exists())
                .andExpect(jsonPath("$.numero", is("654321")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andExpect(jsonPath("$.regiaoDto").exists())
                .andExpect(jsonPath("$.regiaoDto.uuid", is("teste-uuid-atualiza-regiao")))
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}

}
