package org.tperio.congregacao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.jdbc.SqlConfig.TransactionMode;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.tperio.regiao.adapter.in.web.RegiaoDto;
import org.tperio.testutils.BaseControllerTest;

import com.fasterxml.jackson.databind.ObjectMapper;

class RegiaoControllerTest extends BaseControllerTest {
	
	@Test
	void testaCriaRegiaoComSucesso() throws Exception {
		RegiaoDto dto = RegiaoDto.builder()
				.nome("RJ23")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/api/regiao")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("RJ23")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid").isNotEmpty())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/congregacao/gravaregiao.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/congregacao/apagaregiao.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaRecuperarRegioesAtivasComSucesso() throws Exception {
		
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/api/regiao")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].nome").exists())
                .andExpect(jsonPath("$[0].nome", is("teste atualiza regiao")))
                .andExpect(jsonPath("$[0].uuid").exists())
                .andExpect(jsonPath("$[0].uuid").isNotEmpty())
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
	
	@Test
	@Sql(scripts = {"classpath:db/congregacao/gravaregiao.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(scripts = {"classpath:db/congregacao/apagaregiao.sql"}, 
	config = @SqlConfig(transactionMode = TransactionMode.ISOLATED), 
	executionPhase = ExecutionPhase.AFTER_TEST_METHOD)
	void testaAtualizaRegiaoComSucesso() throws Exception {
		RegiaoDto dto = RegiaoDto.builder()
				.uuid("teste-uuid-atualiza-regiao")
				.nome("RJ26")
				.build();
		String json = new ObjectMapper().writeValueAsString(dto);
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.put("/api/regiao/teste-uuid-atualiza-regiao")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.nome").exists())
                .andExpect(jsonPath("$.nome", is("RJ26")))
                .andExpect(jsonPath("$.uuid").exists())
                .andExpect(jsonPath("$.uuid", is("teste-uuid-atualiza-regiao")))
                .andReturn();
		String content = result.getResponse().getContentAsString();
		assertNotNull(content);
	}
}
