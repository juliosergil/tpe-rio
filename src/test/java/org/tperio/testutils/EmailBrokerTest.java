package org.tperio.testutils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.tperio.TpeRioApplication;
import org.tperio.agendamento.domain.Agendamento;
import org.tperio.ponto.domain.Ponto;
import org.tperio.publicador.domain.Publicador;
import org.tperio.service.EmailBroker;

@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@ContextConfiguration(classes = TpeRioApplication.class)
class EmailBrokerTest {
	
	@Autowired private EmailBroker broker;

	@Test
	void testaTemplateAgendamentoComAuxiliarComSucesso() {
		Agendamento agendamento = new Agendamento();
		Publicador principal = new Publicador();
		principal.setGenero("M");
		principal.setEmail("teste@teste.com");
		principal.setNome("Marcão");
		principal.setTelefone("21981655159");
		Publicador auxiliar = new Publicador();
		auxiliar.setGenero("F");
		auxiliar.setEmail("testea@teste.com");
		auxiliar.setNome("Gabriel");
		auxiliar.setTelefone("21981675009");
		Ponto ponto = new Ponto("123");
		ponto.setNome("Teste ponto");
		ponto.setResponsavel(principal);
		ponto.setAuxiliar(auxiliar);
		ponto.setEndereco("teste endereco");
		ponto.setEnderecodeposito("teste endereco deposito");
		agendamento.setPonto(ponto);
		agendamento.setPrincipal(principal);
		agendamento.setAuxiliar(auxiliar);
		agendamento.setHorainicio("7");
		agendamento.setHorafim("10");
		agendamento.setData(new Date());
		String retorno = broker.enviaEmailAgendamento(agendamento);
		assertNotNull(retorno);
	}
	
	@Test
	void testaTemplateProgramacaoDoDiaComSucesso() {
		Agendamento ag = criaAgendamento("11", "12");
		ag.setAuxiliar(null);
		Agendamento ag2 = criaAgendamento("12", "14");
		ag2.setPrincipal(null);
		String retorno = broker.enviaEmailProgramacaoDia("teste@gmail.com",
				Arrays.asList(criaAgendamento("7", "10"), criaAgendamento("10", "11"), ag, ag2),
				new Date(),
				criaPonto());
		assertNotNull(retorno);
		System.out.println(retorno);
		
	}
	
	private Ponto criaPonto() {
		Ponto ponto = new Ponto("123");
		ponto.setNome("Teste ponto");
		ponto.setResponsavel(criaPublicadorPrincipal());
		ponto.setAuxiliar(criaPublicadorAuxiliar());
		ponto.setEndereco("teste endereco");
		ponto.setEnderecodeposito("teste endereco deposito");
		return ponto;
	}
	
	private Publicador criaPublicadorPrincipal() {
		Publicador principal = new Publicador();
		principal.setGenero("M");
		principal.setEmail("teste@teste.com");
		principal.setNome("Marcão");
		principal.setTelefone("21981655159");
		return principal;
	}
	
	private Publicador criaPublicadorAuxiliar() {
		Publicador auxiliar = new Publicador();
		auxiliar.setGenero("F");
		auxiliar.setEmail("testea@teste.com");
		auxiliar.setNome("Gabriel");
		auxiliar.setTelefone("21981675009");
		return auxiliar;
	}
	
	
	private Agendamento criaAgendamento(String horainicio, String horafim) {
		Agendamento agendamento = new Agendamento();
		agendamento.setPonto(criaPonto());
		agendamento.setPrincipal(criaPublicadorPrincipal());
		agendamento.setAuxiliar(criaPublicadorAuxiliar());
		agendamento.setHorainicio(horainicio);
		agendamento.setHorafim(horafim);
		agendamento.setData(new Date());
		return agendamento;
	}
	
	@Test
	void testaTemplateAgendamentoSemAuxiliarComSucesso() {
		Agendamento agendamento = new Agendamento();
		agendamento.setPonto(criaPonto());
		agendamento.setPrincipal(criaPublicadorPrincipal());
		agendamento.setHorainicio("7");
		agendamento.setHorafim("10");
		agendamento.setData(new Date());
		String retorno = broker.enviaEmailAgendamento(agendamento);
		assertNotNull(retorno);
	}
	
	@Test
	void testaTemplateCancelamentoAgendamentoComSucesso() {
		Agendamento agendamento = new Agendamento();
		agendamento.setPonto(criaPonto());
		agendamento.setPrincipal(criaPublicadorPrincipal());
		agendamento.setHorainicio("7");
		agendamento.setHorafim("10");
		agendamento.setData(new Date());
		String retorno = broker.enviaEmailCancelamento(agendamento.getPrincipal(), agendamento);
		assertNotNull(retorno);
	}
	
}
