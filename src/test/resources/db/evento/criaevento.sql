insert into regiao (idregiao, nome, uuid, datacriacao) 
values (18997, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-atualiza-ponto', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (18997, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 18997, '2022-05-31 22:31:23.000000');


INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (9997, 'marcao', 'eventocriapubuuid1', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 18997, NOW());

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (9996, 'marcao', 'eventocriapubuuid2', 'gabrieldeantonio@proton2.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 18997, NOW());

INSERT INTO `ponto` (`nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES ('Teste', 'testepontoativoeventocriauuid', 'rua teste', 18997, '123', '321', 9997, 9996, 'rua teste deposito', '456', '654', '0', '0', 9997, '2022-04-27 22:31:23.000000', '1');

INSERT INTO `evento` (`idevento`, `nome`, `uuid`, `data_inicio`, `data_fim`, `datacriacao`) 
VALUES (8899, 'Teste ponto evento', 'testepontoeventocriauuid', '2022-01-01', '2022-01-30', '2022-04-27 22:31:23.000000');