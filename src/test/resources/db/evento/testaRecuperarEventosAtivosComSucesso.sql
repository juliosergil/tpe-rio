insert into regiao (idregiao, nome, uuid, datacriacao) 
values (8997, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-atualiza-ponto', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (8997, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 8997, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (8997, 'marcao', 'eventopubuuid1', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 8997, NOW());

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (8996, 'marcao', 'eventopubuuid2', 'gabrieldeantonio@proton2.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 8997, NOW());

INSERT INTO `ponto` (`idponto`, `nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES (8999, 'Teste', 'testerecuperaeventouuid', 'rua teste', 8997, '123', '321', 8997, 8996, 'rua teste deposito', '456', '654', '0', '0', 8997, '2022-04-27 22:31:23.000000', '1');


INSERT INTO `evento` (`idevento`, `nome`, `uuid`, `data_inicio`, `data_fim`, `datacriacao`) 
VALUES (8999, 'Teste evento', 'testepontoeventorecuperauuid', '2022-01-01', '2022-01-30', now());
