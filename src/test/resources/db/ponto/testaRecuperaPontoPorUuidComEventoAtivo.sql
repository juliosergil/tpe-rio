insert into regiao (idregiao, nome, uuid, datacriacao) 
values (7999, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (7999, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 7999, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (7999, 'marcao', 'd3a84da2-7eb8-11ec-90d6-0242ac120006', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', 
	'21981655159', 1, 'M', 7999, NOW()
);

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (7998, 'marcao', 'd3a84da2-7eb8-11ec-90d6-0242ac120007', 'gabrieldeantonio@proton2.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', 
	'21981655159', 1, 'M', 7999, NOW()
);

INSERT INTO `ponto` (`idponto`, `nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES (8899, 'Teste', 'testpontouuidcomevento', 'rua teste', 7999, '123', '321', 7999, 7998, 'rua teste deposito', '456', '654', '0', '0', 7998, '2022-04-27 22:31:23.000000', '1');

INSERT INTO `evento` (`idevento`, `nome`, `uuid`, `data_inicio`, `data_fim`, `datacriacao`) 
VALUES (8899, 'Teste evento', 'testepubcomeventouuid', '2022-01-01', '2023-12-30', '2022-04-27 22:31:23.000000');

INSERT INTO `pontosevento` (`idponto`, `idevento`) 
VALUES (8899, 8899);

INSERT INTO `publicadoresevento` (`idevento`, `idpublicador`) 
VALUES (8899, 7999);
