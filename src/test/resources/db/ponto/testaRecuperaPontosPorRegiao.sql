delete from pontosevento;
delete from ponto;

insert into regiao (idregiao, nome, uuid, datacriacao) 
values (4997, 'teste recupera pontos por regiao', 'teste-recupera-pontos-regiao', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (4997, 'Teste Grava com sucesso', 1, 'recupera-ponto-sucesso','123', 4997, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (4997, 'marcao', 'd3a84da2-7eb8-11ec-90d6-0242ac120008', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 4997, NOW());

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (4996, 'marcao', 'd3a84da2-7eb8-11ec-90d6-0242ac120009', 'gabrieldeantonio@proton2.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 4997, NOW());

INSERT INTO `ponto` (`nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES ('Teste', 'testepontoativouuid', 'rua teste', 4997, '123', '321', 4997, 4996, 'rua teste deposito', '456', '654', '0', '0', 4996, '2022-04-27 22:31:23.000000', '1');