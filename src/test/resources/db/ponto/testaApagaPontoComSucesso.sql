insert into regiao (idregiao, nome, uuid, datacriacao) 
values (666333, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (666333, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 666333, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (666333, 'marcao', 'd3a84da2-7eb8-11ec-90d6-0242ac120006', 'gabrieldeantonio@proton12.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 666333, NOW());

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (667334, 'marcao', 'd3a84da2-7eb8-11ec-90d6-0242ac120007', 'gabrieldeantonio@proton23.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 666333, NOW());

INSERT INTO `ponto` (`nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES ('Teste apaga com sucesso', 'teste-apaga-ponto-sucesso', 'rua teste', 666333, '123', '321', 666333, 667334, 'rua teste deposito', '456', '654', '0', '0', 666333, '2022-04-27 22:31:23.000000', '1');
