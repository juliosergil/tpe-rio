insert into regiao (idregiao, nome, uuid, datacriacao) values (1122, 'teste regiao 2', 'teste-uuid-regiao2', NOW());
INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (1122, 'Teste pub Grava com sucesso', 1, 'pubeventouuid','123', 1122, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (1122, 'marcao', 'eventopontopubuuid3', 'gabrieldeantonio@proton3.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 1122, NOW());

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (1123, 'marcao', 'eventopontopubuuid4', 'gabrieldeantonio@proton4.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 1122, NOW());

INSERT INTO `evento` (`idevento`, `nome`, `uuid`, `data_inicio`, `data_fim`, `datacriacao`) 
VALUES (1122, 'Teste ponto evento', 'testepubeventouuid', '2022-01-01', '2022-01-30', '2022-04-27 22:31:23.000000');