insert into regiao (idregiao, nome, uuid, datacriacao) 
values (11995, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-novo-ponto', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (11995, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 11995, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (11995, 'marcao', 'testerecagendamento1', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 11995, NOW());

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (11994, 'marcao', 'testerecagendamento2', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 11995, NOW());

INSERT INTO `ponto` (`idponto`, `nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES (11999, 'Teste Grava com sucesso', 'd3a84da2-7eb8-11ec-90d6-0242ac120099', 'rua teste', 11995, '123', '321', 11995, 11994, 'rua teste deposito', '456', '654', '0', '0', 11995, '2022-04-27 22:31:23.000000', '1');

INSERT INTO agendamento (idponto, uuid, horainicio, horafim, data, idprincipal, idauxiliar, idpublicador, datacriacao)
VALUES (11999, 'Teste Grava com sucesso', '12:00', '13:00', '2022-06-25', 11995, null, 11994, NOW());