delete from publicadoresevento;
delete from pontosevento;
delete from evento;
delete from agendamento;
delete from ponto;
delete from publicador;

insert into regiao (idregiao, nome, uuid, datacriacao) 
values (71994, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-atualiza-ponto', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (71994, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 71994, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (71995, 'marcao', 'testerecpubagendamento1-atualizaagendamento', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.',
	'21981655159', 1, 'M', 71994, NOW()
);

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (71994, 'marcao', 'testerecpubagendamento2-atualizaagendamento', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.',
	'21981655159', 1, 'M', 71994, NOW()
);

INSERT INTO `ponto` (`idponto`, `nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES (71999, 'Teste-Grava-com-sucesso-atualizaagendamento', 'testerecpubagendamento-atualizaagendamento', 'rua teste', 71994, '123', '321', 71995, 71994, 'rua teste deposito', '456', '654', '0', '0', 71995, '2022-04-27 22:31:23.000000', '1');

INSERT INTO agendamento (idponto, uuid, horainicio, horafim, data, idprincipal, idauxiliar, idpublicador, datacriacao)
VALUES (71999, 'teste-atualiza-com-sucesso-agendamento', '12:00', '13:00', NOW(), 71995, 71994, 71995, NOW());