insert into regiao (idregiao, nome, uuid, datacriacao) 
values (12994, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-novo-ponto', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (12994, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 12994, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (12995, 'marcao', 'testerecagendamento1', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 12994, NOW());

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (12994, 'marcao', 'testerecagendamento2', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 12994, NOW());

INSERT INTO `ponto` (`idponto`, `nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES (12999, 'Teste Grava com sucesso', 'd3a84da2-7eb8-11ec-90d6-0242ac120099', 'rua teste', 12994, '123', '321', 12995, 12994, 'rua teste deposito', '456', '654', '0', '0', 12994, '2022-04-27 22:31:23.000000', '1');

INSERT INTO `evento` (`idevento`, `nome`, `uuid`, `data_inicio`, `data_fim`, `datacriacao`) 
VALUES (12999, 'Teste evento', 'testeeventouuid', '2022-01-01', '2023-12-30', '2022-04-27 22:31:23.000000');

INSERT INTO `pontosevento` (`idponto`, `idevento`) 
VALUES (12999, 12999);

INSERT INTO agendamento (idagendamento, idponto, uuid, horainicio, horafim, data, idprincipal, idauxiliar, idpublicador, datacriacao)
VALUES (12999, 12999, 'Teste Grava com sucesso', '12:00', '13:00', '2022-06-25', 12995, null, 12994, NOW());
