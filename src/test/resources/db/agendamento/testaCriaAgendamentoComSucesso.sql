insert into regiao (idregiao, nome, uuid, datacriacao) 
values (664, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-atualiza-ponto', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (664, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 664, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (664, 'marcao', 'testeagendamento1', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.',
	'21981655159', 1, 'M',  664, NOW()
);

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (663, 'marcao', 'testeagendamento2', 'gabrieldeantonio@proton2.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', 
	'21981655159', 1, 'M', 664, NOW()
);

INSERT INTO `ponto` (`nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES ('Teste Grava com sucesso', 'testepontoagendamentouuid', 'rua teste', 664, '123', '321', 664, 663, 'rua teste deposito', '456', '654', '0', '0', 663, '2022-04-27 22:31:23.000000', '1');