insert into regiao (idregiao, nome, uuid, datacriacao) 
values (59597, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-publicador', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (59597, 'Teste Grava com sucesso', 1, 'teste-atualiza-pub-sucesso','123', 59597, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES ('marcao', 'teste-atualiza-pub-com-sucesso', 'gabrieldeantonio@protonUPDATE.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 59597, NOW());