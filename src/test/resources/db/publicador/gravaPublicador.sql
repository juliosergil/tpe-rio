delete from publicador where idpublicador = 1;

delete from congregacao where idcongregacao = 159595;

delete from regiao where idregiao = 159595;

insert into regiao (idregiao, nome, uuid, datacriacao) 
values (159595, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-publicador', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (159595, 'Teste Grava com sucesso', 1, 'teste-grava-publicador','123', 159595, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES ('testemarcao', 'teste-pesquisa-pub-com-sucesso', 'gabrieldeantonio@protonPESQUISA.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 159595, NOW());