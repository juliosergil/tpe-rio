insert into regiao (idregiao, nome, uuid, datacriacao) 
values (59599, 'teste atualiza regiao', 'teste-uuid-atualiza-regiao-publicador', NOW());

INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (59599, 'Teste Grava com sucesso', 1, 'd3a84da2-7eb8-11ec-90d6-0242ac120066','123', 59599, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (77777, 'marcao', 'teste-atualiza-pub-com-sucesso', 'gabrieldeantonio@protonupdateeve.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 59599, NOW());

INSERT INTO `evento` (`idevento`, `nome`, `uuid`, `data_inicio`, `data_fim`, `datacriacao`) 
VALUES (8879, 'Teste evento', 'testepubcomeventouuid', '2022-01-01', '2023-12-30', '2022-04-27 22:31:23.000000');

INSERT INTO `publicadoresevento` (`idevento`, `idpublicador`) 
VALUES (8879, 77777);