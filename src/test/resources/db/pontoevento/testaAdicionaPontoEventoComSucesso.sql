delete from pontosevento;
delete from ponto;
delete from evento;

insert into regiao (idregiao, nome, uuid, datacriacao) values (1121, 'teste regiao', 'teste-uuid-regiao', NOW());
INSERT INTO `congregacao` (idcongregacao, `nome`, `idcircuito`, `uuid`, `numero`,`idregiao`,`datacriacao`) 
VALUES (997, 'Teste Grava com sucesso', 1, 'pontoeventouuid','123', 1121, '2022-05-31 22:31:23.000000');

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (9977, 'marcao', 'eventopontopubuuid1', 'gabrieldeantonio@proton1.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 997, NOW());

INSERT INTO publicador(`idpublicador`, `nome`, `uuid`, `email`, `senha`, `telefone`, `idperfil`, `genero`, `idcongregacao`, `datacriacao`) 
VALUES (9976, 'marcao', 'eventopontopubuuid2', 'gabrieldeantonio@proton2.me', '$2a$10$GPPRXJP9zKAgE3qA/ArdZeeyu0TYevSbEmyfb4X2Xb0DNjLUUhQm.', '21981655159', 1, 'M', 997, NOW());

INSERT INTO `ponto` (`nome`, `uuid`, `endereco`, `idregiao`, `latitude`, `longitude`, `idresponsavel`, `idauxiliar`, `enderecodeposito`, `latitudedeposito`, `longitudedeposito`, `turistico`, `evento`, `idpublicador`, `datacriacao`, `ativo`) 
VALUES ('Teste', 'testeaddpontoeventouuid', 'rua teste', 1121, '123', '321', 9977, 9976, 'rua teste deposito', '456', '654', '0', '0', 9977, '2022-04-27 22:31:23.000000', '1');

INSERT INTO `evento` (`idevento`, `nome`, `uuid`, `data_inicio`, `data_fim`, `datacriacao`) 
VALUES (8899, 'Teste ponto evento', 'testepontoeventouuid', '2022-01-01', '2022-01-30', '2022-04-27 22:31:23.000000');